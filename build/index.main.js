// Automatically generated with Reach 0.1.7 (d2352c9e)
/* eslint-disable */
export const _version = '0.1.7';
export const _versionHash = '0.1.7 (d2352c9e)';
export const _backendVersion = 6;

export function getExports(s) {
  const stdlib = s.reachStdlib;
  return {
    };
  };
export function _getViews(s, viewlib) {
  const stdlib = s.reachStdlib;
  const ctc0 = stdlib.T_Address;
  const ctc1 = stdlib.T_Token;
  const ctc2 = stdlib.T_UInt;
  const ctc3 = stdlib.T_Bool;
  const ctc4 = stdlib.T_Tuple([ctc3, ctc2, ctc2, ctc2, ctc2, ctc2, ctc2]);
  const ctc5 = stdlib.T_Null;
  const ctc6 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc7 = stdlib.T_Struct([['challengeId', ctc6], ['paid', ctc3], ['wagerAmount', ctc2]]);
  const ctc8 = stdlib.T_Data({
    None: ctc5,
    Some: ctc7
    });
  const map0_ctc = ctc8;
  
  
  return {
    infos: {
      ViewReader: {
        read: {
          decode: async (i, svs, args) => {
            if (stdlib.eq(i, stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 1))) {
              const [v850] = svs;
              stdlib.assert(false, 'illegal view')
              }
            if (stdlib.eq(i, stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4))) {
              const [v850, v863, v870, v871, v873, v874, v875, v882, v883] = svs;
              return (await ((async () => {
                
                const v900 = [true, v874, v875, v873, v870, v871, v882];
                
                return v900;}))(...args));
              }
            
            stdlib.assert(false, 'illegal view')
            },
          ty: ctc4
          }
        }
      },
    views: {
      1: [ctc0],
      4: [ctc0, ctc1, ctc2, ctc2, ctc2, ctc2, ctc2, ctc2, ctc2]
      }
    };
  
  };
export function _getMaps(s) {
  const stdlib = s.reachStdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Tuple([ctc5]);
  return {
    mapDataTy: ctc6
    };
  };
export async function Deployer(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for Deployer expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for Deployer expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Token;
  const ctc7 = stdlib.T_Tuple([ctc3, ctc6]);
  const ctc8 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc9 = stdlib.T_Tuple([ctc8]);
  const ctc10 = stdlib.T_Tuple([]);
  const ctc11 = stdlib.T_Address;
  const ctc12 = stdlib.T_Tuple([ctc11]);
  const ctc13 = stdlib.T_Tuple([ctc1, ctc11, ctc11]);
  const ctc14 = stdlib.T_Tuple([ctc13]);
  const ctc15 = stdlib.T_Tuple([ctc1, ctc11, ctc11, ctc3, ctc3, ctc3]);
  const ctc16 = stdlib.T_Tuple([ctc15]);
  const ctc17 = stdlib.T_Data({
    contractAPI_deposit0: ctc9,
    contractAPI_exitLoop0: ctc10,
    contractAPI_optIn0: ctc10,
    contractAPI_resetPlayerWager0: ctc12,
    contractAPI_timeoutAnnouncement0: ctc14,
    contractAPI_winnerAnnouncement0: ctc16
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const txn1 = await (ctc.sendrecv({
    args: [],
    evt_cnt: 0,
    funcNum: 0,
    lct: stdlib.checkedBigNumberify('./index.rsh:31:12:dot', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [],
    pay: [stdlib.checkedBigNumberify('./index.rsh:decimal', stdlib.UInt_max, 0), []],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [], secs: v852, time: v851, didSend: v19, from: v850 } = txn1;
      
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify('./index.rsh:decimal', stdlib.UInt_max, 0),
        kind: 'to',
        tok: undefined
        });
      const v854 = await ctc.getContractAddress();
      const v855 = await ctc.getContractInfo();
      
      sim_r.isHalt = false;
      
      return sim_r;
      }),
    soloSend: true,
    timeoutAt: undefined,
    tys: [],
    waitIfNotPresent: false
    }));
  const {data: [], secs: v852, time: v851, didSend: v19, from: v850 } = txn1;
  ;
  const v854 = await ctc.getContractAddress();
  const v855 = await ctc.getContractInfo();
  stdlib.protect(ctc0, await interact.showCtcInfo(v855), {
    at: './index.rsh:37:29:application',
    fs: ['at ./index.rsh:36:16:application call to [unknown function] (defined at: ./index.rsh:36:20:function exp)'],
    msg: 'showCtcInfo',
    who: 'Deployer'
    });
  stdlib.protect(ctc0, await interact.showAddress(v854), {
    at: './index.rsh:38:29:application',
    fs: ['at ./index.rsh:36:16:application call to [unknown function] (defined at: ./index.rsh:36:20:function exp)'],
    msg: 'showAddress',
    who: 'Deployer'
    });
  const v858 = stdlib.protect(ctc7, await interact.getZoneToken(), {
    at: './index.rsh:39:65:application',
    fs: ['at ./index.rsh:36:16:application call to [unknown function] (defined at: ./index.rsh:36:20:function exp)'],
    msg: 'getZoneToken',
    who: 'Deployer'
    });
  const v859 = v858[stdlib.checkedBigNumberify('./index.rsh:39:15:array', stdlib.UInt_max, 0)];
  const v860 = v858[stdlib.checkedBigNumberify('./index.rsh:39:15:array', stdlib.UInt_max, 1)];
  
  const txn2 = await (ctc.sendrecv({
    args: [v850, v859, v860],
    evt_cnt: 2,
    funcNum: 1,
    lct: v851,
    onlyIf: true,
    out_tys: [ctc3, ctc6],
    pay: [stdlib.checkedBigNumberify('./index.rsh:43:39:decimal', stdlib.UInt_max, 2000000), []],
    sim_p: (async (txn2) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v862, v863], secs: v865, time: v864, didSend: v36, from: v861 } = txn2;
      
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
        kind: 'init',
        tok: v863
        });
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify('./index.rsh:43:39:decimal', stdlib.UInt_max, 2000000),
        kind: 'to',
        tok: undefined
        });
      const v868 = stdlib.addressEq(v850, v861);
      stdlib.assert(v868, {
        at: './index.rsh:43:12:dot',
        fs: [],
        msg: 'sender correct',
        who: 'Deployer'
        });
      const v870 = stdlib.checkedBigNumberify('./index.rsh:46:30:decimal', stdlib.UInt_max, 0);
      const v871 = stdlib.checkedBigNumberify('./index.rsh:46:32:decimal', stdlib.UInt_max, 0);
      const v872 = true;
      const v873 = stdlib.checkedBigNumberify('./index.rsh:46:28:decimal', stdlib.UInt_max, 0);
      const v874 = stdlib.checkedBigNumberify('./index.rsh:46:24:decimal', stdlib.UInt_max, 0);
      const v875 = stdlib.checkedBigNumberify('./index.rsh:46:26:decimal', stdlib.UInt_max, 0);
      const v876 = v864;
      const v882 = stdlib.checkedBigNumberify('./index.rsh:43:39:decimal', stdlib.UInt_max, 2000000);
      const v883 = stdlib.checkedBigNumberify('./index.rsh:43:12:dot', stdlib.UInt_max, 0);
      
      if (await (async () => {
        
        return v872;})()) {
        sim_r.isHalt = false;
        }
      else {
        sim_r.txns.push({
          amt: v882,
          kind: 'from',
          to: v850,
          tok: undefined
          });
        sim_r.txns.push({
          amt: v883,
          kind: 'from',
          to: v850,
          tok: v863
          });
        sim_r.txns.push({
          kind: 'halt',
          tok: v863
          })
        sim_r.txns.push({
          kind: 'halt',
          tok: undefined
          })
        sim_r.isHalt = true;
        }
      return sim_r;
      }),
    soloSend: true,
    timeoutAt: undefined,
    tys: [ctc11, ctc3, ctc6],
    waitIfNotPresent: false
    }));
  const {data: [v862, v863], secs: v865, time: v864, didSend: v36, from: v861 } = txn2;
  ;
  ;
  const v868 = stdlib.addressEq(v850, v861);
  stdlib.assert(v868, {
    at: './index.rsh:43:12:dot',
    fs: [],
    msg: 'sender correct',
    who: 'Deployer'
    });
  let v870 = stdlib.checkedBigNumberify('./index.rsh:46:30:decimal', stdlib.UInt_max, 0);
  let v871 = stdlib.checkedBigNumberify('./index.rsh:46:32:decimal', stdlib.UInt_max, 0);
  let v872 = true;
  let v873 = stdlib.checkedBigNumberify('./index.rsh:46:28:decimal', stdlib.UInt_max, 0);
  let v874 = stdlib.checkedBigNumberify('./index.rsh:46:24:decimal', stdlib.UInt_max, 0);
  let v875 = stdlib.checkedBigNumberify('./index.rsh:46:26:decimal', stdlib.UInt_max, 0);
  let v876 = v864;
  let v882 = stdlib.checkedBigNumberify('./index.rsh:43:39:decimal', stdlib.UInt_max, 2000000);
  let v883 = stdlib.checkedBigNumberify('./index.rsh:43:12:dot', stdlib.UInt_max, 0);
  
  while (await (async () => {
    
    return v872;})()) {
    const txn3 = await (ctc.recv({
      didSend: false,
      evt_cnt: 1,
      funcNum: 3,
      out_tys: [ctc17],
      timeoutAt: undefined,
      waitIfNotPresent: false
      }));
    const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn3;
    switch (v1110[0]) {
      case 'contractAPI_deposit0': {
        const v1113 = v1110[1];
        const v1114 = v1113[stdlib.checkedBigNumberify('./index.rsh:53:7:spread', stdlib.UInt_max, 0)];
        const v1115 = v1114[stdlib.checkedBigNumberify('./index.rsh:57:7:array', stdlib.UInt_max, 0)];
        const v1116 = v1114[stdlib.checkedBigNumberify('./index.rsh:57:7:array', stdlib.UInt_max, 1)];
        const v1132 = stdlib.add(v882, v1115);
        ;
        ;
        undefined;
        const v1139 = stdlib.gt(v1115, stdlib.checkedBigNumberify('./index.rsh:59:24:decimal', stdlib.UInt_max, 0));
        stdlib.assert(v1139, {
          at: './index.rsh:59:14:application',
          fs: ['at ./index.rsh:58:5:application call to [unknown function] (defined at: ./index.rsh:58:5:function exp)'],
          msg: null,
          who: 'Deployer'
          });
        const v1140 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v1109), null);
        let v1141;
        switch (v1140[0]) {
          case 'None': {
            const v1142 = v1140[1];
            v1141 = true;
            
            break;
            }
          case 'Some': {
            const v1143 = v1140[1];
            v1141 = false;
            
            break;
            }
          }
        await txn3.getOutput('api', 'v1141', ctc2, v1141);
        const v1149 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v1109), null);
        switch (v1149[0]) {
          case 'None': {
            const v1150 = v1149[1];
            const v1151 = {
              challengeId: v1116,
              paid: true,
              wagerAmount: v1115
              };
            map0[v1109] = v1151;
            const v1152 = stdlib.add(v874, stdlib.checkedBigNumberify('./index.rsh:65:44:decimal', stdlib.UInt_max, 1));
            const v1153 = stdlib.add(v875, v1115);
            const v1154 = stdlib.add(v870, stdlib.checkedBigNumberify('./index.rsh:65:113:decimal', stdlib.UInt_max, 1));
            const v1155 = stdlib.add(v871, v1115);
            const cv870 = v1154;
            const cv871 = v1155;
            const cv872 = true;
            const cv873 = v873;
            const cv874 = v1152;
            const cv875 = v1153;
            const cv876 = v1111;
            const cv882 = v1132;
            const cv883 = v883;
            
            v870 = cv870;
            v871 = cv871;
            v872 = cv872;
            v873 = cv873;
            v874 = cv874;
            v875 = cv875;
            v876 = cv876;
            v882 = cv882;
            v883 = cv883;
            
            continue;
            break;
            }
          case 'Some': {
            const v1165 = v1149[1];
            const v1166 = stdlib.add(v871, v1115);
            const cv870 = v870;
            const cv871 = v1166;
            const cv872 = true;
            const cv873 = v873;
            const cv874 = v874;
            const cv875 = v875;
            const cv876 = v1111;
            const cv882 = v1132;
            const cv883 = v883;
            
            v870 = cv870;
            v871 = cv871;
            v872 = cv872;
            v873 = cv873;
            v874 = cv874;
            v875 = cv875;
            v876 = cv876;
            v882 = cv882;
            v883 = cv883;
            
            continue;
            break;
            }
          }
        break;
        }
      case 'contractAPI_exitLoop0': {
        const v1415 = v1110[1];
        ;
        ;
        undefined;
        const v1474 = true;
        await txn3.getOutput('api', 'v1474', ctc2, v1474);
        const cv870 = v870;
        const cv871 = v871;
        const cv872 = false;
        const cv873 = v873;
        const cv874 = v874;
        const cv875 = v875;
        const cv876 = v1111;
        const cv882 = v882;
        const cv883 = v883;
        
        v870 = cv870;
        v871 = cv871;
        v872 = cv872;
        v873 = cv873;
        v874 = cv874;
        v875 = cv875;
        v876 = cv876;
        v882 = cv882;
        v883 = cv883;
        
        continue;
        break;
        }
      case 'contractAPI_optIn0': {
        const v1712 = v1110[1];
        ;
        ;
        undefined;
        const v1779 = true;
        await txn3.getOutput('api', 'v1779', ctc2, v1779);
        const cv870 = v870;
        const cv871 = v871;
        const cv872 = true;
        const cv873 = v873;
        const cv874 = v874;
        const cv875 = v875;
        const cv876 = v1111;
        const cv882 = v882;
        const cv883 = v883;
        
        v870 = cv870;
        v871 = cv871;
        v872 = cv872;
        v873 = cv873;
        v874 = cv874;
        v875 = cv875;
        v876 = cv876;
        v882 = cv882;
        v883 = cv883;
        
        continue;
        break;
        }
      case 'contractAPI_resetPlayerWager0': {
        const v2009 = v1110[1];
        const v2010 = v2009[stdlib.checkedBigNumberify('./index.rsh:216:7:spread', stdlib.UInt_max, 0)];
        ;
        ;
        undefined;
        const v2088 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2010), null);
        let v2089;
        switch (v2088[0]) {
          case 'None': {
            const v2090 = v2088[1];
            const v2092 = [false, stdlib.checkedBigNumberify('./index.rsh:237:25:decimal', stdlib.UInt_max, 0)];
            v2089 = v2092;
            
            break;
            }
          case 'Some': {
            const v2093 = v2088[1];
            const v2094 = v2093.wagerAmount;
            const v2096 = [true, v2094];
            v2089 = v2096;
            
            break;
            }
          }
        const v2097 = true;
        await txn3.getOutput('api', 'v2097', ctc2, v2097);
        const v2103 = v2089[stdlib.checkedBigNumberify('./index.rsh:244:27:array ref', stdlib.UInt_max, 0)];
        if (v2103) {
          const v2104 = v2089[stdlib.checkedBigNumberify('./index.rsh:245:36:array ref', stdlib.UInt_max, 1)];
          const v2105 = stdlib.gt(v2104, stdlib.checkedBigNumberify('./index.rsh:245:43:decimal', stdlib.UInt_max, 0));
          const v2108 = stdlib.ge(v882, v2104);
          const v2109 = v2105 ? v2108 : false;
          stdlib.assert(v2109, {
            at: './index.rsh:245:16:application',
            fs: ['at ./index.rsh:233:5:application call to [unknown function] (defined at: ./index.rsh:233:5:function exp)'],
            msg: null,
            who: 'Deployer'
            });
          const v2114 = stdlib.sub(v882, v2104);
          ;
          const v2115 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:247:101:decimal', stdlib.UInt_max, 1));
          const v2117 = stdlib.sub(v871, v2104);
          const cv870 = v2115;
          const cv871 = v2117;
          const cv872 = true;
          const cv873 = v873;
          const cv874 = v874;
          const cv875 = v875;
          const cv876 = v1111;
          const cv882 = v2114;
          const cv883 = v883;
          
          v870 = cv870;
          v871 = cv871;
          v872 = cv872;
          v873 = cv873;
          v874 = cv874;
          v875 = cv875;
          v876 = cv876;
          v882 = cv882;
          v883 = cv883;
          
          continue;}
        else {
          const cv870 = v870;
          const cv871 = v871;
          const cv872 = true;
          const cv873 = v873;
          const cv874 = v874;
          const cv875 = v875;
          const cv876 = v1111;
          const cv882 = v882;
          const cv883 = v883;
          
          v870 = cv870;
          v871 = cv871;
          v872 = cv872;
          v873 = cv873;
          v874 = cv874;
          v875 = cv875;
          v876 = cv876;
          v882 = cv882;
          v883 = cv883;
          
          continue;}
        break;
        }
      case 'contractAPI_timeoutAnnouncement0': {
        const v2309 = v1110[1];
        const v2310 = v2309[stdlib.checkedBigNumberify('./index.rsh:74:7:spread', stdlib.UInt_max, 0)];
        const v2312 = v2310[stdlib.checkedBigNumberify('./index.rsh:106:7:array', stdlib.UInt_max, 1)];
        const v2313 = v2310[stdlib.checkedBigNumberify('./index.rsh:106:7:array', stdlib.UInt_max, 2)];
        ;
        ;
        undefined;
        const v2442 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2312), null);
        let v2443;
        switch (v2442[0]) {
          case 'None': {
            const v2444 = v2442[1];
            const v2446 = [false, stdlib.checkedBigNumberify('./index.rsh:112:25:decimal', stdlib.UInt_max, 0)];
            v2443 = v2446;
            
            break;
            }
          case 'Some': {
            const v2447 = v2442[1];
            const v2448 = v2447.wagerAmount;
            const v2450 = [true, v2448];
            v2443 = v2450;
            
            break;
            }
          }
        const v2451 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2313), null);
        let v2452;
        switch (v2451[0]) {
          case 'None': {
            const v2453 = v2451[1];
            const v2455 = [false, stdlib.checkedBigNumberify('./index.rsh:124:25:decimal', stdlib.UInt_max, 0)];
            v2452 = v2455;
            
            break;
            }
          case 'Some': {
            const v2456 = v2451[1];
            const v2457 = v2456.wagerAmount;
            const v2459 = [true, v2457];
            v2452 = v2459;
            
            break;
            }
          }
        const v2460 = v2443[stdlib.checkedBigNumberify('./index.rsh:133:49:array ref', stdlib.UInt_max, 0)];
        const v2461 = v2452[stdlib.checkedBigNumberify('./index.rsh:133:72:array ref', stdlib.UInt_max, 0)];
        const v2462 = v2460 ? true : v2461;
        await txn3.getOutput('api', 'v2462', ctc2, v2462);
        const v2470 = v2460 ? v2461 : false;
        if (v2470) {
          const v2472 = v2443[stdlib.checkedBigNumberify('./index.rsh:136:47:array ref', stdlib.UInt_max, 1)];
          const v2473 = v2452[stdlib.checkedBigNumberify('./index.rsh:136:69:array ref', stdlib.UInt_max, 1)];
          const v2474 = stdlib.add(v2472, v2473);
          const v2475 = stdlib.ge(v882, v2474);
          stdlib.assert(v2475, {
            at: './index.rsh:136:16:application',
            fs: ['at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
            msg: null,
            who: 'Deployer'
            });
          const v2480 = stdlib.sub(v882, v2472);
          ;
          const v2485 = stdlib.sub(v2480, v2473);
          ;
          map0[v2312] = undefined;
          map0[v2313] = undefined;
          const v2487 = 'All Done.';
          stdlib.protect(ctc0, await interact.log(v2487), {
            at: './index.rsh:1:39:application',
            fs: ['at ./index.rsh:1:21:application call to [unknown function] (defined at: ./index.rsh:1:25:function exp)', 'at ./index.rsh:141:30:application call to "liftedInteract" (defined at: ./index.rsh:141:30:application)', 'at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
            msg: 'log',
            who: 'Deployer'
            });
          
          const v2488 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:142:101:decimal', stdlib.UInt_max, 2));
          const v2492 = stdlib.sub(v871, v2474);
          const cv870 = v2488;
          const cv871 = v2492;
          const cv872 = true;
          const cv873 = v873;
          const cv874 = v874;
          const cv875 = v875;
          const cv876 = v1111;
          const cv882 = v2485;
          const cv883 = v883;
          
          v870 = cv870;
          v871 = cv871;
          v872 = cv872;
          v873 = cv873;
          v874 = cv874;
          v875 = cv875;
          v876 = cv876;
          v882 = cv882;
          v883 = cv883;
          
          continue;}
        else {
          if (v2460) {
            const v2502 = v2443[stdlib.checkedBigNumberify('./index.rsh:146:35:array ref', stdlib.UInt_max, 1)];
            const v2503 = stdlib.gt(v2502, stdlib.checkedBigNumberify('./index.rsh:146:42:decimal', stdlib.UInt_max, 0));
            const v2506 = stdlib.ge(v882, v2502);
            const v2507 = v2503 ? v2506 : false;
            stdlib.assert(v2507, {
              at: './index.rsh:146:16:application',
              fs: ['at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
              msg: null,
              who: 'Deployer'
              });
            const v2512 = stdlib.sub(v882, v2502);
            ;
            map0[v2312] = undefined;
            const v2513 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:149:101:decimal', stdlib.UInt_max, 1));
            const v2515 = stdlib.sub(v871, v2502);
            const cv870 = v2513;
            const cv871 = v2515;
            const cv872 = true;
            const cv873 = v873;
            const cv874 = v874;
            const cv875 = v875;
            const cv876 = v1111;
            const cv882 = v2512;
            const cv883 = v883;
            
            v870 = cv870;
            v871 = cv871;
            v872 = cv872;
            v873 = cv873;
            v874 = cv874;
            v875 = cv875;
            v876 = cv876;
            v882 = cv882;
            v883 = cv883;
            
            continue;}
          else {
            if (v2461) {
              const v2525 = v2452[stdlib.checkedBigNumberify('./index.rsh:153:35:array ref', stdlib.UInt_max, 1)];
              const v2526 = stdlib.gt(v2525, stdlib.checkedBigNumberify('./index.rsh:153:42:decimal', stdlib.UInt_max, 0));
              const v2529 = stdlib.ge(v882, v2525);
              const v2530 = v2526 ? v2529 : false;
              stdlib.assert(v2530, {
                at: './index.rsh:153:16:application',
                fs: ['at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
                msg: null,
                who: 'Deployer'
                });
              const v2535 = stdlib.sub(v882, v2525);
              ;
              map0[v2313] = undefined;
              const v2536 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:156:101:decimal', stdlib.UInt_max, 1));
              const v2538 = stdlib.sub(v871, v2525);
              const cv870 = v2536;
              const cv871 = v2538;
              const cv872 = true;
              const cv873 = v873;
              const cv874 = v874;
              const cv875 = v875;
              const cv876 = v1111;
              const cv882 = v2535;
              const cv883 = v883;
              
              v870 = cv870;
              v871 = cv871;
              v872 = cv872;
              v873 = cv873;
              v874 = cv874;
              v875 = cv875;
              v876 = cv876;
              v882 = cv882;
              v883 = cv883;
              
              continue;}
            else {
              const cv870 = v870;
              const cv871 = v871;
              const cv872 = true;
              const cv873 = v873;
              const cv874 = v874;
              const cv875 = v875;
              const cv876 = v1111;
              const cv882 = v882;
              const cv883 = v883;
              
              v870 = cv870;
              v871 = cv871;
              v872 = cv872;
              v873 = cv873;
              v874 = cv874;
              v875 = cv875;
              v876 = cv876;
              v882 = cv882;
              v883 = cv883;
              
              continue;}}}
        break;
        }
      case 'contractAPI_winnerAnnouncement0': {
        const v2612 = v1110[1];
        const v2613 = v2612[stdlib.checkedBigNumberify('./index.rsh:163:7:spread', stdlib.UInt_max, 0)];
        const v2615 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 1)];
        const v2616 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 2)];
        const v2617 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 3)];
        const v2618 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 4)];
        ;
        ;
        undefined;
        const v2869 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2615), null);
        let v2870;
        switch (v2869[0]) {
          case 'None': {
            const v2871 = v2869[1];
            v2870 = false;
            
            break;
            }
          case 'Some': {
            const v2872 = v2869[1];
            v2870 = true;
            
            break;
            }
          }
        const v2873 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2616), null);
        let v2874;
        switch (v2873[0]) {
          case 'None': {
            const v2875 = v2873[1];
            v2874 = false;
            
            break;
            }
          case 'Some': {
            const v2876 = v2873[1];
            v2874 = true;
            
            break;
            }
          }
        const v2877 = v2870 ? v2874 : false;
        await txn3.getOutput('api', 'v2877', ctc2, v2877);
        if (v2877) {
          const v2884 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:192:19:decimal', stdlib.UInt_max, 2), v2617);
          const v2885 = stdlib.gt(v2884, stdlib.checkedBigNumberify('./index.rsh:192:30:decimal', stdlib.UInt_max, 0));
          const v2888 = stdlib.le(v2884, v882);
          const v2889 = v2885 ? v2888 : false;
          stdlib.assert(v2889, {
            at: './index.rsh:192:16:application',
            fs: ['at ./index.rsh:166:5:application call to [unknown function] (defined at: ./index.rsh:166:5:function exp)'],
            msg: null,
            who: 'Deployer'
            });
          const v2891 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:193:31:decimal', stdlib.UInt_max, 2), v2618);
          const v2892 = stdlib.sub(v2884, v2891);
          const v2896 = stdlib.sub(v882, v2892);
          ;
          map0[v2615] = undefined;
          map0[v2616] = undefined;
          const v2898 = stdlib.add(v873, v2891);
          const v2899 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:196:114:decimal', stdlib.UInt_max, 2));
          const v2901 = stdlib.sub(v871, v2884);
          const cv870 = v2899;
          const cv871 = v2901;
          const cv872 = true;
          const cv873 = v2898;
          const cv874 = v874;
          const cv875 = v875;
          const cv876 = v1111;
          const cv882 = v2896;
          const cv883 = v883;
          
          v870 = cv870;
          v871 = cv871;
          v872 = cv872;
          v873 = cv873;
          v874 = cv874;
          v875 = cv875;
          v876 = cv876;
          v882 = cv882;
          v883 = cv883;
          
          continue;}
        else {
          const cv870 = v870;
          const cv871 = v871;
          const cv872 = true;
          const cv873 = v873;
          const cv874 = v874;
          const cv875 = v875;
          const cv876 = v1111;
          const cv882 = v882;
          const cv883 = v883;
          
          v870 = cv870;
          v871 = cv871;
          v872 = cv872;
          v873 = cv873;
          v874 = cv874;
          v875 = cv875;
          v876 = cv876;
          v882 = cv882;
          v883 = cv883;
          
          continue;}
        break;
        }
      }
    
    }
  ;
  ;
  return;
  
  
  
  
  };
export async function contractAPI_deposit(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_deposit expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_deposit expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc9 = stdlib.T_Tuple([ctc8]);
  const ctc10 = stdlib.T_Tuple([]);
  const ctc11 = stdlib.T_Tuple([ctc6]);
  const ctc12 = stdlib.T_Tuple([ctc1, ctc6, ctc6]);
  const ctc13 = stdlib.T_Tuple([ctc12]);
  const ctc14 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc15 = stdlib.T_Tuple([ctc14]);
  const ctc16 = stdlib.T_Data({
    contractAPI_deposit0: ctc9,
    contractAPI_exitLoop0: ctc10,
    contractAPI_optIn0: ctc10,
    contractAPI_resetPlayerWager0: ctc11,
    contractAPI_timeoutAnnouncement0: ctc13,
    contractAPI_winnerAnnouncement0: ctc15
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v850, v863, v870, v871, v873, v874, v875, v882, v883] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v903 = stdlib.protect(ctc9, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:54:5:application call to [unknown function] (defined at: ./index.rsh:54:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_deposit0" (defined at: ./index.rsh:53:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_deposit'
    });
  const v904 = v903[stdlib.checkedBigNumberify('./index.rsh:53:7:spread', stdlib.UInt_max, 0)];
  const v905 = v904[stdlib.checkedBigNumberify('./index.rsh:54:7:array', stdlib.UInt_max, 0)];
  const v908 = stdlib.gt(v905, stdlib.checkedBigNumberify('./index.rsh:55:24:decimal', stdlib.UInt_max, 0));
  stdlib.assert(v908, {
    at: './index.rsh:55:14:application',
    fs: ['at ./index.rsh:54:5:application call to [unknown function] (defined at: ./index.rsh:54:29:function exp)', 'at ./index.rsh:54:5:application call to [unknown function] (defined at: ./index.rsh:54:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_deposit0" (defined at: ./index.rsh:53:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
    msg: null,
    who: 'contractAPI_deposit'
    });
  
  const v1038 = ['contractAPI_deposit0', v903];
  
  const txn1 = await (ctc.sendrecv({
    args: [v850, v863, v870, v871, v873, v874, v875, v882, v883, v1038],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc16],
    pay: [v905, [[stdlib.checkedBigNumberify('./index.rsh:57:40:decimal', stdlib.UInt_max, 0), v863]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
      
      switch (v1110[0]) {
        case 'contractAPI_deposit0': {
          const v1113 = v1110[1];
          const v1114 = v1113[stdlib.checkedBigNumberify('./index.rsh:53:7:spread', stdlib.UInt_max, 0)];
          const v1115 = v1114[stdlib.checkedBigNumberify('./index.rsh:57:7:array', stdlib.UInt_max, 0)];
          const v1116 = v1114[stdlib.checkedBigNumberify('./index.rsh:57:7:array', stdlib.UInt_max, 1)];
          const v1132 = stdlib.add(v882, v1115);
          sim_r.txns.push({
            amt: v1115,
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:57:40:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v863
            });
          undefined;
          const v1139 = stdlib.gt(v1115, stdlib.checkedBigNumberify('./index.rsh:59:24:decimal', stdlib.UInt_max, 0));
          stdlib.assert(v1139, {
            at: './index.rsh:59:14:application',
            fs: ['at ./index.rsh:58:5:application call to [unknown function] (defined at: ./index.rsh:58:5:function exp)'],
            msg: null,
            who: 'contractAPI_deposit'
            });
          const v1140 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v1109), null);
          let v1141;
          switch (v1140[0]) {
            case 'None': {
              const v1142 = v1140[1];
              v1141 = true;
              
              break;
              }
            case 'Some': {
              const v1143 = v1140[1];
              v1141 = false;
              
              break;
              }
            }
          const v1144 = await txn1.getOutput('api', 'v1141', ctc2, v1141);
          
          const v1149 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v1109), null);
          switch (v1149[0]) {
            case 'None': {
              const v1150 = v1149[1];
              const v1151 = {
                challengeId: v1116,
                paid: true,
                wagerAmount: v1115
                };
              stdlib.simMapSet(sim_r, 0, v1109, v1151);
              const v1152 = stdlib.add(v874, stdlib.checkedBigNumberify('./index.rsh:65:44:decimal', stdlib.UInt_max, 1));
              const v1153 = stdlib.add(v875, v1115);
              const v1154 = stdlib.add(v870, stdlib.checkedBigNumberify('./index.rsh:65:113:decimal', stdlib.UInt_max, 1));
              const v1155 = stdlib.add(v871, v1115);
              const v29224 = v1154;
              const v29225 = v1155;
              const v29227 = v873;
              const v29228 = v1152;
              const v29229 = v1153;
              const v29231 = v1132;
              const v29232 = v883;
              sim_r.isHalt = false;
              
              break;
              }
            case 'Some': {
              const v1165 = v1149[1];
              const v1166 = stdlib.add(v871, v1115);
              const v29233 = v870;
              const v29234 = v1166;
              const v29236 = v873;
              const v29237 = v874;
              const v29238 = v875;
              const v29240 = v1132;
              const v29241 = v883;
              sim_r.isHalt = false;
              
              break;
              }
            }
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v1415 = v1110[1];
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v1712 = v1110[1];
          
          break;
          }
        case 'contractAPI_resetPlayerWager0': {
          const v2009 = v1110[1];
          
          break;
          }
        case 'contractAPI_timeoutAnnouncement0': {
          const v2309 = v1110[1];
          
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v2612 = v1110[1];
          
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc16],
    waitIfNotPresent: false
    }));
  const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
  switch (v1110[0]) {
    case 'contractAPI_deposit0': {
      const v1113 = v1110[1];
      const v1114 = v1113[stdlib.checkedBigNumberify('./index.rsh:53:7:spread', stdlib.UInt_max, 0)];
      const v1115 = v1114[stdlib.checkedBigNumberify('./index.rsh:57:7:array', stdlib.UInt_max, 0)];
      const v1116 = v1114[stdlib.checkedBigNumberify('./index.rsh:57:7:array', stdlib.UInt_max, 1)];
      const v1132 = stdlib.add(v882, v1115);
      ;
      ;
      undefined;
      const v1139 = stdlib.gt(v1115, stdlib.checkedBigNumberify('./index.rsh:59:24:decimal', stdlib.UInt_max, 0));
      stdlib.assert(v1139, {
        at: './index.rsh:59:14:application',
        fs: ['at ./index.rsh:58:5:application call to [unknown function] (defined at: ./index.rsh:58:5:function exp)'],
        msg: null,
        who: 'contractAPI_deposit'
        });
      const v1140 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v1109), null);
      let v1141;
      switch (v1140[0]) {
        case 'None': {
          const v1142 = v1140[1];
          v1141 = true;
          
          break;
          }
        case 'Some': {
          const v1143 = v1140[1];
          v1141 = false;
          
          break;
          }
        }
      const v1144 = await txn1.getOutput('api', 'v1141', ctc2, v1141);
      if (v558) {
        stdlib.protect(ctc0, await interact.out(v1113, v1144), {
          at: './index.rsh:53:8:application',
          fs: ['at ./index.rsh:53:8:application call to [unknown function] (defined at: ./index.rsh:53:8:function exp)', 'at ./index.rsh:60:20:application call to "depositResult" (defined at: ./index.rsh:58:5:function exp)', 'at ./index.rsh:58:5:application call to [unknown function] (defined at: ./index.rsh:58:5:function exp)'],
          msg: 'out',
          who: 'contractAPI_deposit'
          });
        }
      else {
        }
      
      const v1149 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v1109), null);
      switch (v1149[0]) {
        case 'None': {
          const v1150 = v1149[1];
          const v1151 = {
            challengeId: v1116,
            paid: true,
            wagerAmount: v1115
            };
          map0[v1109] = v1151;
          const v1152 = stdlib.add(v874, stdlib.checkedBigNumberify('./index.rsh:65:44:decimal', stdlib.UInt_max, 1));
          const v1153 = stdlib.add(v875, v1115);
          const v1154 = stdlib.add(v870, stdlib.checkedBigNumberify('./index.rsh:65:113:decimal', stdlib.UInt_max, 1));
          const v1155 = stdlib.add(v871, v1115);
          const v29224 = v1154;
          const v29225 = v1155;
          const v29227 = v873;
          const v29228 = v1152;
          const v29229 = v1153;
          const v29231 = v1132;
          const v29232 = v883;
          return;
          
          break;
          }
        case 'Some': {
          const v1165 = v1149[1];
          const v1166 = stdlib.add(v871, v1115);
          const v29233 = v870;
          const v29234 = v1166;
          const v29236 = v873;
          const v29237 = v874;
          const v29238 = v875;
          const v29240 = v1132;
          const v29241 = v883;
          return;
          
          break;
          }
        }
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v1415 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_optIn0': {
      const v1712 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_resetPlayerWager0': {
      const v2009 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_timeoutAnnouncement0': {
      const v2309 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v2612 = v1110[1];
      return;
      break;
      }
    }
  
  
  };
export async function contractAPI_exitLoop(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_exitLoop expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_exitLoop expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([]);
  const ctc9 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc10 = stdlib.T_Tuple([ctc9]);
  const ctc11 = stdlib.T_Tuple([ctc6]);
  const ctc12 = stdlib.T_Tuple([ctc1, ctc6, ctc6]);
  const ctc13 = stdlib.T_Tuple([ctc12]);
  const ctc14 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc15 = stdlib.T_Tuple([ctc14]);
  const ctc16 = stdlib.T_Data({
    contractAPI_deposit0: ctc10,
    contractAPI_exitLoop0: ctc8,
    contractAPI_optIn0: ctc8,
    contractAPI_resetPlayerWager0: ctc11,
    contractAPI_timeoutAnnouncement0: ctc13,
    contractAPI_winnerAnnouncement0: ctc15
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v850, v863, v870, v871, v873, v874, v875, v882, v883] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v1003 = stdlib.protect(ctc8, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:205:7:application call to [unknown function] (defined at: ./index.rsh:205:7:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_exitLoop0" (defined at: ./index.rsh:205:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_exitLoop'
    });
  
  const v1051 = ['contractAPI_exitLoop0', v1003];
  
  const txn1 = await (ctc.sendrecv({
    args: [v850, v863, v870, v871, v873, v874, v875, v882, v883, v1051],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc16],
    pay: [stdlib.checkedBigNumberify('./index.rsh:205:7:decimal', stdlib.UInt_max, 0), [[stdlib.checkedBigNumberify('./index.rsh:52:12:decimal', stdlib.UInt_max, 0), v863]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
      
      switch (v1110[0]) {
        case 'contractAPI_deposit0': {
          const v1113 = v1110[1];
          
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v1415 = v1110[1];
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:205:7:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:52:12:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v863
            });
          undefined;
          const v1474 = true;
          const v1475 = await txn1.getOutput('api', 'v1474', ctc2, v1474);
          
          sim_r.txns.push({
            amt: v882,
            kind: 'from',
            to: v850,
            tok: undefined
            });
          sim_r.txns.push({
            amt: v883,
            kind: 'from',
            to: v850,
            tok: v863
            });
          sim_r.txns.push({
            kind: 'halt',
            tok: v863
            })
          sim_r.txns.push({
            kind: 'halt',
            tok: undefined
            })
          sim_r.isHalt = true;
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v1712 = v1110[1];
          
          break;
          }
        case 'contractAPI_resetPlayerWager0': {
          const v2009 = v1110[1];
          
          break;
          }
        case 'contractAPI_timeoutAnnouncement0': {
          const v2309 = v1110[1];
          
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v2612 = v1110[1];
          
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc16],
    waitIfNotPresent: false
    }));
  const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
  switch (v1110[0]) {
    case 'contractAPI_deposit0': {
      const v1113 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v1415 = v1110[1];
      ;
      ;
      undefined;
      const v1474 = true;
      const v1475 = await txn1.getOutput('api', 'v1474', ctc2, v1474);
      if (v558) {
        stdlib.protect(ctc0, await interact.out(v1415, v1475), {
          at: './index.rsh:205:8:application',
          fs: ['at ./index.rsh:205:8:application call to [unknown function] (defined at: ./index.rsh:205:8:function exp)', 'at ./index.rsh:205:43:application call to "result" (defined at: ./index.rsh:205:23:function exp)', 'at ./index.rsh:205:23:application call to [unknown function] (defined at: ./index.rsh:205:23:function exp)'],
          msg: 'out',
          who: 'contractAPI_exitLoop'
          });
        }
      else {
        }
      
      ;
      ;
      return;
      
      break;
      }
    case 'contractAPI_optIn0': {
      const v1712 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_resetPlayerWager0': {
      const v2009 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_timeoutAnnouncement0': {
      const v2309 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v2612 = v1110[1];
      return;
      break;
      }
    }
  
  
  };
export async function contractAPI_optIn(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_optIn expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_optIn expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([]);
  const ctc9 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc10 = stdlib.T_Tuple([ctc9]);
  const ctc11 = stdlib.T_Tuple([ctc6]);
  const ctc12 = stdlib.T_Tuple([ctc1, ctc6, ctc6]);
  const ctc13 = stdlib.T_Tuple([ctc12]);
  const ctc14 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc15 = stdlib.T_Tuple([ctc14]);
  const ctc16 = stdlib.T_Data({
    contractAPI_deposit0: ctc10,
    contractAPI_exitLoop0: ctc8,
    contractAPI_optIn0: ctc8,
    contractAPI_resetPlayerWager0: ctc11,
    contractAPI_timeoutAnnouncement0: ctc13,
    contractAPI_winnerAnnouncement0: ctc15
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v850, v863, v870, v871, v873, v874, v875, v882, v883] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v996 = stdlib.protect(ctc8, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:204:7:application call to [unknown function] (defined at: ./index.rsh:204:7:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_optIn0" (defined at: ./index.rsh:204:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_optIn'
    });
  
  const v1059 = ['contractAPI_optIn0', v996];
  
  const txn1 = await (ctc.sendrecv({
    args: [v850, v863, v870, v871, v873, v874, v875, v882, v883, v1059],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc16],
    pay: [stdlib.checkedBigNumberify('./index.rsh:204:7:decimal', stdlib.UInt_max, 0), [[stdlib.checkedBigNumberify('./index.rsh:52:12:decimal', stdlib.UInt_max, 0), v863]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
      
      switch (v1110[0]) {
        case 'contractAPI_deposit0': {
          const v1113 = v1110[1];
          
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v1415 = v1110[1];
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v1712 = v1110[1];
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:204:7:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:52:12:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v863
            });
          undefined;
          const v1779 = true;
          const v1780 = await txn1.getOutput('api', 'v1779', ctc2, v1779);
          
          const v29467 = v870;
          const v29468 = v871;
          const v29470 = v873;
          const v29471 = v874;
          const v29472 = v875;
          const v29474 = v882;
          const v29475 = v883;
          sim_r.isHalt = false;
          
          break;
          }
        case 'contractAPI_resetPlayerWager0': {
          const v2009 = v1110[1];
          
          break;
          }
        case 'contractAPI_timeoutAnnouncement0': {
          const v2309 = v1110[1];
          
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v2612 = v1110[1];
          
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc16],
    waitIfNotPresent: false
    }));
  const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
  switch (v1110[0]) {
    case 'contractAPI_deposit0': {
      const v1113 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v1415 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_optIn0': {
      const v1712 = v1110[1];
      ;
      ;
      undefined;
      const v1779 = true;
      const v1780 = await txn1.getOutput('api', 'v1779', ctc2, v1779);
      if (v558) {
        stdlib.protect(ctc0, await interact.out(v1712, v1780), {
          at: './index.rsh:204:8:application',
          fs: ['at ./index.rsh:204:8:application call to [unknown function] (defined at: ./index.rsh:204:8:function exp)', 'at ./index.rsh:204:40:application call to "result" (defined at: ./index.rsh:204:20:function exp)', 'at ./index.rsh:204:20:application call to [unknown function] (defined at: ./index.rsh:204:20:function exp)'],
          msg: 'out',
          who: 'contractAPI_optIn'
          });
        }
      else {
        }
      
      const v29467 = v870;
      const v29468 = v871;
      const v29470 = v873;
      const v29471 = v874;
      const v29472 = v875;
      const v29474 = v882;
      const v29475 = v883;
      return;
      
      break;
      }
    case 'contractAPI_resetPlayerWager0': {
      const v2009 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_timeoutAnnouncement0': {
      const v2309 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v2612 = v1110[1];
      return;
      break;
      }
    }
  
  
  };
export async function contractAPI_resetPlayerWager(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_resetPlayerWager expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_resetPlayerWager expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([ctc6]);
  const ctc9 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc10 = stdlib.T_Tuple([ctc9]);
  const ctc11 = stdlib.T_Tuple([]);
  const ctc12 = stdlib.T_Tuple([ctc1, ctc6, ctc6]);
  const ctc13 = stdlib.T_Tuple([ctc12]);
  const ctc14 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc15 = stdlib.T_Tuple([ctc14]);
  const ctc16 = stdlib.T_Data({
    contractAPI_deposit0: ctc10,
    contractAPI_exitLoop0: ctc11,
    contractAPI_optIn0: ctc11,
    contractAPI_resetPlayerWager0: ctc8,
    contractAPI_timeoutAnnouncement0: ctc13,
    contractAPI_winnerAnnouncement0: ctc15
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v850, v863, v870, v871, v873, v874, v875, v882, v883] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v1010 = stdlib.protect(ctc8, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:217:5:application call to [unknown function] (defined at: ./index.rsh:217:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_resetPlayerWager0" (defined at: ./index.rsh:216:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_resetPlayerWager'
    });
  const v1011 = v1010[stdlib.checkedBigNumberify('./index.rsh:216:7:spread', stdlib.UInt_max, 0)];
  const v1013 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v1011), null);
  let v1014;
  switch (v1013[0]) {
    case 'None': {
      const v1015 = v1013[1];
      const v1017 = [false, stdlib.checkedBigNumberify('./index.rsh:221:25:decimal', stdlib.UInt_max, 0)];
      v1014 = v1017;
      
      break;
      }
    case 'Some': {
      const v1018 = v1013[1];
      const v1019 = v1018.wagerAmount;
      const v1020 = stdlib.gt(v1019, stdlib.checkedBigNumberify('./index.rsh:224:39:decimal', stdlib.UInt_max, 0));
      const v1023 = stdlib.ge(v882, v1019);
      const v1024 = v1020 ? v1023 : false;
      stdlib.assert(v1024, {
        at: './index.rsh:224:17:application',
        fs: ['at ./index.rsh:219:31:application call to [unknown function] (defined at: ./index.rsh:223:21:function exp)', 'at ./index.rsh:219:31:application call to [unknown function] (defined at: ./index.rsh:219:31:function exp)', 'at ./index.rsh:217:5:application call to [unknown function] (defined at: ./index.rsh:217:15:function exp)', 'at ./index.rsh:217:5:application call to [unknown function] (defined at: ./index.rsh:217:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_resetPlayerWager0" (defined at: ./index.rsh:216:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
        msg: null,
        who: 'contractAPI_resetPlayerWager'
        });
      const v1027 = [true, v1019];
      v1014 = v1027;
      
      break;
      }
    }
  const v1028 = v1014[stdlib.checkedBigNumberify('./index.rsh:228:27:array ref', stdlib.UInt_max, 0)];
  if (v1028) {
    const v1029 = v1014[stdlib.checkedBigNumberify('./index.rsh:229:35:array ref', stdlib.UInt_max, 1)];
    const v1030 = stdlib.gt(v1029, stdlib.checkedBigNumberify('./index.rsh:229:42:decimal', stdlib.UInt_max, 0));
    const v1033 = stdlib.ge(v882, v1029);
    const v1034 = v1030 ? v1033 : false;
    stdlib.assert(v1034, {
      at: './index.rsh:229:15:application',
      fs: ['at ./index.rsh:217:5:application call to [unknown function] (defined at: ./index.rsh:217:15:function exp)', 'at ./index.rsh:217:5:application call to [unknown function] (defined at: ./index.rsh:217:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_resetPlayerWager0" (defined at: ./index.rsh:216:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
      msg: null,
      who: 'contractAPI_resetPlayerWager'
      });
    }
  else {
    }
  
  const v1067 = ['contractAPI_resetPlayerWager0', v1010];
  
  const txn1 = await (ctc.sendrecv({
    args: [v850, v863, v870, v871, v873, v874, v875, v882, v883, v1067],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc16],
    pay: [stdlib.checkedBigNumberify('./index.rsh:232:19:decimal', stdlib.UInt_max, 0), [[stdlib.checkedBigNumberify('./index.rsh:232:22:decimal', stdlib.UInt_max, 0), v863]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
      
      switch (v1110[0]) {
        case 'contractAPI_deposit0': {
          const v1113 = v1110[1];
          
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v1415 = v1110[1];
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v1712 = v1110[1];
          
          break;
          }
        case 'contractAPI_resetPlayerWager0': {
          const v2009 = v1110[1];
          const v2010 = v2009[stdlib.checkedBigNumberify('./index.rsh:216:7:spread', stdlib.UInt_max, 0)];
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:232:19:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:232:22:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v863
            });
          undefined;
          const v2088 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v2010), null);
          let v2089;
          switch (v2088[0]) {
            case 'None': {
              const v2090 = v2088[1];
              const v2092 = [false, stdlib.checkedBigNumberify('./index.rsh:237:25:decimal', stdlib.UInt_max, 0)];
              v2089 = v2092;
              
              break;
              }
            case 'Some': {
              const v2093 = v2088[1];
              const v2094 = v2093.wagerAmount;
              const v2096 = [true, v2094];
              v2089 = v2096;
              
              break;
              }
            }
          const v2097 = true;
          const v2098 = await txn1.getOutput('api', 'v2097', ctc2, v2097);
          
          const v2103 = v2089[stdlib.checkedBigNumberify('./index.rsh:244:27:array ref', stdlib.UInt_max, 0)];
          if (v2103) {
            const v2104 = v2089[stdlib.checkedBigNumberify('./index.rsh:245:36:array ref', stdlib.UInt_max, 1)];
            const v2105 = stdlib.gt(v2104, stdlib.checkedBigNumberify('./index.rsh:245:43:decimal', stdlib.UInt_max, 0));
            const v2108 = stdlib.ge(v882, v2104);
            const v2109 = v2105 ? v2108 : false;
            stdlib.assert(v2109, {
              at: './index.rsh:245:16:application',
              fs: ['at ./index.rsh:233:5:application call to [unknown function] (defined at: ./index.rsh:233:5:function exp)'],
              msg: null,
              who: 'contractAPI_resetPlayerWager'
              });
            const v2114 = stdlib.sub(v882, v2104);
            sim_r.txns.push({
              amt: v2104,
              kind: 'from',
              to: v2010,
              tok: undefined
              });
            const v2115 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:247:101:decimal', stdlib.UInt_max, 1));
            const v2117 = stdlib.sub(v871, v2104);
            const v29584 = v2115;
            const v29585 = v2117;
            const v29587 = v873;
            const v29588 = v874;
            const v29589 = v875;
            const v29591 = v2114;
            const v29592 = v883;
            sim_r.isHalt = false;
            }
          else {
            const v29593 = v870;
            const v29594 = v871;
            const v29596 = v873;
            const v29597 = v874;
            const v29598 = v875;
            const v29600 = v882;
            const v29601 = v883;
            sim_r.isHalt = false;
            }
          break;
          }
        case 'contractAPI_timeoutAnnouncement0': {
          const v2309 = v1110[1];
          
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v2612 = v1110[1];
          
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc16],
    waitIfNotPresent: false
    }));
  const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
  switch (v1110[0]) {
    case 'contractAPI_deposit0': {
      const v1113 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v1415 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_optIn0': {
      const v1712 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_resetPlayerWager0': {
      const v2009 = v1110[1];
      const v2010 = v2009[stdlib.checkedBigNumberify('./index.rsh:216:7:spread', stdlib.UInt_max, 0)];
      ;
      ;
      undefined;
      const v2088 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2010), null);
      let v2089;
      switch (v2088[0]) {
        case 'None': {
          const v2090 = v2088[1];
          const v2092 = [false, stdlib.checkedBigNumberify('./index.rsh:237:25:decimal', stdlib.UInt_max, 0)];
          v2089 = v2092;
          
          break;
          }
        case 'Some': {
          const v2093 = v2088[1];
          const v2094 = v2093.wagerAmount;
          const v2096 = [true, v2094];
          v2089 = v2096;
          
          break;
          }
        }
      const v2097 = true;
      const v2098 = await txn1.getOutput('api', 'v2097', ctc2, v2097);
      if (v558) {
        stdlib.protect(ctc0, await interact.out(v2009, v2098), {
          at: './index.rsh:216:8:application',
          fs: ['at ./index.rsh:216:8:application call to [unknown function] (defined at: ./index.rsh:216:8:function exp)', 'at ./index.rsh:243:18:application call to "resetResult" (defined at: ./index.rsh:233:5:function exp)', 'at ./index.rsh:233:5:application call to [unknown function] (defined at: ./index.rsh:233:5:function exp)'],
          msg: 'out',
          who: 'contractAPI_resetPlayerWager'
          });
        }
      else {
        }
      
      const v2103 = v2089[stdlib.checkedBigNumberify('./index.rsh:244:27:array ref', stdlib.UInt_max, 0)];
      if (v2103) {
        const v2104 = v2089[stdlib.checkedBigNumberify('./index.rsh:245:36:array ref', stdlib.UInt_max, 1)];
        const v2105 = stdlib.gt(v2104, stdlib.checkedBigNumberify('./index.rsh:245:43:decimal', stdlib.UInt_max, 0));
        const v2108 = stdlib.ge(v882, v2104);
        const v2109 = v2105 ? v2108 : false;
        stdlib.assert(v2109, {
          at: './index.rsh:245:16:application',
          fs: ['at ./index.rsh:233:5:application call to [unknown function] (defined at: ./index.rsh:233:5:function exp)'],
          msg: null,
          who: 'contractAPI_resetPlayerWager'
          });
        const v2114 = stdlib.sub(v882, v2104);
        ;
        const v2115 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:247:101:decimal', stdlib.UInt_max, 1));
        const v2117 = stdlib.sub(v871, v2104);
        const v29584 = v2115;
        const v29585 = v2117;
        const v29587 = v873;
        const v29588 = v874;
        const v29589 = v875;
        const v29591 = v2114;
        const v29592 = v883;
        return;
        }
      else {
        const v29593 = v870;
        const v29594 = v871;
        const v29596 = v873;
        const v29597 = v874;
        const v29598 = v875;
        const v29600 = v882;
        const v29601 = v883;
        return;
        }
      break;
      }
    case 'contractAPI_timeoutAnnouncement0': {
      const v2309 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v2612 = v1110[1];
      return;
      break;
      }
    }
  
  
  };
export async function contractAPI_timeoutAnnouncement(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_timeoutAnnouncement expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_timeoutAnnouncement expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([ctc1, ctc6, ctc6]);
  const ctc9 = stdlib.T_Tuple([ctc8]);
  const ctc10 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc11 = stdlib.T_Tuple([ctc10]);
  const ctc12 = stdlib.T_Tuple([]);
  const ctc13 = stdlib.T_Tuple([ctc6]);
  const ctc14 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc15 = stdlib.T_Tuple([ctc14]);
  const ctc16 = stdlib.T_Data({
    contractAPI_deposit0: ctc11,
    contractAPI_exitLoop0: ctc12,
    contractAPI_optIn0: ctc12,
    contractAPI_resetPlayerWager0: ctc13,
    contractAPI_timeoutAnnouncement0: ctc9,
    contractAPI_winnerAnnouncement0: ctc15
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v850, v863, v870, v871, v873, v874, v875, v882, v883] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v914 = stdlib.protect(ctc9, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_timeoutAnnouncement0" (defined at: ./index.rsh:74:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_timeoutAnnouncement'
    });
  const v915 = v914[stdlib.checkedBigNumberify('./index.rsh:74:7:spread', stdlib.UInt_max, 0)];
  const v917 = v915[stdlib.checkedBigNumberify('./index.rsh:75:7:array', stdlib.UInt_max, 1)];
  const v918 = v915[stdlib.checkedBigNumberify('./index.rsh:75:7:array', stdlib.UInt_max, 2)];
  const v920 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v917), null);
  let v921;
  switch (v920[0]) {
    case 'None': {
      const v922 = v920[1];
      const v924 = [false, stdlib.checkedBigNumberify('./index.rsh:79:25:decimal', stdlib.UInt_max, 0)];
      v921 = v924;
      
      break;
      }
    case 'Some': {
      const v925 = v920[1];
      const v926 = v925.wagerAmount;
      const v927 = stdlib.gt(v926, stdlib.checkedBigNumberify('./index.rsh:82:39:decimal', stdlib.UInt_max, 0));
      const v930 = stdlib.ge(v882, v926);
      const v931 = v927 ? v930 : false;
      stdlib.assert(v931, {
        at: './index.rsh:82:17:application',
        fs: ['at ./index.rsh:77:32:application call to [unknown function] (defined at: ./index.rsh:81:21:function exp)', 'at ./index.rsh:77:32:application call to [unknown function] (defined at: ./index.rsh:77:32:function exp)', 'at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:38:function exp)', 'at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_timeoutAnnouncement0" (defined at: ./index.rsh:74:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
        msg: null,
        who: 'contractAPI_timeoutAnnouncement'
        });
      const v934 = [true, v926];
      v921 = v934;
      
      break;
      }
    }
  const v935 = v921[stdlib.checkedBigNumberify('./index.rsh:86:28:array ref', stdlib.UInt_max, 0)];
  if (v935) {
    const v936 = v921[stdlib.checkedBigNumberify('./index.rsh:87:36:array ref', stdlib.UInt_max, 1)];
    const v937 = stdlib.gt(v936, stdlib.checkedBigNumberify('./index.rsh:87:43:decimal', stdlib.UInt_max, 0));
    const v940 = stdlib.ge(v882, v936);
    const v941 = v937 ? v940 : false;
    stdlib.assert(v941, {
      at: './index.rsh:87:15:application',
      fs: ['at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:38:function exp)', 'at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_timeoutAnnouncement0" (defined at: ./index.rsh:74:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
      msg: null,
      who: 'contractAPI_timeoutAnnouncement'
      });
    }
  else {
    }
  const v942 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v918), null);
  let v943;
  switch (v942[0]) {
    case 'None': {
      const v944 = v942[1];
      const v946 = [false, stdlib.checkedBigNumberify('./index.rsh:92:25:decimal', stdlib.UInt_max, 0)];
      v943 = v946;
      
      break;
      }
    case 'Some': {
      const v947 = v942[1];
      const v948 = v947.wagerAmount;
      const v949 = stdlib.gt(v948, stdlib.checkedBigNumberify('./index.rsh:95:40:decimal', stdlib.UInt_max, 0));
      const v952 = stdlib.ge(v882, v948);
      const v953 = v949 ? v952 : false;
      stdlib.assert(v953, {
        at: './index.rsh:95:17:application',
        fs: ['at ./index.rsh:90:32:application call to [unknown function] (defined at: ./index.rsh:94:22:function exp)', 'at ./index.rsh:90:32:application call to [unknown function] (defined at: ./index.rsh:90:32:function exp)', 'at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:38:function exp)', 'at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_timeoutAnnouncement0" (defined at: ./index.rsh:74:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
        msg: null,
        who: 'contractAPI_timeoutAnnouncement'
        });
      const v956 = [true, v948];
      v943 = v956;
      
      break;
      }
    }
  const v957 = v943[stdlib.checkedBigNumberify('./index.rsh:99:28:array ref', stdlib.UInt_max, 0)];
  if (v957) {
    const v958 = v943[stdlib.checkedBigNumberify('./index.rsh:100:36:array ref', stdlib.UInt_max, 1)];
    const v959 = stdlib.gt(v958, stdlib.checkedBigNumberify('./index.rsh:100:43:decimal', stdlib.UInt_max, 0));
    const v962 = stdlib.ge(v882, v958);
    const v963 = v959 ? v962 : false;
    stdlib.assert(v963, {
      at: './index.rsh:100:15:application',
      fs: ['at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:38:function exp)', 'at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_timeoutAnnouncement0" (defined at: ./index.rsh:74:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
      msg: null,
      who: 'contractAPI_timeoutAnnouncement'
      });
    }
  else {
    }
  const v966 = v935 ? v957 : false;
  if (v966) {
    const v968 = v921[stdlib.checkedBigNumberify('./index.rsh:103:48:array ref', stdlib.UInt_max, 1)];
    const v969 = v943[stdlib.checkedBigNumberify('./index.rsh:103:72:array ref', stdlib.UInt_max, 1)];
    const v970 = stdlib.add(v968, v969);
    const v971 = stdlib.ge(v882, v970);
    stdlib.assert(v971, {
      at: './index.rsh:103:15:application',
      fs: ['at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:38:function exp)', 'at ./index.rsh:75:5:application call to [unknown function] (defined at: ./index.rsh:75:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_timeoutAnnouncement0" (defined at: ./index.rsh:74:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
      msg: null,
      who: 'contractAPI_timeoutAnnouncement'
      });
    }
  else {
    }
  
  const v1078 = ['contractAPI_timeoutAnnouncement0', v914];
  
  const txn1 = await (ctc.sendrecv({
    args: [v850, v863, v870, v871, v873, v874, v875, v882, v883, v1078],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc16],
    pay: [stdlib.checkedBigNumberify('./index.rsh:106:42:decimal', stdlib.UInt_max, 0), [[stdlib.checkedBigNumberify('./index.rsh:106:45:decimal', stdlib.UInt_max, 0), v863]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
      
      switch (v1110[0]) {
        case 'contractAPI_deposit0': {
          const v1113 = v1110[1];
          
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v1415 = v1110[1];
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v1712 = v1110[1];
          
          break;
          }
        case 'contractAPI_resetPlayerWager0': {
          const v2009 = v1110[1];
          
          break;
          }
        case 'contractAPI_timeoutAnnouncement0': {
          const v2309 = v1110[1];
          const v2310 = v2309[stdlib.checkedBigNumberify('./index.rsh:74:7:spread', stdlib.UInt_max, 0)];
          const v2312 = v2310[stdlib.checkedBigNumberify('./index.rsh:106:7:array', stdlib.UInt_max, 1)];
          const v2313 = v2310[stdlib.checkedBigNumberify('./index.rsh:106:7:array', stdlib.UInt_max, 2)];
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:106:42:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:106:45:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v863
            });
          undefined;
          const v2442 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v2312), null);
          let v2443;
          switch (v2442[0]) {
            case 'None': {
              const v2444 = v2442[1];
              const v2446 = [false, stdlib.checkedBigNumberify('./index.rsh:112:25:decimal', stdlib.UInt_max, 0)];
              v2443 = v2446;
              
              break;
              }
            case 'Some': {
              const v2447 = v2442[1];
              const v2448 = v2447.wagerAmount;
              const v2450 = [true, v2448];
              v2443 = v2450;
              
              break;
              }
            }
          const v2451 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v2313), null);
          let v2452;
          switch (v2451[0]) {
            case 'None': {
              const v2453 = v2451[1];
              const v2455 = [false, stdlib.checkedBigNumberify('./index.rsh:124:25:decimal', stdlib.UInt_max, 0)];
              v2452 = v2455;
              
              break;
              }
            case 'Some': {
              const v2456 = v2451[1];
              const v2457 = v2456.wagerAmount;
              const v2459 = [true, v2457];
              v2452 = v2459;
              
              break;
              }
            }
          const v2460 = v2443[stdlib.checkedBigNumberify('./index.rsh:133:49:array ref', stdlib.UInt_max, 0)];
          const v2461 = v2452[stdlib.checkedBigNumberify('./index.rsh:133:72:array ref', stdlib.UInt_max, 0)];
          const v2462 = v2460 ? true : v2461;
          const v2463 = await txn1.getOutput('api', 'v2462', ctc2, v2462);
          
          const v2470 = v2460 ? v2461 : false;
          if (v2470) {
            const v2472 = v2443[stdlib.checkedBigNumberify('./index.rsh:136:47:array ref', stdlib.UInt_max, 1)];
            const v2473 = v2452[stdlib.checkedBigNumberify('./index.rsh:136:69:array ref', stdlib.UInt_max, 1)];
            const v2474 = stdlib.add(v2472, v2473);
            const v2475 = stdlib.ge(v882, v2474);
            stdlib.assert(v2475, {
              at: './index.rsh:136:16:application',
              fs: ['at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
              msg: null,
              who: 'contractAPI_timeoutAnnouncement'
              });
            const v2480 = stdlib.sub(v882, v2472);
            sim_r.txns.push({
              amt: v2472,
              kind: 'from',
              to: v2312,
              tok: undefined
              });
            const v2485 = stdlib.sub(v2480, v2473);
            sim_r.txns.push({
              amt: v2473,
              kind: 'from',
              to: v2313,
              tok: undefined
              });
            stdlib.simMapSet(sim_r, 0, v2312, undefined);
            stdlib.simMapSet(sim_r, 0, v2313, undefined);
            const v2488 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:142:101:decimal', stdlib.UInt_max, 2));
            const v2492 = stdlib.sub(v871, v2474);
            const v29710 = v2488;
            const v29711 = v2492;
            const v29713 = v873;
            const v29714 = v874;
            const v29715 = v875;
            const v29717 = v2485;
            const v29718 = v883;
            sim_r.isHalt = false;
            }
          else {
            if (v2460) {
              const v2502 = v2443[stdlib.checkedBigNumberify('./index.rsh:146:35:array ref', stdlib.UInt_max, 1)];
              const v2503 = stdlib.gt(v2502, stdlib.checkedBigNumberify('./index.rsh:146:42:decimal', stdlib.UInt_max, 0));
              const v2506 = stdlib.ge(v882, v2502);
              const v2507 = v2503 ? v2506 : false;
              stdlib.assert(v2507, {
                at: './index.rsh:146:16:application',
                fs: ['at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
                msg: null,
                who: 'contractAPI_timeoutAnnouncement'
                });
              const v2512 = stdlib.sub(v882, v2502);
              sim_r.txns.push({
                amt: v2502,
                kind: 'from',
                to: v2312,
                tok: undefined
                });
              stdlib.simMapSet(sim_r, 0, v2312, undefined);
              const v2513 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:149:101:decimal', stdlib.UInt_max, 1));
              const v2515 = stdlib.sub(v871, v2502);
              const v29719 = v2513;
              const v29720 = v2515;
              const v29722 = v873;
              const v29723 = v874;
              const v29724 = v875;
              const v29726 = v2512;
              const v29727 = v883;
              sim_r.isHalt = false;
              }
            else {
              if (v2461) {
                const v2525 = v2452[stdlib.checkedBigNumberify('./index.rsh:153:35:array ref', stdlib.UInt_max, 1)];
                const v2526 = stdlib.gt(v2525, stdlib.checkedBigNumberify('./index.rsh:153:42:decimal', stdlib.UInt_max, 0));
                const v2529 = stdlib.ge(v882, v2525);
                const v2530 = v2526 ? v2529 : false;
                stdlib.assert(v2530, {
                  at: './index.rsh:153:16:application',
                  fs: ['at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
                  msg: null,
                  who: 'contractAPI_timeoutAnnouncement'
                  });
                const v2535 = stdlib.sub(v882, v2525);
                sim_r.txns.push({
                  amt: v2525,
                  kind: 'from',
                  to: v2313,
                  tok: undefined
                  });
                stdlib.simMapSet(sim_r, 0, v2313, undefined);
                const v2536 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:156:101:decimal', stdlib.UInt_max, 1));
                const v2538 = stdlib.sub(v871, v2525);
                const v29728 = v2536;
                const v29729 = v2538;
                const v29731 = v873;
                const v29732 = v874;
                const v29733 = v875;
                const v29735 = v2535;
                const v29736 = v883;
                sim_r.isHalt = false;
                }
              else {
                const v29737 = v870;
                const v29738 = v871;
                const v29740 = v873;
                const v29741 = v874;
                const v29742 = v875;
                const v29744 = v882;
                const v29745 = v883;
                sim_r.isHalt = false;
                }}}
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v2612 = v1110[1];
          
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc16],
    waitIfNotPresent: false
    }));
  const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
  switch (v1110[0]) {
    case 'contractAPI_deposit0': {
      const v1113 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v1415 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_optIn0': {
      const v1712 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_resetPlayerWager0': {
      const v2009 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_timeoutAnnouncement0': {
      const v2309 = v1110[1];
      const v2310 = v2309[stdlib.checkedBigNumberify('./index.rsh:74:7:spread', stdlib.UInt_max, 0)];
      const v2312 = v2310[stdlib.checkedBigNumberify('./index.rsh:106:7:array', stdlib.UInt_max, 1)];
      const v2313 = v2310[stdlib.checkedBigNumberify('./index.rsh:106:7:array', stdlib.UInt_max, 2)];
      ;
      ;
      undefined;
      const v2442 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2312), null);
      let v2443;
      switch (v2442[0]) {
        case 'None': {
          const v2444 = v2442[1];
          const v2446 = [false, stdlib.checkedBigNumberify('./index.rsh:112:25:decimal', stdlib.UInt_max, 0)];
          v2443 = v2446;
          
          break;
          }
        case 'Some': {
          const v2447 = v2442[1];
          const v2448 = v2447.wagerAmount;
          const v2450 = [true, v2448];
          v2443 = v2450;
          
          break;
          }
        }
      const v2451 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2313), null);
      let v2452;
      switch (v2451[0]) {
        case 'None': {
          const v2453 = v2451[1];
          const v2455 = [false, stdlib.checkedBigNumberify('./index.rsh:124:25:decimal', stdlib.UInt_max, 0)];
          v2452 = v2455;
          
          break;
          }
        case 'Some': {
          const v2456 = v2451[1];
          const v2457 = v2456.wagerAmount;
          const v2459 = [true, v2457];
          v2452 = v2459;
          
          break;
          }
        }
      const v2460 = v2443[stdlib.checkedBigNumberify('./index.rsh:133:49:array ref', stdlib.UInt_max, 0)];
      const v2461 = v2452[stdlib.checkedBigNumberify('./index.rsh:133:72:array ref', stdlib.UInt_max, 0)];
      const v2462 = v2460 ? true : v2461;
      const v2463 = await txn1.getOutput('api', 'v2462', ctc2, v2462);
      if (v558) {
        stdlib.protect(ctc0, await interact.out(v2309, v2463), {
          at: './index.rsh:74:8:application',
          fs: ['at ./index.rsh:74:8:application call to [unknown function] (defined at: ./index.rsh:74:8:function exp)', 'at ./index.rsh:133:32:application call to "timeoutAnnouncementResult" (defined at: ./index.rsh:107:5:function exp)', 'at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
          msg: 'out',
          who: 'contractAPI_timeoutAnnouncement'
          });
        }
      else {
        }
      
      const v2470 = v2460 ? v2461 : false;
      if (v2470) {
        const v2472 = v2443[stdlib.checkedBigNumberify('./index.rsh:136:47:array ref', stdlib.UInt_max, 1)];
        const v2473 = v2452[stdlib.checkedBigNumberify('./index.rsh:136:69:array ref', stdlib.UInt_max, 1)];
        const v2474 = stdlib.add(v2472, v2473);
        const v2475 = stdlib.ge(v882, v2474);
        stdlib.assert(v2475, {
          at: './index.rsh:136:16:application',
          fs: ['at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
          msg: null,
          who: 'contractAPI_timeoutAnnouncement'
          });
        const v2480 = stdlib.sub(v882, v2472);
        ;
        const v2485 = stdlib.sub(v2480, v2473);
        ;
        map0[v2312] = undefined;
        map0[v2313] = undefined;
        const v2488 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:142:101:decimal', stdlib.UInt_max, 2));
        const v2492 = stdlib.sub(v871, v2474);
        const v29710 = v2488;
        const v29711 = v2492;
        const v29713 = v873;
        const v29714 = v874;
        const v29715 = v875;
        const v29717 = v2485;
        const v29718 = v883;
        return;
        }
      else {
        if (v2460) {
          const v2502 = v2443[stdlib.checkedBigNumberify('./index.rsh:146:35:array ref', stdlib.UInt_max, 1)];
          const v2503 = stdlib.gt(v2502, stdlib.checkedBigNumberify('./index.rsh:146:42:decimal', stdlib.UInt_max, 0));
          const v2506 = stdlib.ge(v882, v2502);
          const v2507 = v2503 ? v2506 : false;
          stdlib.assert(v2507, {
            at: './index.rsh:146:16:application',
            fs: ['at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
            msg: null,
            who: 'contractAPI_timeoutAnnouncement'
            });
          const v2512 = stdlib.sub(v882, v2502);
          ;
          map0[v2312] = undefined;
          const v2513 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:149:101:decimal', stdlib.UInt_max, 1));
          const v2515 = stdlib.sub(v871, v2502);
          const v29719 = v2513;
          const v29720 = v2515;
          const v29722 = v873;
          const v29723 = v874;
          const v29724 = v875;
          const v29726 = v2512;
          const v29727 = v883;
          return;
          }
        else {
          if (v2461) {
            const v2525 = v2452[stdlib.checkedBigNumberify('./index.rsh:153:35:array ref', stdlib.UInt_max, 1)];
            const v2526 = stdlib.gt(v2525, stdlib.checkedBigNumberify('./index.rsh:153:42:decimal', stdlib.UInt_max, 0));
            const v2529 = stdlib.ge(v882, v2525);
            const v2530 = v2526 ? v2529 : false;
            stdlib.assert(v2530, {
              at: './index.rsh:153:16:application',
              fs: ['at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)'],
              msg: null,
              who: 'contractAPI_timeoutAnnouncement'
              });
            const v2535 = stdlib.sub(v882, v2525);
            ;
            map0[v2313] = undefined;
            const v2536 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:156:101:decimal', stdlib.UInt_max, 1));
            const v2538 = stdlib.sub(v871, v2525);
            const v29728 = v2536;
            const v29729 = v2538;
            const v29731 = v873;
            const v29732 = v874;
            const v29733 = v875;
            const v29735 = v2535;
            const v29736 = v883;
            return;
            }
          else {
            const v29737 = v870;
            const v29738 = v871;
            const v29740 = v873;
            const v29741 = v874;
            const v29742 = v875;
            const v29744 = v882;
            const v29745 = v883;
            return;
            }}}
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v2612 = v1110[1];
      return;
      break;
      }
    }
  
  
  };
export async function contractAPI_winnerAnnouncement(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_winnerAnnouncement expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_winnerAnnouncement expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc9 = stdlib.T_Tuple([ctc8]);
  const ctc10 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc11 = stdlib.T_Tuple([ctc10]);
  const ctc12 = stdlib.T_Tuple([]);
  const ctc13 = stdlib.T_Tuple([ctc6]);
  const ctc14 = stdlib.T_Tuple([ctc1, ctc6, ctc6]);
  const ctc15 = stdlib.T_Tuple([ctc14]);
  const ctc16 = stdlib.T_Data({
    contractAPI_deposit0: ctc11,
    contractAPI_exitLoop0: ctc12,
    contractAPI_optIn0: ctc12,
    contractAPI_resetPlayerWager0: ctc13,
    contractAPI_timeoutAnnouncement0: ctc15,
    contractAPI_winnerAnnouncement0: ctc9
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v850, v863, v870, v871, v873, v874, v875, v882, v883] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v977 = stdlib.protect(ctc9, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:164:5:application call to [unknown function] (defined at: ./index.rsh:164:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_winnerAnnouncement0" (defined at: ./index.rsh:163:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_winnerAnnouncement'
    });
  const v978 = v977[stdlib.checkedBigNumberify('./index.rsh:163:7:spread', stdlib.UInt_max, 0)];
  const v982 = v978[stdlib.checkedBigNumberify('./index.rsh:164:7:array', stdlib.UInt_max, 3)];
  const v986 = stdlib.gt(v982, stdlib.checkedBigNumberify('./index.rsh:164:87:decimal', stdlib.UInt_max, 0));
  const v987 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:164:94:decimal', stdlib.UInt_max, 2), v982);
  const v989 = stdlib.le(v987, v882);
  const v990 = v986 ? v989 : false;
  stdlib.assert(v990, {
    at: './index.rsh:164:77:application',
    fs: ['at ./index.rsh:164:5:application call to [unknown function] (defined at: ./index.rsh:164:66:function exp)', 'at ./index.rsh:164:5:application call to [unknown function] (defined at: ./index.rsh:164:5:function exp)', 'at ./index.rsh:46:17:application call to "runcontractAPI_winnerAnnouncement0" (defined at: ./index.rsh:163:7:function exp)', 'at ./index.rsh:46:17:application call to [unknown function] (defined at: ./index.rsh:46:17:function exp)'],
    msg: null,
    who: 'contractAPI_winnerAnnouncement'
    });
  
  const v1092 = ['contractAPI_winnerAnnouncement0', v977];
  
  const txn1 = await (ctc.sendrecv({
    args: [v850, v863, v870, v871, v873, v874, v875, v882, v883, v1092],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc16],
    pay: [stdlib.checkedBigNumberify('./index.rsh:165:70:decimal', stdlib.UInt_max, 0), [[stdlib.checkedBigNumberify('./index.rsh:165:73:decimal', stdlib.UInt_max, 0), v863]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
      
      switch (v1110[0]) {
        case 'contractAPI_deposit0': {
          const v1113 = v1110[1];
          
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v1415 = v1110[1];
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v1712 = v1110[1];
          
          break;
          }
        case 'contractAPI_resetPlayerWager0': {
          const v2009 = v1110[1];
          
          break;
          }
        case 'contractAPI_timeoutAnnouncement0': {
          const v2309 = v1110[1];
          
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v2612 = v1110[1];
          const v2613 = v2612[stdlib.checkedBigNumberify('./index.rsh:163:7:spread', stdlib.UInt_max, 0)];
          const v2615 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 1)];
          const v2616 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 2)];
          const v2617 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 3)];
          const v2618 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 4)];
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:165:70:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:165:73:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v863
            });
          undefined;
          const v2869 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v2615), null);
          let v2870;
          switch (v2869[0]) {
            case 'None': {
              const v2871 = v2869[1];
              v2870 = false;
              
              break;
              }
            case 'Some': {
              const v2872 = v2869[1];
              v2870 = true;
              
              break;
              }
            }
          const v2873 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v2616), null);
          let v2874;
          switch (v2873[0]) {
            case 'None': {
              const v2875 = v2873[1];
              v2874 = false;
              
              break;
              }
            case 'Some': {
              const v2876 = v2873[1];
              v2874 = true;
              
              break;
              }
            }
          const v2877 = v2870 ? v2874 : false;
          const v2878 = await txn1.getOutput('api', 'v2877', ctc2, v2877);
          
          if (v2877) {
            const v2884 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:192:19:decimal', stdlib.UInt_max, 2), v2617);
            const v2885 = stdlib.gt(v2884, stdlib.checkedBigNumberify('./index.rsh:192:30:decimal', stdlib.UInt_max, 0));
            const v2888 = stdlib.le(v2884, v882);
            const v2889 = v2885 ? v2888 : false;
            stdlib.assert(v2889, {
              at: './index.rsh:192:16:application',
              fs: ['at ./index.rsh:166:5:application call to [unknown function] (defined at: ./index.rsh:166:5:function exp)'],
              msg: null,
              who: 'contractAPI_winnerAnnouncement'
              });
            const v2891 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:193:31:decimal', stdlib.UInt_max, 2), v2618);
            const v2892 = stdlib.sub(v2884, v2891);
            const v2896 = stdlib.sub(v882, v2892);
            sim_r.txns.push({
              amt: v2892,
              kind: 'from',
              to: v2615,
              tok: undefined
              });
            stdlib.simMapSet(sim_r, 0, v2615, undefined);
            stdlib.simMapSet(sim_r, 0, v2616, undefined);
            const v2898 = stdlib.add(v873, v2891);
            const v2899 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:196:114:decimal', stdlib.UInt_max, 2));
            const v2901 = stdlib.sub(v871, v2884);
            const v29854 = v2899;
            const v29855 = v2901;
            const v29857 = v2898;
            const v29858 = v874;
            const v29859 = v875;
            const v29861 = v2896;
            const v29862 = v883;
            sim_r.isHalt = false;
            }
          else {
            const v29863 = v870;
            const v29864 = v871;
            const v29866 = v873;
            const v29867 = v874;
            const v29868 = v875;
            const v29870 = v882;
            const v29871 = v883;
            sim_r.isHalt = false;
            }
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc16],
    waitIfNotPresent: false
    }));
  const {data: [v1110], secs: v1112, time: v1111, didSend: v558, from: v1109 } = txn1;
  switch (v1110[0]) {
    case 'contractAPI_deposit0': {
      const v1113 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v1415 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_optIn0': {
      const v1712 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_resetPlayerWager0': {
      const v2009 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_timeoutAnnouncement0': {
      const v2309 = v1110[1];
      return;
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v2612 = v1110[1];
      const v2613 = v2612[stdlib.checkedBigNumberify('./index.rsh:163:7:spread', stdlib.UInt_max, 0)];
      const v2615 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 1)];
      const v2616 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 2)];
      const v2617 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 3)];
      const v2618 = v2613[stdlib.checkedBigNumberify('./index.rsh:165:7:array', stdlib.UInt_max, 4)];
      ;
      ;
      undefined;
      const v2869 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2615), null);
      let v2870;
      switch (v2869[0]) {
        case 'None': {
          const v2871 = v2869[1];
          v2870 = false;
          
          break;
          }
        case 'Some': {
          const v2872 = v2869[1];
          v2870 = true;
          
          break;
          }
        }
      const v2873 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v2616), null);
      let v2874;
      switch (v2873[0]) {
        case 'None': {
          const v2875 = v2873[1];
          v2874 = false;
          
          break;
          }
        case 'Some': {
          const v2876 = v2873[1];
          v2874 = true;
          
          break;
          }
        }
      const v2877 = v2870 ? v2874 : false;
      const v2878 = await txn1.getOutput('api', 'v2877', ctc2, v2877);
      if (v558) {
        stdlib.protect(ctc0, await interact.out(v2612, v2878), {
          at: './index.rsh:163:8:application',
          fs: ['at ./index.rsh:163:8:application call to [unknown function] (defined at: ./index.rsh:163:8:function exp)', 'at ./index.rsh:189:31:application call to "winnerAnnouncementResult" (defined at: ./index.rsh:166:5:function exp)', 'at ./index.rsh:166:5:application call to [unknown function] (defined at: ./index.rsh:166:5:function exp)'],
          msg: 'out',
          who: 'contractAPI_winnerAnnouncement'
          });
        }
      else {
        }
      
      if (v2877) {
        const v2884 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:192:19:decimal', stdlib.UInt_max, 2), v2617);
        const v2885 = stdlib.gt(v2884, stdlib.checkedBigNumberify('./index.rsh:192:30:decimal', stdlib.UInt_max, 0));
        const v2888 = stdlib.le(v2884, v882);
        const v2889 = v2885 ? v2888 : false;
        stdlib.assert(v2889, {
          at: './index.rsh:192:16:application',
          fs: ['at ./index.rsh:166:5:application call to [unknown function] (defined at: ./index.rsh:166:5:function exp)'],
          msg: null,
          who: 'contractAPI_winnerAnnouncement'
          });
        const v2891 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:193:31:decimal', stdlib.UInt_max, 2), v2618);
        const v2892 = stdlib.sub(v2884, v2891);
        const v2896 = stdlib.sub(v882, v2892);
        ;
        map0[v2615] = undefined;
        map0[v2616] = undefined;
        const v2898 = stdlib.add(v873, v2891);
        const v2899 = stdlib.sub(v870, stdlib.checkedBigNumberify('./index.rsh:196:114:decimal', stdlib.UInt_max, 2));
        const v2901 = stdlib.sub(v871, v2884);
        const v29854 = v2899;
        const v29855 = v2901;
        const v29857 = v2898;
        const v29858 = v874;
        const v29859 = v875;
        const v29861 = v2896;
        const v29862 = v883;
        return;
        }
      else {
        const v29863 = v870;
        const v29864 = v871;
        const v29866 = v873;
        const v29867 = v874;
        const v29868 = v875;
        const v29870 = v882;
        const v29871 = v883;
        return;
        }
      break;
      }
    }
  
  
  };
const _ALGO = {
  appApproval: `#pragma version 5
txn RekeyTo
global ZeroAddress
==
assert
txn Lease
global ZeroAddress
==
assert
int 0
store 0
txn ApplicationID
bz alloc
byte base64()
app_global_get
dup
int 0
extract_uint64
store 1
int 8
extract_uint64
store 2
txn OnCompletion
int OptIn
==
bz normal
txn Sender
int 42
bzero
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
b checkSize
normal:
txn NumAppArgs
int 3
==
assert
txna ApplicationArgs 0
btoi
preamble:
// Handler 0
dup
int 0
==
bz l0_afterHandler0
pop
// check step
int 0
load 1
==
assert
// check time
txna ApplicationArgs 1
btoi
dup
int 0
==
swap
load 2
==
||
assert
byte base64()
pop
txna ApplicationArgs 2
dup
len
int 0
==
assert
pop
// "CheckPay"
// "./index.rsh:31:12:dot"
// "[]"
int 100000
dup
bz l1_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
global CurrentApplicationAddress
dig 1
gtxns Receiver
==
assert
l1_checkTxnK:
pop
// "CheckPay"
// "./index.rsh:31:12:dot"
// "[]"
txn Sender
int 1
bzero
dig 1
extract 0 32
app_global_put
pop
int 1
store 1
global Round
store 2
txn OnCompletion
int NoOp
==
assert
b updateState
l0_afterHandler0:
// Handler 1
dup
int 1
==
bz l2_afterHandler1
pop
// check step
int 1
load 1
==
assert
// check time
txna ApplicationArgs 1
btoi
dup
int 0
==
swap
load 2
==
||
assert
int 1
bzero
app_global_get
dup
store 255
pop
txna ApplicationArgs 2
dup
len
int 16
==
assert
dup
int 0
extract_uint64
store 254
dup
int 8
extract_uint64
store 253
pop
// Initializing token
int 100000
dup
bz l3_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
global CurrentApplicationAddress
dig 1
gtxns Receiver
==
assert
l3_checkTxnK:
pop
int 0
itxn_begin
itxn_field AssetAmount
int axfer
itxn_field TypeEnum
global CurrentApplicationAddress
itxn_field AssetReceiver
load 253
itxn_field XferAsset
itxn_submit
int 0
l4_makeTxnK:
pop
// "CheckPay"
// "./index.rsh:43:12:dot"
// "[]"
int 2000000
dup
bz l5_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
global CurrentApplicationAddress
dig 1
gtxns Receiver
==
assert
l5_checkTxnK:
pop
// Just "sender correct"
// "./index.rsh:43:12:dot"
// "[]"
load 255
txn Sender
==
assert
load 255
load 253
itob
concat
int 16
bzero
int 1
itob // bool
substring 7 8
concat
int 8
bzero
concat
int 8
bzero
concat
int 8
bzero
concat
global Round
itob
concat
byte base64(AAAAAAAehIA=)
concat
int 8
bzero
concat
b loopBody2
l2_afterHandler1:
l6_afterHandler2:
// Handler 3
dup
int 3
==
bz l7_afterHandler3
pop
// check step
int 4
load 1
==
assert
// check time
txna ApplicationArgs 1
btoi
dup
int 0
==
swap
load 2
==
||
assert
int 1
bzero
app_global_get
dup
extract 0 32
store 255
dup
int 32
extract_uint64
store 254
dup
int 40
extract_uint64
store 253
dup
int 48
extract_uint64
store 252
dup
int 56
extract_uint64
store 251
dup
int 64
extract_uint64
store 250
dup
int 72
extract_uint64
store 249
dup
int 80
extract_uint64
store 248
dup
int 88
extract_uint64
store 247
pop
txna ApplicationArgs 2
dup
len
int 121
==
assert
dup
store 246
pop
load 246
int 0
getbyte
int 0
==
bz l9_switchAftercontractAPI_deposit0
load 246
extract 1 40
dup
store 245
dup
store 244
int 0
extract_uint64
store 243
load 248
load 243
+
store 242
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
load 243
dup
bz l10_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
global CurrentApplicationAddress
dig 1
gtxns Receiver
==
assert
l10_checkTxnK:
pop
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
// Nothing
// "./index.rsh:59:14:application"
// "[at ./index.rsh:58:5:application call to [unknown function] (defined at: ./index.rsh:58:5:function exp)]"
load 243
int 0
>
assert
txn Sender
dup
int 1
bzero
app_local_get
swap
pop
dup
store 241
int 0
getbyte
int 0
==
bz l12_switchAfterNone
int 1
store 240
l12_switchAfterNone:
load 241
int 0
getbyte
int 1
==
bz l13_switchAfterSome
int 0
store 240
l13_switchAfterSome:
l11_switchK:
byte base64(AAAAAAAABHU=)
load 240
itob // bool
substring 7 8
concat
log // 9
load 240
txn Sender
dup
int 1
bzero
app_local_get
swap
pop
dup
store 239
int 0
getbyte
int 0
==
bz l15_switchAfterNone
txn Sender
dup
dup
int 1
bzero
app_local_get
swap
pop
byte base64(AQ==)
load 244
extract 8 32
int 1
itob // bool
substring 7 8
concat
load 243
itob
concat
concat
store 238
dup
pop
load 238
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 255
load 254
itob
concat
load 253
int 1
+
itob
load 252
load 243
+
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
int 1
+
itob
concat
load 249
load 243
+
itob
concat
global Round
itob
concat
load 242
itob
concat
load 247
itob
concat
b loopBody2
l15_switchAfterNone:
load 239
int 0
getbyte
int 1
==
bz l16_switchAfterSome
load 255
load 254
itob
concat
load 253
itob
load 252
load 243
+
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 242
itob
concat
load 247
itob
concat
b loopBody2
l16_switchAfterSome:
l14_switchK:
l9_switchAftercontractAPI_deposit0:
load 246
int 0
getbyte
int 1
==
bz l17_switchAftercontractAPI_exitLoop0
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
byte base64(AAAAAAAABcI=)
int 1
itob // bool
substring 7 8
concat
log // 9
int 1
load 255
load 254
itob
concat
load 253
itob
load 252
itob
concat
int 0
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
itob
concat
load 247
itob
concat
b loopBody2
l17_switchAftercontractAPI_exitLoop0:
load 246
int 0
getbyte
int 2
==
bz l18_switchAftercontractAPI_optIn0
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
byte base64(AAAAAAAABvM=)
int 1
itob // bool
substring 7 8
concat
log // 9
int 1
load 255
load 254
itob
concat
load 253
itob
load 252
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
itob
concat
load 247
itob
concat
b loopBody2
l18_switchAftercontractAPI_optIn0:
load 246
int 0
getbyte
int 3
==
bz l19_switchAftercontractAPI_resetPlayerWager0
load 246
extract 1 32
dup
store 245
store 244
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
load 244
dup
int 1
bzero
app_local_get
swap
pop
dup
store 243
int 0
getbyte
int 0
==
bz l21_switchAfterNone
int 0
itob // bool
substring 7 8
int 8
bzero
concat
store 242
l21_switchAfterNone:
load 243
int 0
getbyte
int 1
==
bz l22_switchAfterSome
load 243
extract 1 41
store 241
int 1
itob // bool
substring 7 8
load 241
extract 33 8
concat
store 242
l22_switchAfterSome:
l20_switchK:
byte base64(AAAAAAAACDE=)
int 1
itob // bool
substring 7 8
concat
log // 9
int 1
load 242
extract 0 1
btoi
bz l23_ifF
load 242
int 1
extract_uint64
store 241
// Nothing
// "./index.rsh:245:16:application"
// "[at ./index.rsh:233:5:application call to [unknown function] (defined at: ./index.rsh:233:5:function exp)]"
load 241
int 0
>
load 248
load 241
>=
&&
assert
load 241
dup
bz l24_makeTxnK
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
load 244
itxn_field Receiver
itxn_submit
int 0
l24_makeTxnK:
pop
load 255
load 254
itob
concat
load 253
int 1
-
itob
load 252
load 241
-
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
load 241
-
itob
concat
load 247
itob
concat
b loopBody2
l23_ifF:
load 255
load 254
itob
concat
load 253
itob
load 252
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
itob
concat
load 247
itob
concat
b loopBody2
l19_switchAftercontractAPI_resetPlayerWager0:
load 246
int 0
getbyte
int 4
==
bz l25_switchAftercontractAPI_timeoutAnnouncement0
load 246
extract 1 96
dup
store 245
dup
store 244
extract 32 32
store 243
load 244
extract 64 32
store 242
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
load 243
dup
int 1
bzero
app_local_get
swap
pop
dup
store 241
int 0
getbyte
int 0
==
bz l27_switchAfterNone
int 0
itob // bool
substring 7 8
int 8
bzero
concat
store 240
l27_switchAfterNone:
load 241
int 0
getbyte
int 1
==
bz l28_switchAfterSome
load 241
extract 1 41
store 239
int 1
itob // bool
substring 7 8
load 239
extract 33 8
concat
store 240
l28_switchAfterSome:
l26_switchK:
load 242
dup
int 1
bzero
app_local_get
swap
pop
dup
store 239
int 0
getbyte
int 0
==
bz l30_switchAfterNone
int 0
itob // bool
substring 7 8
int 8
bzero
concat
store 238
l30_switchAfterNone:
load 239
int 0
getbyte
int 1
==
bz l31_switchAfterSome
load 239
extract 1 41
store 237
int 1
itob // bool
substring 7 8
load 237
extract 33 8
concat
store 238
l31_switchAfterSome:
l29_switchK:
load 240
extract 0 1
btoi
store 237
load 238
extract 0 1
btoi
store 236
byte base64(AAAAAAAACZ4=)
load 237
load 236
||
itob // bool
substring 7 8
concat
log // 9
load 237
load 236
||
load 237
load 236
&&
bz l32_ifF
load 240
int 1
extract_uint64
store 235
load 238
int 1
extract_uint64
store 234
load 235
load 234
+
store 233
// Nothing
// "./index.rsh:136:16:application"
// "[at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)]"
load 248
load 233
>=
assert
load 235
dup
bz l33_makeTxnK
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
load 243
itxn_field Receiver
itxn_submit
int 0
l33_makeTxnK:
pop
load 234
dup
bz l34_makeTxnK
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
load 242
itxn_field Receiver
itxn_submit
int 0
l34_makeTxnK:
pop
load 243
dup
dup
int 1
bzero
app_local_get
swap
pop
int 42
bzero
store 232
dup
pop
load 232
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 242
dup
dup
int 1
bzero
app_local_get
swap
pop
int 42
bzero
store 232
dup
pop
load 232
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 255
load 254
itob
concat
load 253
int 2
-
itob
load 252
load 233
-
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
load 235
-
load 234
-
itob
concat
load 247
itob
concat
b loopBody2
l32_ifF:
load 237
bz l35_ifF
load 240
int 1
extract_uint64
store 235
// Nothing
// "./index.rsh:146:16:application"
// "[at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)]"
load 235
int 0
>
load 248
load 235
>=
&&
assert
load 235
dup
bz l36_makeTxnK
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
load 243
itxn_field Receiver
itxn_submit
int 0
l36_makeTxnK:
pop
load 243
dup
dup
int 1
bzero
app_local_get
swap
pop
int 42
bzero
store 234
dup
pop
load 234
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 255
load 254
itob
concat
load 253
int 1
-
itob
load 252
load 235
-
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
load 235
-
itob
concat
load 247
itob
concat
b loopBody2
l35_ifF:
load 236
bz l37_ifF
load 238
int 1
extract_uint64
store 235
// Nothing
// "./index.rsh:153:16:application"
// "[at ./index.rsh:107:5:application call to [unknown function] (defined at: ./index.rsh:107:5:function exp)]"
load 235
int 0
>
load 248
load 235
>=
&&
assert
load 235
dup
bz l38_makeTxnK
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
load 242
itxn_field Receiver
itxn_submit
int 0
l38_makeTxnK:
pop
load 242
dup
dup
int 1
bzero
app_local_get
swap
pop
int 42
bzero
store 234
dup
pop
load 234
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 255
load 254
itob
concat
load 253
int 1
-
itob
load 252
load 235
-
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
load 235
-
itob
concat
load 247
itob
concat
b loopBody2
l37_ifF:
load 255
load 254
itob
concat
load 253
itob
load 252
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
itob
concat
load 247
itob
concat
b loopBody2
l25_switchAftercontractAPI_timeoutAnnouncement0:
load 246
int 0
getbyte
int 5
==
bz l39_switchAftercontractAPI_winnerAnnouncement0
load 246
extract 1 120
dup
store 245
dup
store 244
extract 32 32
store 243
load 244
extract 64 32
store 242
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
// "CheckPay"
// "./index.rsh:46:17:dot"
// "[]"
load 243
dup
int 1
bzero
app_local_get
swap
pop
dup
store 241
int 0
getbyte
int 0
==
bz l41_switchAfterNone
int 0
store 240
l41_switchAfterNone:
load 241
int 0
getbyte
int 1
==
bz l42_switchAfterSome
int 1
store 240
l42_switchAfterSome:
l40_switchK:
load 242
dup
int 1
bzero
app_local_get
swap
pop
dup
store 239
int 0
getbyte
int 0
==
bz l44_switchAfterNone
int 0
store 238
l44_switchAfterNone:
load 239
int 0
getbyte
int 1
==
bz l45_switchAfterSome
int 1
store 238
l45_switchAfterSome:
l43_switchK:
load 240
load 238
&&
store 237
byte base64(AAAAAAAACz0=)
load 237
itob // bool
substring 7 8
concat
log // 9
load 237
dup
bz l46_ifF
int 2
load 244
int 96
extract_uint64
*
store 236
// Nothing
// "./index.rsh:192:16:application"
// "[at ./index.rsh:166:5:application call to [unknown function] (defined at: ./index.rsh:166:5:function exp)]"
load 236
int 0
>
load 236
load 248
<=
&&
assert
int 2
load 244
int 104
extract_uint64
*
store 235
load 236
load 235
-
dup
store 234
dup
bz l47_makeTxnK
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
load 243
itxn_field Receiver
itxn_submit
int 0
l47_makeTxnK:
pop
load 243
dup
dup
int 1
bzero
app_local_get
swap
pop
int 42
bzero
store 233
dup
pop
load 233
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 242
dup
dup
int 1
bzero
app_local_get
swap
pop
int 42
bzero
store 233
dup
pop
load 233
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 255
load 254
itob
concat
load 253
int 2
-
itob
load 252
load 236
-
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
load 235
+
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
load 234
-
itob
concat
load 247
itob
concat
b loopBody2
l46_ifF:
load 255
load 254
itob
concat
load 253
itob
load 252
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
itob
concat
load 247
itob
concat
b loopBody2
l39_switchAftercontractAPI_winnerAnnouncement0:
l8_switchK:
l7_afterHandler3:
int 0
assert
loopBody2:
dup
int 0
extract_uint64
store 255
dup
int 8
extract_uint64
store 254
dup
extract 16 1
btoi
store 253
dup
int 17
extract_uint64
store 252
dup
int 25
extract_uint64
store 251
dup
int 33
extract_uint64
store 250
dup
int 41
extract_uint64
store 249
dup
int 49
extract_uint64
store 248
dup
int 57
extract_uint64
store 247
pop
dup
extract 0 32
store 246
dup
int 32
extract_uint64
store 245
pop
load 253
bz l48_ifF
load 246
load 245
itob
concat
load 255
itob
concat
load 254
itob
concat
load 252
itob
concat
load 251
itob
concat
load 250
itob
concat
load 248
itob
concat
load 247
itob
concat
int 1
bzero
dig 1
extract 0 96
app_global_put
pop
int 4
store 1
global Round
store 2
txn OnCompletion
int NoOp
==
assert
b updateState
l48_ifF:
load 248
dup
bz l49_makeTxnK
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
load 246
itxn_field Receiver
itxn_submit
int 0
l49_makeTxnK:
pop
load 247
dup
bz l50_makeTxnK
itxn_begin
itxn_field AssetAmount
int axfer
itxn_field TypeEnum
load 246
itxn_field AssetReceiver
load 245
itxn_field XferAsset
itxn_submit
int 0
l50_makeTxnK:
pop
int 0
itxn_begin
itxn_field AssetAmount
int axfer
itxn_field TypeEnum
global CreatorAddress
itxn_field AssetCloseTo
global CurrentApplicationAddress
itxn_field AssetReceiver
load 245
itxn_field XferAsset
itxn_submit
int 0
l51_makeTxnK:
pop
int 0
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
global CreatorAddress
itxn_field CloseRemainderTo
global CurrentApplicationAddress
itxn_field Receiver
itxn_submit
int 0
l52_makeTxnK:
pop
txn OnCompletion
int DeleteApplication
==
assert
updateState:
byte base64()
load 1
itob
load 2
itob
concat
app_global_put
checkSize:
load 0
dup
dup
int 1
+
global GroupSize
==
assert
txn GroupIndex
==
assert
int 1000
*
txn Fee
<=
assert
done:
int 1
return
alloc:
txn OnCompletion
int NoOp
==
assert
int 0
store 1
int 0
store 2
b updateState
`,
  appClear: `#pragma version 5
int 0
`,
  mapDataKeys: 1,
  mapDataSize: 42,
  stateKeys: 1,
  stateSize: 96,
  unsupported: [],
  version: 6
  };
const _ETH = {
  ABI: `[
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "internalType": "struct T7",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "stateMutability": "payable",
    "type": "constructor"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "msg",
        "type": "uint256"
      }
    ],
    "name": "ReachError",
    "type": "error"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "indexed": false,
        "internalType": "struct T7",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_e0",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v862",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v863",
                "type": "address"
              }
            ],
            "internalType": "struct T11",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T12",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_e1",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "components": [
                  {
                    "internalType": "enum _enum_T21",
                    "name": "which",
                    "type": "uint8"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "internalType": "uint256",
                            "name": "elem0",
                            "type": "uint256"
                          },
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem1",
                            "type": "tuple"
                          }
                        ],
                        "internalType": "struct T13",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T14",
                    "name": "_contractAPI_deposit0",
                    "type": "tuple"
                  },
                  {
                    "internalType": "bool",
                    "name": "_contractAPI_exitLoop0",
                    "type": "bool"
                  },
                  {
                    "internalType": "bool",
                    "name": "_contractAPI_optIn0",
                    "type": "bool"
                  },
                  {
                    "components": [
                      {
                        "internalType": "address payable",
                        "name": "elem0",
                        "type": "address"
                      }
                    ],
                    "internalType": "struct T16",
                    "name": "_contractAPI_resetPlayerWager0",
                    "type": "tuple"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem0",
                            "type": "tuple"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem1",
                            "type": "address"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem2",
                            "type": "address"
                          }
                        ],
                        "internalType": "struct T17",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T18",
                    "name": "_contractAPI_timeoutAnnouncement0",
                    "type": "tuple"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem0",
                            "type": "tuple"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem1",
                            "type": "address"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem2",
                            "type": "address"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem3",
                            "type": "uint256"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem4",
                            "type": "uint256"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem5",
                            "type": "uint256"
                          }
                        ],
                        "internalType": "struct T19",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T20",
                    "name": "_contractAPI_winnerAnnouncement0",
                    "type": "tuple"
                  }
                ],
                "internalType": "struct T21",
                "name": "v1110",
                "type": "tuple"
              }
            ],
            "internalType": "struct T23",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T24",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_e3",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v1141",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v1141",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v1474",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v1474",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v1779",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v1779",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v2097",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v2097",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v2462",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v2462",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v2877",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v2877",
    "type": "event"
  },
  {
    "stateMutability": "payable",
    "type": "fallback"
  },
  {
    "inputs": [],
    "name": "ViewReader_read",
    "outputs": [
      {
        "components": [
          {
            "internalType": "bool",
            "name": "elem0",
            "type": "bool"
          },
          {
            "internalType": "uint256",
            "name": "elem1",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem2",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem3",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem4",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem5",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem6",
            "type": "uint256"
          }
        ],
        "internalType": "struct T3",
        "name": "",
        "type": "tuple"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "_reachCreationTime",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "_reachCurrentState",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      },
      {
        "internalType": "bytes",
        "name": "",
        "type": "bytes"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "_reachCurrentTime",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v862",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v863",
                "type": "address"
              }
            ],
            "internalType": "struct T11",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T12",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_m1",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "components": [
                  {
                    "internalType": "enum _enum_T21",
                    "name": "which",
                    "type": "uint8"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "internalType": "uint256",
                            "name": "elem0",
                            "type": "uint256"
                          },
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem1",
                            "type": "tuple"
                          }
                        ],
                        "internalType": "struct T13",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T14",
                    "name": "_contractAPI_deposit0",
                    "type": "tuple"
                  },
                  {
                    "internalType": "bool",
                    "name": "_contractAPI_exitLoop0",
                    "type": "bool"
                  },
                  {
                    "internalType": "bool",
                    "name": "_contractAPI_optIn0",
                    "type": "bool"
                  },
                  {
                    "components": [
                      {
                        "internalType": "address payable",
                        "name": "elem0",
                        "type": "address"
                      }
                    ],
                    "internalType": "struct T16",
                    "name": "_contractAPI_resetPlayerWager0",
                    "type": "tuple"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem0",
                            "type": "tuple"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem1",
                            "type": "address"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem2",
                            "type": "address"
                          }
                        ],
                        "internalType": "struct T17",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T18",
                    "name": "_contractAPI_timeoutAnnouncement0",
                    "type": "tuple"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem0",
                            "type": "tuple"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem1",
                            "type": "address"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem2",
                            "type": "address"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem3",
                            "type": "uint256"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem4",
                            "type": "uint256"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem5",
                            "type": "uint256"
                          }
                        ],
                        "internalType": "struct T19",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T20",
                    "name": "_contractAPI_winnerAnnouncement0",
                    "type": "tuple"
                  }
                ],
                "internalType": "struct T21",
                "name": "v1110",
                "type": "tuple"
              }
            ],
            "internalType": "struct T23",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T24",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_m3",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "elem0",
            "type": "uint256"
          },
          {
            "components": [
              {
                "internalType": "bytes32",
                "name": "elem0",
                "type": "bytes32"
              }
            ],
            "internalType": "struct T0",
            "name": "elem1",
            "type": "tuple"
          }
        ],
        "internalType": "struct T13",
        "name": "_a0",
        "type": "tuple"
      }
    ],
    "name": "contractAPI_deposit",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "contractAPI_exitLoop",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "contractAPI_optIn",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address payable",
        "name": "_a0",
        "type": "address"
      }
    ],
    "name": "contractAPI_resetPlayerWager",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "bytes32",
                "name": "elem0",
                "type": "bytes32"
              }
            ],
            "internalType": "struct T0",
            "name": "elem0",
            "type": "tuple"
          },
          {
            "internalType": "address payable",
            "name": "elem1",
            "type": "address"
          },
          {
            "internalType": "address payable",
            "name": "elem2",
            "type": "address"
          }
        ],
        "internalType": "struct T17",
        "name": "_a0",
        "type": "tuple"
      }
    ],
    "name": "contractAPI_timeoutAnnouncement",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "bytes32",
                "name": "elem0",
                "type": "bytes32"
              }
            ],
            "internalType": "struct T0",
            "name": "elem0",
            "type": "tuple"
          },
          {
            "internalType": "address payable",
            "name": "elem1",
            "type": "address"
          },
          {
            "internalType": "address payable",
            "name": "elem2",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "elem3",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem4",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem5",
            "type": "uint256"
          }
        ],
        "internalType": "struct T19",
        "name": "_a0",
        "type": "tuple"
      }
    ],
    "name": "contractAPI_winnerAnnouncement",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "stateMutability": "payable",
    "type": "receive"
  }
]`,
  Bytecode: `0x6080604052604051620033723803806200337283398101604081905262000026916200019c565b600080554360035560408051825181526020808401511515908201527ff6b2f582026eaf8fd1fe583a836da56a1b30b8bd595170ad494773aa9148b06e910160405180910390a16200007b34156008620000cc565b6040805160208082018352338083526001600081905543905583519182015290910160405160208183030381529060405260029080519060200190620000c3929190620000f6565b50505062000244565b81620000f25760405163100960cb60e01b81526004810182905260240160405180910390fd5b5050565b828054620001049062000207565b90600052602060002090601f01602090048101928262000128576000855562000173565b82601f106200014357805160ff191683800117855562000173565b8280016001018555821562000173579182015b828111156200017357825182559160200191906001019062000156565b506200018192915062000185565b5090565b5b8082111562000181576000815560010162000186565b600060408284031215620001af57600080fd5b604080519081016001600160401b0381118282101715620001e057634e487b7160e01b600052604160045260246000fd5b6040528251815260208301518015158114620001fb57600080fd5b60208201529392505050565b600181811c908216806200021c57607f821691505b602082108114156200023e57634e487b7160e01b600052602260045260246000fd5b50919050565b61311e80620002546000396000f3fe6080604052600436106100a55760003560e01c8063832307571161006157806383230757146101775780638376c5bd1461018c578063ab53f2c61461019f578063e0b74db3146101c2578063e2f100881461022f578063e5dc75de1461024257005b80630aec9b53146100ae5780631e93b0f1146100d857806327c19c1a146100f757806334f5d489146101175780634c671b7514610137578063793c1e141461015757005b366100ac57005b005b3480156100ba57600080fd5b506100c3610257565b60405190151581526020015b60405180910390f35b3480156100e457600080fd5b506003545b6040519081526020016100cf565b34801561010357600080fd5b506100c36101123660046129f9565b6102a4565b34801561012357600080fd5b506100c3610132366004612a2a565b6102f8565b34801561014357600080fd5b506100c3610152366004612aca565b610359565b34801561016357600080fd5b506100c3610172366004612b88565b6103b1565b34801561018357600080fd5b506001546100e9565b6100ac61019a366004612ba4565b610409565b3480156101ab57600080fd5b506101b461042d565b6040516100cf929190612be8565b3480156101ce57600080fd5b506101d76104ca565b6040516100cf9190600060e0820190508251151582526020830151602083015260408301516040830152606083015160608301526080830151608083015260a083015160a083015260c083015160c083015292915050565b6100ac61023d366004612c22565b610726565b34801561024e57600080fd5b506100c3610746565b60006102616122fd565b610269612332565b610271612351565b6000606082015260028152604080516020808201909252828152908301526102998284610796565b505060400151919050565b60006102ae6122fd565b6102b6612332565b6102be612351565b6040805160208082018352878252838101919091526000835281518082019092528282528301526102ef8284610796565b50505192915050565b60006103026122fd565b61030a612332565b610312612351565b60408051602080820183526001600160a01b0388168252608084019190915260038352815180820190925282825283015261034d8284610796565b50506060015192915050565b60006103636122fd565b61036b612332565b610373612351565b604080516020808201835287825260a08401919091526004835281518082019092528282528301526103a58284610796565b50506080015192915050565b60006103bb6122fd565b6103c3612332565b6103cb612351565b604080516020808201835287825260c08401919091526005835281518082019092528282528301526103fd8284610796565b505060a0015192915050565b6104116122fd565b61042961042336849003840184612c4b565b82611bf6565b5050565b60006060600054600280805461044290612cb1565b80601f016020809104026020016040519081016040528092919081815260200182805461046e90612cb1565b80156104bb5780601f10610490576101008083540402835291602001916104bb565b820191906000526020600020905b81548152906001019060200180831161049e57829003601f168201915b50505050509050915091509091565b61050c6040518060e001604052806000151581526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b600160005414156105c75760006002805461052690612cb1565b80601f016020809104026020016040519081016040528092919081815260200182805461055290612cb1565b801561059f5780601f106105745761010080835404028352916020019161059f565b820191906000526020600020905b81548152906001019060200180831161058257829003601f168201915b50505050508060200190518101906105b79190612cf1565b90506105c560006007611dce565b505b60046000541415610717576000600280546105e190612cb1565b80601f016020809104026020016040519081016040528092919081815260200182805461060d90612cb1565b801561065a5780601f1061062f5761010080835404028352916020019161065a565b820191906000526020600020905b81548152906001019060200180831161063d57829003601f168201915b50505050508060200190518101906106729190612d1f565b90506106b86040805161010081018252600060208201818152928201819052606082018190526080820181905260a0820181905260c0820181905260e082015290815290565b80516001905260a08083015182516020015260c080840151835160409081019190915260808086015185516060908101919091529186015185519091015284015183519092019190915260e09092015181519092019190915251919050565b61072360006007611dce565b90565b61072e6122fd565b61042961074036849003840184612e62565b82610796565b60006107506122fd565b610758612332565b610760612351565b60006040820152600181819052506040805160208082019092528281529083015261078b8284610796565b505060200151919050565b6107a6600460005414601f611dce565b81516107c19015806107ba57508251600154145b6020611dce565b6000808055600280546107d390612cb1565b80601f01602080910402602001604051908101604052809291908181526020018280546107ff90612cb1565b801561084c5780601f106108215761010080835404028352916020019161084c565b820191906000526020600020905b81548152906001019060200180831161082f57829003601f168201915b50505050508060200190518101906108649190612d1f565b905061086e612409565b7f8dfb0d5eef6b13ecbd176c390cb7bcfcd23d562adace32565817b18172317b6a8460405161089d9190612f37565b60405180910390a160006020850151515160058111156108bf576108bf612c35565b1415610c0d57602080850151510151808252515160e08301516108e29190613061565b6020820152805151516108f8903414600d611dce565b61091261090b3384602001516000611df4565b600e611dce565b80515151610923901515600f611dce565b61092c33611e0c565b6040820181905251600090600181111561094857610948612c35565b141561095a5760016060820152610981565b6001604082015151600181111561097357610973612c35565b141561098157600060608201525b7f2fd867a4d4602ce0631d0cbd316b786f5692a9a28c3ba29cc16e221d32d405c781606001516040516109b8911515815260200190565b60405180910390a16060810151151583526109d233611e0c565b608082018190525160009060018111156109ee576109ee612c35565b1415610b3e5780515160209081015160a083018051919091528051600190830181905283515151825160409081019190915233600090815260048552819020805460ff199081168417825593518051519382019390935593820151600285018054909416901515179092550151600390910155610a69612702565b825181516001600160a01b039182169052602080850151835192169101526040830151610a9890600190613061565b602082015152815151516060840151610ab19190613061565b6020808301805190910191909152805160016040909101819052608085015191516060019190915260a0840151610ae89190613061565b6020820151608001528151515160c0840151610b049190613061565b6020808301805160a0019290925281514360c090910152830151815160e001526101008085015191510152610b3881611efb565b50611bf0565b60016080820151516001811115610b5757610b57612c35565b1415610c0857610b65612702565b825181516001600160a01b039182169052602080850151835192169181019190915260408401519082015152815151516060840151610ba49190613061565b602080830180518201929092528151600160409091015260808086015183516060015260a08087015184519092019190915260c080870151845190920191909152825143910152830151815160e001526101008085015191510152610b3881611efb565b611bf0565b6001602085015151516005811115610c2757610c27612c35565b1415610d3657610c3934156010611dce565b610c53610c4c3384602001516000611df4565b6011611dce565b604051600181527f71b394be105e653e5548face0f7e817ec81cfe5b1fb8b7306550b434a81189029060200160405180910390a160016020840152610c96612702565b825181516001600160a01b03918216905260208085015183519216918101919091526040808501518284018051919091526060808701518251909401939093528051600092019190915260808086015182519093019290925260a08086015182519093019290925260c08086015182519093019290925280514392019190915260e0808501518251909101526101008085015191510152610b3881611efb565b6002602085015151516005811115610d5057610d50612c35565b1415610e5f57610d6234156012611dce565b610d7c610d753384602001516000611df4565b6013611dce565b604051600181527f92c2ef66374fd62e3b1a8a197f8f3d93506d3a0c4cbdd7035aa9dae6df8e50e69060200160405180910390a160016040840152610dbf612702565b825181516001600160a01b03918216905260208085015183519216918101919091526040808501518284018051919091526060808701518251909401939093528051600192019190915260808086015182519093019290925260a08086015182519093019290925260c08086015182519093019290925280514392019190915260e0808501518251909101526101008085015191510152610b3881611efb565b6003602085015151516005811115610e7957610e79612c35565b14156110f9576020840151516080015160c0820152610e9a34156014611dce565b610eb4610ead3384602001516000611df4565b6015611dce565b60c081015151610ec390611e0c565b60e08201819052516000906001811115610edf57610edf612c35565b1415610f04576101208101805160009081905281516020015251610100820152610f55565b600160e0820151516001811115610f1d57610f1d612c35565b1415610f555760e081015160409081015161014083019081526101608301805160019052905190910151815160200152516101008201525b604051600181527f9ebb1cc6b04ada4c9208d9df91fe81ab61b208bb629150089d3a041b8a879b469060200160405180910390a16001606084015261010081015151156110f157610fd060008261010001516020015111610fb7576000610fc9565b816101000151602001518360e0015110155b6016611dce565b60c081015151610100820151602001516040516001600160a01b039092169181156108fc0291906000818181858888f19350505050158015611016573d6000803e3d6000fd5b5061101f612702565b825181516001600160a01b03918216905260208085015183519216910152604083015161104e90600190613079565b602080830151919091526101008301510151606084015161106f9190613079565b602080830180518201929092528151600160409091015260808086015183516060015260a08087015184519092019190915260c080870151845190920191909152915143920191909152610100830151015160e08401516110d09190613079565b60208201805160e001919091526101008085015191510152610b3881611efb565b610dbf612702565b600460208501515151600581111561111357611113612c35565b141561185f5760208401515160a0015161018082015261113534156017611dce565b61114f6111483384602001516000611df4565b6018611dce565b610180810151516020015161116390611e0c565b6101a0820181905251600090600181111561118057611180612c35565b14156111a5576101e081018051600090819052815160200152516101c08201526111f8565b60016101a08201515160018111156111bf576111bf612c35565b14156111f8576101a081015160409081015161020083019081526102208301805160019052905190910151815160200152516101c08201525b610180810151516040015161120c90611e0c565b610240820181905251600090600181111561122957611229612c35565b141561124e5761028081018051600090819052815160200152516102608201526112a1565b600161024082015151600181111561126857611268612c35565b14156112a1576102408101516040908101516102a083019081526102c08301805160019052905190910151815160200152516102608201525b6101c0810151517feb4597d2019a3f14dfe09a6354e7451b130bed612710b26796ea2e9fccb9b8f3906112da57610260820151516112dd565b60015b604051901515815260200160405180910390a16101c081015151611307576102608101515161130a565b60015b151560808401526101c08101515161132357600061132b565b610260810151515b1561154a5780610260015160200151816101c001516020015161134e9190613061565b6102e0820181905260e08301516113689111156019611dce565b80610180015160000151602001516001600160a01b03166108fc826101c00151602001519081150290604051600060405180830381858888f193505050501580156113b7573d6000803e3d6000fd5b50610180810151516040908101516102608301516020015191516001600160a01b039091169180156108fc02916000818181858888f19350505050158015611403573d6000803e3d6000fd5b5061018081018051516020908101516001600160a01b03908116600090815260049092526040808320805461ffff19908116825560018083018690556002808401805460ff1990811690915560039485018890559751518501519095168652928520805490911681559182018490559181018054909416909355919091015561148a612702565b825181516001600160a01b0391821690526020808501518351921691015260408301516114b990600290613079565b6020820151526102e082015160608401516114d49190613079565b602080830180518201929092528151600160409091015260808086015183516060015260a08087015184519092019190915260c0808701518451909201919091529151439201919091526102608301518101516101c08401519091015160e08501516115409190613079565b6110d09190613079565b6101c081015151156116d75761158a6000826101c001516020015111611571576000611583565b816101c00151602001518360e0015110155b601a611dce565b80610180015160000151602001516001600160a01b03166108fc826101c00151602001519081150290604051600060405180830381858888f193505050501580156115d9573d6000803e3d6000fd5b50610180810151516020908101516001600160a01b03166000908152600490915260408120805461ffff191681556001810182905560028101805460ff1916905560030155611626612702565b825181516001600160a01b03918216905260208085015183519216910152604083015161165590600190613079565b602080830151919091526101c0830151015160608401516116769190613079565b602080830180518201929092528151600160409091015260808086015183516060015260a08087015184519092019190915260c0808701518451909201919091529151439201919091526101c0830151015160e08401516110d09190613079565b61026081015151156110f157611717600082610260015160200151116116fe576000611710565b816102600151602001518360e0015110155b601b611dce565b610180810151516040908101516102608301516020015191516001600160a01b039091169180156108fc02916000818181858888f19350505050158015611762573d6000803e3d6000fd5b50610180810151516040908101516001600160a01b031660009081526004602052908120805461ffff191681556001810182905560028101805460ff19169055600301556117ae612702565b825181516001600160a01b0391821690526020808501518351921691015260408301516117dd90600190613079565b60208083015191909152610260830151015160608401516117fe9190613079565b602080830180518201929092528151600160409091015260808086015183516060015260a08087015184519092019190915260c080870151845190920191909152915143920191909152610260830151015160e08401516110d09190613079565b600560208501515151600581111561187957611879612c35565b1415611bf05760208401515160c0015161030082015261189b3415601c611dce565b6118b56118ae3384602001516000611df4565b601d611dce565b61030081015151602001516118c990611e0c565b61032082018190525160009060018111156118e6576118e6612c35565b14156118f9576000610340820152611922565b600161032082015151600181111561191357611913612c35565b14156119225760016103408201525b610300810151516040015161193690611e0c565b610360820181905251600090600181111561195357611953612c35565b141561196657600061038082015261198f565b600161036082015151600181111561198057611980612c35565b141561198f5760016103808201525b8061034001516119a05760006119a7565b8061038001515b15156103a082018190526040519081527f5bb0fa12f5689c81a68083351587115a04a0daaac1bbf2240ea3fa43c7eb83f19060200160405180910390a16103a081018051151560a085015251156110f1576103008101515160600151611a0e906002613090565b6103c08201819052611a3a90611a25576000611a33565b8260e00151826103c0015111155b601e611dce565b6103008101515160800151611a50906002613090565b6103e082018190526103c0820151611a689190613079565b610400820181905261030082015151602001516040516001600160a01b039091169180156108fc02916000818181858888f19350505050158015611ab0573d6000803e3d6000fd5b5061030081018051516020908101516001600160a01b03908116600090815260049092526040808320805461ffff19908116825560018083018690556002808401805460ff19908116909155600394850188905597515185015190951686529285208054909116815591820184905591810180549094169093559190910155611b37612702565b825181516001600160a01b039182169052602080850151835192169101526040830151611b6690600290613079565b6020820151526103c08201516060840151611b819190613079565b60208083018051909101919091525160016040909101526103e08201516080840151611bad9190613061565b6020820180516060019190915260a08085015182516080015260c08086015183519092019190915290514391015261040082015160e08401516110d09190613079565b50505050565b611c06600160005414600b611dce565b8151611c21901580611c1a57508251600154145b600c611dce565b600080805560028054611c3390612cb1565b80601f0160208091040260200160405190810160405280929190818152602001828054611c5f90612cb1565b8015611cac5780601f10611c8157610100808354040283529160200191611cac565b820191906000526020600020905b815481529060010190602001808311611c8f57829003601f168201915b5050505050806020019051810190611cc49190612cf1565b90507f64e402695c69cb5e36a62d15ce0da10e549e9202f37e9905d2bc2b09411fd1c783604051611d18919081518152602091820151805183830152909101516001600160a01b0316604082015260600190565b60405180910390a1611d30621e848034146009611dce565b8051611d48906001600160a01b03163314600a611dce565b611d50612702565b815181516001600160a01b0391821690526020808601518101518351921691810191909152808201805160009081905281519092018290528051600160409091015280516060018290528051608001829052805160a00182905280514360c0909101528051621e848060e090910152516101000152611bf081611efb565b816104295760405163100960cb60e01b8152600481018290526024015b60405180910390fd5b6000611e0283853085612103565b90505b9392505050565b611e14612775565b60016001600160a01b03831660009081526004602052604090205460ff166001811115611e4357611e43612c35565b1415611eea576001600160a01b038216600090815260046020526040908190208151606081019092528054829060ff166001811115611e8457611e84612c35565b6001811115611e9557611e95612c35565b81528154610100900460ff908116151560208084019190915260408051608081018252600186015460608201908152815260028601549093161515918301919091526003909301548184015291015292915050565b60008152600160208201525b919050565b8060200151604001511561208957611f6a60405180610120016040528060006001600160a01b0316815260200160006001600160a01b03168152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b8151516001600160a01b0390811682528251602090810151909116818301528083018051516040808501919091528151830151606080860191909152825101516080808601919091528251015160a0808601919091528251015160c0850152815160e0908101519085015290516101009081015190840152600460005543600155516120609183910181516001600160a01b0390811682526020808401519091169082015260408083015190820152606080830151908201526080808301519082015260a0828101519082015260c0808301519082015260e0808301519082015261010091820151918101919091526101200190565b604051602081830303815290604052600290805190602001906120849291906127ae565b505050565b805151602082015160e001516040516001600160a01b039092169181156108fc0291906000818181858888f193505050501580156120cb573d6000803e3d6000fd5b50805160208082015191519083015161010001516120ea9291906121dd565b6000808055600181905561210090600290612832565b50565b604080516001600160a01b0385811660248301528481166044830152606480830185905283518084039091018152608490920183526020820180516001600160e01b03166323b872dd60e01b17905291516000928392839291891691839161216a916130af565b60006040518083038185875af1925050503d80600081146121a7576040519150601f19603f3d011682016040523d82523d6000602084013e6121ac565b606091505b50915091506121bd828260016121f1565b50808060200190518101906121d291906130cb565b979650505050505050565b6121e883838361222c565b61208457600080fd5b60608315612200575081611e05565b8251156122105782518084602001fd5b60405163100960cb60e01b815260048101839052602401611deb565b604080516001600160a01b038481166024830152604480830185905283518084039091018152606490920183526020820180516001600160e01b031663a9059cbb60e01b17905291516000928392839291881691839161228b916130af565b60006040518083038185875af1925050503d80600081146122c8576040519150601f19603f3d011682016040523d82523d6000602084013e6122cd565b606091505b50915091506122de828260026121f1565b50808060200190518101906122f391906130cb565b9695505050505050565b6040805160c081018252600080825260208201819052918101829052606081018290526080810182905260a081019190915290565b60405180604001604052806000815260200161234c61286c565b905290565b6040805160e08101909152806000815260200161236c61287f565b81526000602080830182905260408084018390528051918201905290815260608201526080016123c06040805160a08101825260006080820181815260208301908152928201819052606082015290815290565b815260200161234c6040805161010081018252600060e0820181815260208301908152928201819052606082018190526080820181905260a0820181905260c082015290815290565b60405180610420016040528061241d61287f565b815260200160008152602001612431612775565b815260006020820152604001612445612775565b8152604080516080810182526000606082018181528252602082810182905292820152910190815260408051602080820183526000825283015201612488612775565b81526020016124ac6040518060400160405280600015158152602001600081525090565b81526020016124d06040518060400160405280600015158152602001600081525090565b8152604080516080810182526000606082018181528252602080830182905282840182905280850192909252825180840184528181529182015291019081526020016125406040805160a08101825260006080820181815260208301908152928201819052606082015290815290565b815260200161254d612775565b81526020016125716040518060400160405280600015158152602001600081525090565b81526020016125956040518060400160405280600015158152602001600081525090565b8152604080516080810182526000606082018181528252602080830182905282840182905280850192909252825180840184528181529182015291019081526020016125df612775565b81526020016126036040518060400160405280600015158152602001600081525090565b81526020016126276040518060400160405280600015158152602001600081525090565b815260408051608081018252600060608201818152825260208083018290528284018290528085019290925282518084018452818152918201529101908152602001600081526020016126b46040805161010081018252600060e0820181815260208301908152928201819052606082018190526080820181905260a0820181905260c082015290815290565b81526020016126c1612775565b8152600060208201526040016126d5612775565b81526020016000151581526020016000151581526020016000815260200160008152602001600081525090565b6040805160808101825260009181018281526060820192909252908190815260200161234c60405180610120016040528060008152602001600081526020016000151581526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b60408051606080820183526000808352602080840182905284516080810186529283018281528352820181905281840152909182015290565b8280546127ba90612cb1565b90600052602060002090601f0160209004810192826127dc5760008555612822565b82601f106127f557805160ff1916838001178555612822565b82800160010185558215612822579182015b82811115612822578251825591602001919060010190612807565b5061282e929150612892565b5090565b50805461283e90612cb1565b6000825580601f1061284e575050565b601f0160209004906000526020600020908101906121009190612892565b604051806020016040528061234c612351565b604051806020016040528061234c6128a7565b5b8082111561282e5760008155600101612893565b60405180604001604052806000815260200161234c6040518060200160405280600080191681525090565b6040516020810167ffffffffffffffff8111828210171561290357634e487b7160e01b600052604160045260246000fd5b60405290565b6040805190810167ffffffffffffffff8111828210171561290357634e487b7160e01b600052604160045260246000fd5b604051610120810167ffffffffffffffff8111828210171561290357634e487b7160e01b600052604160045260246000fd5b60405160e0810167ffffffffffffffff8111828210171561290357634e487b7160e01b600052604160045260246000fd5b6000602082840312156129af57600080fd5b6129b76128d2565b9135825250919050565b6000604082840312156129d357600080fd5b6129db612909565b9050813581526129ee836020840161299d565b602082015292915050565b600060408284031215612a0b57600080fd5b611e0583836129c1565b6001600160a01b038116811461210057600080fd5b600060208284031215612a3c57600080fd5b8135611e0581612a15565b600060608284031215612a5957600080fd5b6040516060810181811067ffffffffffffffff82111715612a8a57634e487b7160e01b600052604160045260246000fd5b604052905080612a9a848461299d565b81526020830135612aaa81612a15565b60208201526040830135612abd81612a15565b6040919091015292915050565b600060608284031215612adc57600080fd5b611e058383612a47565b600060c08284031215612af857600080fd5b60405160c0810181811067ffffffffffffffff82111715612b2957634e487b7160e01b600052604160045260246000fd5b604052905080612b39848461299d565b81526020830135612b4981612a15565b60208201526040830135612b5c81612a15565b80604083015250606083013560608201526080830135608082015260a083013560a08201525092915050565b600060c08284031215612b9a57600080fd5b611e058383612ae6565b600060608284031215612bb657600080fd5b50919050565b60005b83811015612bd7578181015183820152602001612bbf565b83811115611bf05750506000910152565b8281526040602082015260008251806040840152612c0d816060850160208701612bbc565b601f01601f1916919091016060019392505050565b60006102008284031215612bb657600080fd5b634e487b7160e01b600052602160045260246000fd5b60008183036060811215612c5e57600080fd5b612c66612909565b833581526040601f1983011215612c7c57600080fd5b612c84612909565b9150602084013582526040840135612c9b81612a15565b6020838101919091528101919091529392505050565b600181811c90821680612cc557607f821691505b60208210811415612bb657634e487b7160e01b600052602260045260246000fd5b8051611ef681612a15565b600060208284031215612d0357600080fd5b612d0b6128d2565b8251612d1681612a15565b81529392505050565b60006101208284031215612d3257600080fd5b612d3a61293a565b612d4383612ce6565b8152612d5160208401612ce6565b602082015260408301516040820152606083015160608201526080830151608082015260a083015160a082015260c083015160c082015260e083015160e08201526101008084015181830152508091505092915050565b600060408284031215612dba57600080fd5b612dc26128d2565b9050612dce83836129c1565b815292915050565b801515811461210057600080fd5b8035611ef681612dd6565b600060208284031215612e0157600080fd5b612e096128d2565b90508135612dce81612a15565b600060608284031215612e2857600080fd5b612e306128d2565b9050612dce8383612a47565b600060c08284031215612e4e57600080fd5b612e566128d2565b9050612dce8383612ae6565b6000818303610200811215612e7657600080fd5b612e7e612909565b833581526101e0601f1983011215612e9557600080fd5b612e9d6128d2565b9150612ea761296c565b602085013560068110612eb957600080fd5b8152612ec88660408701612da8565b6020820152612ed960808601612de4565b6040820152612eea60a08601612de4565b6060820152612efc8660c08701612def565b6080820152612f0e8660e08701612e16565b60a0820152612f21866101408701612e3c565b60c0820152825260208101919091529392505050565b8151815260208201515180516102008301919060068110612f6857634e487b7160e01b600052602160045260246000fd5b806020850152506020810151612f8d6040850182518051825260209081015151910152565b506040810151801515608085015250606081015180151560a085015250608081015180516001600160a01b031660c08501525060a0810151805180515160e086015260208101516001600160a01b03908116610100870152604090910151166101208501525060c00151805180515161014085015260208101516001600160a01b0390811661016086015260408201511661018085015260608101516101a085015260808101516101c085015260a001516101e08401525092915050565b634e487b7160e01b600052601160045260246000fd5b600082198211156130745761307461304b565b500190565b60008282101561308b5761308b61304b565b500390565b60008160001904831182151516156130aa576130aa61304b565b500290565b600082516130c1818460208701612bbc565b9190910192915050565b6000602082840312156130dd57600080fd5b8151611e0581612dd656fea2646970667358221220128c61ca489a72184e5d43c0c7e7c65166e967e58f4a62e0cb0f70c862845fc264736f6c63430008090033`,
  BytecodeLen: 13170,
  Which: `oD`,
  version: 5,
  views: {
    ViewReader: {
      read: `ViewReader_read`
      }
    }
  };
export const _Connectors = {
  ALGO: _ALGO,
  ETH: _ETH
  };
export const _Participants = {
  "Deployer": Deployer,
  "contractAPI_deposit": contractAPI_deposit,
  "contractAPI_exitLoop": contractAPI_exitLoop,
  "contractAPI_optIn": contractAPI_optIn,
  "contractAPI_resetPlayerWager": contractAPI_resetPlayerWager,
  "contractAPI_timeoutAnnouncement": contractAPI_timeoutAnnouncement,
  "contractAPI_winnerAnnouncement": contractAPI_winnerAnnouncement
  };
export const _APIs = {
  contractAPI: {
    deposit: contractAPI_deposit,
    exitLoop: contractAPI_exitLoop,
    optIn: contractAPI_optIn,
    resetPlayerWager: contractAPI_resetPlayerWager,
    timeoutAnnouncement: contractAPI_timeoutAnnouncement,
    winnerAnnouncement: contractAPI_winnerAnnouncement
    }
  };
