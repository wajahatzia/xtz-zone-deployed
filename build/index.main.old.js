// Automatically generated with Reach 0.1.7 (1bd49d90)
/* eslint-disable */
export const _version = '0.1.7';
export const _versionHash = '0.1.7 (1bd49d90)';
export const _backendVersion = 6;

export function getExports(s) {
  const stdlib = s.reachStdlib;
  return {
    };
  };
export function _getViews(s, viewlib) {
  const stdlib = s.reachStdlib;
  const ctc0 = stdlib.T_Address;
  const ctc1 = stdlib.T_Token;
  const ctc2 = stdlib.T_UInt;
  const ctc3 = stdlib.T_Bool;
  const ctc4 = stdlib.T_Tuple([ctc3, ctc2, ctc2, ctc2, ctc2, ctc2, ctc2]);
  const ctc5 = stdlib.T_Null;
  const ctc6 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc7 = stdlib.T_Struct([['challengeId', ctc6], ['paid', ctc3], ['wagerAmount', ctc2]]);
  const ctc8 = stdlib.T_Data({
    None: ctc5,
    Some: ctc7
    });
  const map0_ctc = ctc8;
  
  
  return {
    infos: {
      ViewReader: {
        read: {
          decode: async (i, svs, args) => {
            if (stdlib.eq(i, stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 1))) {
              const [v419] = svs;
              stdlib.assert(false, 'illegal view')
              }
            if (stdlib.eq(i, stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4))) {
              const [v419, v432, v437, v438, v440, v441, v442, v449, v450] = svs;
              return (await ((async () => {
                
                const v457 = [true, v441, v442, v440, v437, v438, v449];
                
                return v457;}))(...args));
              }
            
            stdlib.assert(false, 'illegal view')
            },
          ty: ctc4
          }
        }
      },
    views: {
      1: [ctc0],
      4: [ctc0, ctc1, ctc2, ctc2, ctc2, ctc2, ctc2, ctc2, ctc2]
      }
    };
  
  };
export function _getMaps(s) {
  const stdlib = s.reachStdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Tuple([ctc5]);
  return {
    mapDataTy: ctc6
    };
  };
export async function Deployer(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for Deployer expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for Deployer expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Token;
  const ctc7 = stdlib.T_Tuple([ctc3, ctc6]);
  const ctc8 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc9 = stdlib.T_Tuple([ctc8]);
  const ctc10 = stdlib.T_Tuple([]);
  const ctc11 = stdlib.T_Address;
  const ctc12 = stdlib.T_Tuple([ctc1, ctc11, ctc11, ctc3, ctc3, ctc3]);
  const ctc13 = stdlib.T_Tuple([ctc12]);
  const ctc14 = stdlib.T_Data({
    contractAPI_deposit0: ctc9,
    contractAPI_exitLoop0: ctc10,
    contractAPI_optIn0: ctc10,
    contractAPI_winnerAnnouncement0: ctc13
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const txn1 = await (ctc.sendrecv({
    args: [],
    evt_cnt: 0,
    funcNum: 0,
    lct: stdlib.checkedBigNumberify('./index.rsh:28:12:dot', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [],
    pay: [stdlib.checkedBigNumberify('./index.rsh:decimal', stdlib.UInt_max, 0), []],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [], secs: v421, time: v420, didSend: v19, from: v419 } = txn1;
      
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify('./index.rsh:decimal', stdlib.UInt_max, 0),
        kind: 'to',
        tok: undefined
        });
      const v423 = await ctc.getContractAddress();
      const v424 = await ctc.getContractInfo();
      
      sim_r.isHalt = false;
      
      return sim_r;
      }),
    soloSend: true,
    timeoutAt: undefined,
    tys: [],
    waitIfNotPresent: false
    }));
  const {data: [], secs: v421, time: v420, didSend: v19, from: v419 } = txn1;
  ;
  const v423 = await ctc.getContractAddress();
  const v424 = await ctc.getContractInfo();
  stdlib.protect(ctc0, await interact.showCtcInfo(v424), {
    at: './index.rsh:34:29:application',
    fs: ['at ./index.rsh:33:16:application call to [unknown function] (defined at: ./index.rsh:33:20:function exp)'],
    msg: 'showCtcInfo',
    who: 'Deployer'
    });
  stdlib.protect(ctc0, await interact.showAddress(v423), {
    at: './index.rsh:35:29:application',
    fs: ['at ./index.rsh:33:16:application call to [unknown function] (defined at: ./index.rsh:33:20:function exp)'],
    msg: 'showAddress',
    who: 'Deployer'
    });
  const v427 = stdlib.protect(ctc7, await interact.getZoneToken(), {
    at: './index.rsh:36:65:application',
    fs: ['at ./index.rsh:33:16:application call to [unknown function] (defined at: ./index.rsh:33:20:function exp)'],
    msg: 'getZoneToken',
    who: 'Deployer'
    });
  const v428 = v427[stdlib.checkedBigNumberify('./index.rsh:36:15:array', stdlib.UInt_max, 0)];
  const v429 = v427[stdlib.checkedBigNumberify('./index.rsh:36:15:array', stdlib.UInt_max, 1)];
  
  const txn2 = await (ctc.sendrecv({
    args: [v419, v428, v429],
    evt_cnt: 2,
    funcNum: 1,
    lct: v420,
    onlyIf: true,
    out_tys: [ctc3, ctc6],
    pay: [stdlib.checkedBigNumberify('./index.rsh:decimal', stdlib.UInt_max, 0), []],
    sim_p: (async (txn2) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v431, v432], secs: v434, time: v433, didSend: v35, from: v430 } = txn2;
      
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
        kind: 'init',
        tok: v432
        });
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify('./index.rsh:decimal', stdlib.UInt_max, 0),
        kind: 'to',
        tok: undefined
        });
      const v436 = stdlib.addressEq(v419, v430);
      stdlib.assert(v436, {
        at: './index.rsh:40:12:dot',
        fs: [],
        msg: 'sender correct',
        who: 'Deployer'
        });
      const v437 = stdlib.checkedBigNumberify('./index.rsh:42:30:decimal', stdlib.UInt_max, 0);
      const v438 = stdlib.checkedBigNumberify('./index.rsh:42:32:decimal', stdlib.UInt_max, 0);
      const v439 = true;
      const v440 = stdlib.checkedBigNumberify('./index.rsh:42:28:decimal', stdlib.UInt_max, 0);
      const v441 = stdlib.checkedBigNumberify('./index.rsh:42:24:decimal', stdlib.UInt_max, 0);
      const v442 = stdlib.checkedBigNumberify('./index.rsh:42:26:decimal', stdlib.UInt_max, 0);
      const v443 = v433;
      const v449 = stdlib.checkedBigNumberify('./index.rsh:27:11:after expr stmt semicolon', stdlib.UInt_max, 0);
      const v450 = stdlib.checkedBigNumberify('./index.rsh:40:12:dot', stdlib.UInt_max, 0);
      
      if (await (async () => {
        
        return v439;})()) {
        sim_r.isHalt = false;
        }
      else {
        sim_r.txns.push({
          amt: v449,
          kind: 'from',
          to: v419,
          tok: undefined
          });
        sim_r.txns.push({
          amt: v450,
          kind: 'from',
          to: v419,
          tok: v432
          });
        sim_r.txns.push({
          kind: 'halt',
          tok: v432
          })
        sim_r.txns.push({
          kind: 'halt',
          tok: undefined
          })
        sim_r.isHalt = true;
        }
      return sim_r;
      }),
    soloSend: true,
    timeoutAt: undefined,
    tys: [ctc11, ctc3, ctc6],
    waitIfNotPresent: false
    }));
  const {data: [v431, v432], secs: v434, time: v433, didSend: v35, from: v430 } = txn2;
  ;
  ;
  const v436 = stdlib.addressEq(v419, v430);
  stdlib.assert(v436, {
    at: './index.rsh:40:12:dot',
    fs: [],
    msg: 'sender correct',
    who: 'Deployer'
    });
  let v437 = stdlib.checkedBigNumberify('./index.rsh:42:30:decimal', stdlib.UInt_max, 0);
  let v438 = stdlib.checkedBigNumberify('./index.rsh:42:32:decimal', stdlib.UInt_max, 0);
  let v439 = true;
  let v440 = stdlib.checkedBigNumberify('./index.rsh:42:28:decimal', stdlib.UInt_max, 0);
  let v441 = stdlib.checkedBigNumberify('./index.rsh:42:24:decimal', stdlib.UInt_max, 0);
  let v442 = stdlib.checkedBigNumberify('./index.rsh:42:26:decimal', stdlib.UInt_max, 0);
  let v443 = v433;
  let v449 = stdlib.checkedBigNumberify('./index.rsh:27:11:after expr stmt semicolon', stdlib.UInt_max, 0);
  let v450 = stdlib.checkedBigNumberify('./index.rsh:40:12:dot', stdlib.UInt_max, 0);
  
  while (await (async () => {
    
    return v439;})()) {
    const txn3 = await (ctc.recv({
      didSend: false,
      evt_cnt: 1,
      funcNum: 3,
      out_tys: [ctc14],
      timeoutAt: undefined,
      waitIfNotPresent: false
      }));
    const {data: [v549], secs: v551, time: v550, didSend: v276, from: v548 } = txn3;
    switch (v549[0]) {
      case 'contractAPI_deposit0': {
        const v552 = v549[1];
        const v553 = v552[stdlib.checkedBigNumberify('./index.rsh:92:7:spread', stdlib.UInt_max, 0)];
        const v554 = v553[stdlib.checkedBigNumberify('./index.rsh:96:7:array', stdlib.UInt_max, 0)];
        const v555 = v553[stdlib.checkedBigNumberify('./index.rsh:96:7:array', stdlib.UInt_max, 1)];
        const v571 = stdlib.add(v449, v554);
        ;
        ;
        undefined;
        const v578 = stdlib.gt(v554, stdlib.checkedBigNumberify('./index.rsh:98:24:decimal', stdlib.UInt_max, 0));
        stdlib.assert(v578, {
          at: './index.rsh:98:14:application',
          fs: ['at ./index.rsh:97:5:application call to [unknown function] (defined at: ./index.rsh:97:5:function exp)'],
          msg: null,
          who: 'Deployer'
          });
        const v579 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v548), null);
        let v580;
        switch (v579[0]) {
          case 'None': {
            const v581 = v579[1];
            v580 = true;
            
            break;
            }
          case 'Some': {
            const v582 = v579[1];
            v580 = false;
            
            break;
            }
          }
        await txn3.getOutput('api', 'v580', ctc2, v580);
        const v588 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v548), null);
        switch (v588[0]) {
          case 'None': {
            const v589 = v588[1];
            const v590 = {
              challengeId: v555,
              paid: true,
              wagerAmount: v554
              };
            map0[v548] = v590;
            const v591 = stdlib.add(v441, stdlib.checkedBigNumberify('./index.rsh:104:44:decimal', stdlib.UInt_max, 1));
            const v592 = stdlib.add(v442, v554);
            const v593 = stdlib.add(v437, stdlib.checkedBigNumberify('./index.rsh:104:113:decimal', stdlib.UInt_max, 1));
            const v594 = stdlib.add(v438, v554);
            const cv437 = v593;
            const cv438 = v594;
            const cv439 = true;
            const cv440 = v440;
            const cv441 = v591;
            const cv442 = v592;
            const cv443 = v550;
            const cv449 = v571;
            const cv450 = v450;
            
            v437 = cv437;
            v438 = cv438;
            v439 = cv439;
            v440 = cv440;
            v441 = cv441;
            v442 = cv442;
            v443 = cv443;
            v449 = cv449;
            v450 = cv450;
            
            continue;
            break;
            }
          case 'Some': {
            const v604 = v588[1];
            const v605 = stdlib.add(v438, v554);
            const cv437 = v437;
            const cv438 = v605;
            const cv439 = true;
            const cv440 = v440;
            const cv441 = v441;
            const cv442 = v442;
            const cv443 = v550;
            const cv449 = v571;
            const cv450 = v450;
            
            v437 = cv437;
            v438 = cv438;
            v439 = cv439;
            v440 = cv440;
            v441 = cv441;
            v442 = cv442;
            v443 = cv443;
            v449 = cv449;
            v450 = cv450;
            
            continue;
            break;
            }
          }
        break;
        }
      case 'contractAPI_exitLoop0': {
        const v688 = v549[1];
        ;
        ;
        undefined;
        const v747 = true;
        await txn3.getOutput('api', 'v747', ctc2, v747);
        const cv437 = v437;
        const cv438 = v438;
        const cv439 = false;
        const cv440 = v440;
        const cv441 = v441;
        const cv442 = v442;
        const cv443 = v550;
        const cv449 = v449;
        const cv450 = v450;
        
        v437 = cv437;
        v438 = cv438;
        v439 = cv439;
        v440 = cv440;
        v441 = cv441;
        v442 = cv442;
        v443 = cv443;
        v449 = cv449;
        v450 = cv450;
        
        continue;
        break;
        }
      case 'contractAPI_optIn0': {
        const v819 = v549[1];
        ;
        ;
        undefined;
        const v886 = true;
        await txn3.getOutput('api', 'v886', ctc2, v886);
        const cv437 = v437;
        const cv438 = v438;
        const cv439 = true;
        const cv440 = v440;
        const cv441 = v441;
        const cv442 = v442;
        const cv443 = v550;
        const cv449 = v449;
        const cv450 = v450;
        
        v437 = cv437;
        v438 = cv438;
        v439 = cv439;
        v440 = cv440;
        v441 = cv441;
        v442 = cv442;
        v443 = cv443;
        v449 = cv449;
        v450 = cv450;
        
        continue;
        break;
        }
      case 'contractAPI_winnerAnnouncement0': {
        const v950 = v549[1];
        const v951 = v950[stdlib.checkedBigNumberify('./index.rsh:49:7:spread', stdlib.UInt_max, 0)];
        const v953 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 1)];
        const v954 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 2)];
        const v955 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 3)];
        const v956 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 4)];
        ;
        ;
        undefined;
        const v1041 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v953), null);
        let v1042;
        switch (v1041[0]) {
          case 'None': {
            const v1043 = v1041[1];
            v1042 = false;
            
            break;
            }
          case 'Some': {
            const v1044 = v1041[1];
            v1042 = true;
            
            break;
            }
          }
        const v1045 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v954), null);
        let v1046;
        switch (v1045[0]) {
          case 'None': {
            const v1047 = v1045[1];
            v1046 = false;
            
            break;
            }
          case 'Some': {
            const v1048 = v1045[1];
            v1046 = true;
            
            break;
            }
          }
        const v1049 = v1042 ? v1046 : false;
        await txn3.getOutput('api', 'v1049', ctc2, v1049);
        if (v1049) {
          const v1056 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:78:19:decimal', stdlib.UInt_max, 2), v955);
          const v1057 = stdlib.gt(v1056, stdlib.checkedBigNumberify('./index.rsh:78:30:decimal', stdlib.UInt_max, 0));
          const v1060 = stdlib.le(v1056, v449);
          const v1061 = v1057 ? v1060 : false;
          stdlib.assert(v1061, {
            at: './index.rsh:78:16:application',
            fs: ['at ./index.rsh:52:5:application call to [unknown function] (defined at: ./index.rsh:52:5:function exp)'],
            msg: null,
            who: 'Deployer'
            });
          const v1063 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:79:31:decimal', stdlib.UInt_max, 2), v956);
          const v1064 = stdlib.sub(v1056, v1063);
          const v1068 = stdlib.sub(v449, v1064);
          ;
          map0[v953] = undefined;
          map0[v954] = undefined;
          const v1070 = stdlib.add(v440, v1063);
          const v1071 = stdlib.sub(v437, stdlib.checkedBigNumberify('./index.rsh:82:114:decimal', stdlib.UInt_max, 2));
          const v1073 = stdlib.sub(v438, v1056);
          const cv437 = v1071;
          const cv438 = v1073;
          const cv439 = true;
          const cv440 = v1070;
          const cv441 = v441;
          const cv442 = v442;
          const cv443 = v550;
          const cv449 = v1068;
          const cv450 = v450;
          
          v437 = cv437;
          v438 = cv438;
          v439 = cv439;
          v440 = cv440;
          v441 = cv441;
          v442 = cv442;
          v443 = cv443;
          v449 = cv449;
          v450 = cv450;
          
          continue;}
        else {
          const cv437 = v437;
          const cv438 = v438;
          const cv439 = true;
          const cv440 = v440;
          const cv441 = v441;
          const cv442 = v442;
          const cv443 = v550;
          const cv449 = v449;
          const cv450 = v450;
          
          v437 = cv437;
          v438 = cv438;
          v439 = cv439;
          v440 = cv440;
          v441 = cv441;
          v442 = cv442;
          v443 = cv443;
          v449 = cv449;
          v450 = cv450;
          
          continue;}
        break;
        }
      }
    
    }
  ;
  ;
  return;
  
  
  
  
  };
export async function contractAPI_deposit(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_deposit expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_deposit expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc9 = stdlib.T_Tuple([ctc8]);
  const ctc10 = stdlib.T_Tuple([]);
  const ctc11 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc12 = stdlib.T_Tuple([ctc11]);
  const ctc13 = stdlib.T_Data({
    contractAPI_deposit0: ctc9,
    contractAPI_exitLoop0: ctc10,
    contractAPI_optIn0: ctc10,
    contractAPI_winnerAnnouncement0: ctc12
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v419, v432, v437, v438, v440, v441, v442, v449, v450] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v493 = stdlib.protect(ctc9, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:93:5:application call to [unknown function] (defined at: ./index.rsh:93:5:function exp)', 'at ./index.rsh:42:17:application call to "runcontractAPI_deposit0" (defined at: ./index.rsh:92:7:function exp)', 'at ./index.rsh:42:17:application call to [unknown function] (defined at: ./index.rsh:42:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_deposit'
    });
  const v494 = v493[stdlib.checkedBigNumberify('./index.rsh:92:7:spread', stdlib.UInt_max, 0)];
  const v495 = v494[stdlib.checkedBigNumberify('./index.rsh:93:7:array', stdlib.UInt_max, 0)];
  const v498 = stdlib.gt(v495, stdlib.checkedBigNumberify('./index.rsh:94:24:decimal', stdlib.UInt_max, 0));
  stdlib.assert(v498, {
    at: './index.rsh:94:14:application',
    fs: ['at ./index.rsh:93:5:application call to [unknown function] (defined at: ./index.rsh:93:29:function exp)', 'at ./index.rsh:93:5:application call to [unknown function] (defined at: ./index.rsh:93:5:function exp)', 'at ./index.rsh:42:17:application call to "runcontractAPI_deposit0" (defined at: ./index.rsh:92:7:function exp)', 'at ./index.rsh:42:17:application call to [unknown function] (defined at: ./index.rsh:42:17:function exp)'],
    msg: null,
    who: 'contractAPI_deposit'
    });
  
  const v502 = ['contractAPI_deposit0', v493];
  
  const txn1 = await (ctc.sendrecv({
    args: [v419, v432, v437, v438, v440, v441, v442, v449, v450, v502],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc13],
    pay: [v495, [[stdlib.checkedBigNumberify('./index.rsh:96:40:decimal', stdlib.UInt_max, 0), v432]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v549], secs: v551, time: v550, didSend: v276, from: v548 } = txn1;
      
      switch (v549[0]) {
        case 'contractAPI_deposit0': {
          const v552 = v549[1];
          const v553 = v552[stdlib.checkedBigNumberify('./index.rsh:92:7:spread', stdlib.UInt_max, 0)];
          const v554 = v553[stdlib.checkedBigNumberify('./index.rsh:96:7:array', stdlib.UInt_max, 0)];
          const v555 = v553[stdlib.checkedBigNumberify('./index.rsh:96:7:array', stdlib.UInt_max, 1)];
          const v571 = stdlib.add(v449, v554);
          sim_r.txns.push({
            amt: v554,
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:96:40:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v432
            });
          undefined;
          const v578 = stdlib.gt(v554, stdlib.checkedBigNumberify('./index.rsh:98:24:decimal', stdlib.UInt_max, 0));
          stdlib.assert(v578, {
            at: './index.rsh:98:14:application',
            fs: ['at ./index.rsh:97:5:application call to [unknown function] (defined at: ./index.rsh:97:5:function exp)'],
            msg: null,
            who: 'contractAPI_deposit'
            });
          const v579 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v548), null);
          let v580;
          switch (v579[0]) {
            case 'None': {
              const v581 = v579[1];
              v580 = true;
              
              break;
              }
            case 'Some': {
              const v582 = v579[1];
              v580 = false;
              
              break;
              }
            }
          const v583 = await txn1.getOutput('api', 'v580', ctc2, v580);
          
          const v588 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v548), null);
          switch (v588[0]) {
            case 'None': {
              const v589 = v588[1];
              const v590 = {
                challengeId: v555,
                paid: true,
                wagerAmount: v554
                };
              stdlib.simMapSet(sim_r, 0, v548, v590);
              const v591 = stdlib.add(v441, stdlib.checkedBigNumberify('./index.rsh:104:44:decimal', stdlib.UInt_max, 1));
              const v592 = stdlib.add(v442, v554);
              const v593 = stdlib.add(v437, stdlib.checkedBigNumberify('./index.rsh:104:113:decimal', stdlib.UInt_max, 1));
              const v594 = stdlib.add(v438, v554);
              const v4670 = v593;
              const v4671 = v594;
              const v4673 = v440;
              const v4674 = v591;
              const v4675 = v592;
              const v4677 = v571;
              const v4678 = v450;
              sim_r.isHalt = false;
              
              break;
              }
            case 'Some': {
              const v604 = v588[1];
              const v605 = stdlib.add(v438, v554);
              const v4679 = v437;
              const v4680 = v605;
              const v4682 = v440;
              const v4683 = v441;
              const v4684 = v442;
              const v4686 = v571;
              const v4687 = v450;
              sim_r.isHalt = false;
              
              break;
              }
            }
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v688 = v549[1];
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v819 = v549[1];
          
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v950 = v549[1];
          
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc13],
    waitIfNotPresent: false
    }));
  const {data: [v549], secs: v551, time: v550, didSend: v276, from: v548 } = txn1;
  switch (v549[0]) {
    case 'contractAPI_deposit0': {
      const v552 = v549[1];
      const v553 = v552[stdlib.checkedBigNumberify('./index.rsh:92:7:spread', stdlib.UInt_max, 0)];
      const v554 = v553[stdlib.checkedBigNumberify('./index.rsh:96:7:array', stdlib.UInt_max, 0)];
      const v555 = v553[stdlib.checkedBigNumberify('./index.rsh:96:7:array', stdlib.UInt_max, 1)];
      const v571 = stdlib.add(v449, v554);
      ;
      ;
      undefined;
      const v578 = stdlib.gt(v554, stdlib.checkedBigNumberify('./index.rsh:98:24:decimal', stdlib.UInt_max, 0));
      stdlib.assert(v578, {
        at: './index.rsh:98:14:application',
        fs: ['at ./index.rsh:97:5:application call to [unknown function] (defined at: ./index.rsh:97:5:function exp)'],
        msg: null,
        who: 'contractAPI_deposit'
        });
      const v579 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v548), null);
      let v580;
      switch (v579[0]) {
        case 'None': {
          const v581 = v579[1];
          v580 = true;
          
          break;
          }
        case 'Some': {
          const v582 = v579[1];
          v580 = false;
          
          break;
          }
        }
      const v583 = await txn1.getOutput('api', 'v580', ctc2, v580);
      if (v276) {
        stdlib.protect(ctc0, await interact.out(v552, v583), {
          at: './index.rsh:92:8:application',
          fs: ['at ./index.rsh:92:8:application call to [unknown function] (defined at: ./index.rsh:92:8:function exp)', 'at ./index.rsh:99:20:application call to "depositResult" (defined at: ./index.rsh:97:5:function exp)', 'at ./index.rsh:97:5:application call to [unknown function] (defined at: ./index.rsh:97:5:function exp)'],
          msg: 'out',
          who: 'contractAPI_deposit'
          });
        }
      else {
        }
      
      const v588 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v548), null);
      switch (v588[0]) {
        case 'None': {
          const v589 = v588[1];
          const v590 = {
            challengeId: v555,
            paid: true,
            wagerAmount: v554
            };
          map0[v548] = v590;
          const v591 = stdlib.add(v441, stdlib.checkedBigNumberify('./index.rsh:104:44:decimal', stdlib.UInt_max, 1));
          const v592 = stdlib.add(v442, v554);
          const v593 = stdlib.add(v437, stdlib.checkedBigNumberify('./index.rsh:104:113:decimal', stdlib.UInt_max, 1));
          const v594 = stdlib.add(v438, v554);
          const v4670 = v593;
          const v4671 = v594;
          const v4673 = v440;
          const v4674 = v591;
          const v4675 = v592;
          const v4677 = v571;
          const v4678 = v450;
          return;
          
          break;
          }
        case 'Some': {
          const v604 = v588[1];
          const v605 = stdlib.add(v438, v554);
          const v4679 = v437;
          const v4680 = v605;
          const v4682 = v440;
          const v4683 = v441;
          const v4684 = v442;
          const v4686 = v571;
          const v4687 = v450;
          return;
          
          break;
          }
        }
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v688 = v549[1];
      return;
      break;
      }
    case 'contractAPI_optIn0': {
      const v819 = v549[1];
      return;
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v950 = v549[1];
      return;
      break;
      }
    }
  
  
  };
export async function contractAPI_exitLoop(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_exitLoop expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_exitLoop expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([]);
  const ctc9 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc10 = stdlib.T_Tuple([ctc9]);
  const ctc11 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc12 = stdlib.T_Tuple([ctc11]);
  const ctc13 = stdlib.T_Data({
    contractAPI_deposit0: ctc10,
    contractAPI_exitLoop0: ctc8,
    contractAPI_optIn0: ctc8,
    contractAPI_winnerAnnouncement0: ctc12
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v419, v432, v437, v438, v440, v441, v442, v449, v450] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v486 = stdlib.protect(ctc8, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:91:7:application call to [unknown function] (defined at: ./index.rsh:91:7:function exp)', 'at ./index.rsh:42:17:application call to "runcontractAPI_exitLoop0" (defined at: ./index.rsh:91:7:function exp)', 'at ./index.rsh:42:17:application call to [unknown function] (defined at: ./index.rsh:42:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_exitLoop'
    });
  
  const v515 = ['contractAPI_exitLoop0', v486];
  
  const txn1 = await (ctc.sendrecv({
    args: [v419, v432, v437, v438, v440, v441, v442, v449, v450, v515],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc13],
    pay: [stdlib.checkedBigNumberify('./index.rsh:91:7:decimal', stdlib.UInt_max, 0), [[stdlib.checkedBigNumberify('./index.rsh:48:12:decimal', stdlib.UInt_max, 0), v432]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v549], secs: v551, time: v550, didSend: v276, from: v548 } = txn1;
      
      switch (v549[0]) {
        case 'contractAPI_deposit0': {
          const v552 = v549[1];
          
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v688 = v549[1];
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:91:7:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:48:12:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v432
            });
          undefined;
          const v747 = true;
          const v748 = await txn1.getOutput('api', 'v747', ctc2, v747);
          
          sim_r.txns.push({
            amt: v449,
            kind: 'from',
            to: v419,
            tok: undefined
            });
          sim_r.txns.push({
            amt: v450,
            kind: 'from',
            to: v419,
            tok: v432
            });
          sim_r.txns.push({
            kind: 'halt',
            tok: v432
            })
          sim_r.txns.push({
            kind: 'halt',
            tok: undefined
            })
          sim_r.isHalt = true;
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v819 = v549[1];
          
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v950 = v549[1];
          
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc13],
    waitIfNotPresent: false
    }));
  const {data: [v549], secs: v551, time: v550, didSend: v276, from: v548 } = txn1;
  switch (v549[0]) {
    case 'contractAPI_deposit0': {
      const v552 = v549[1];
      return;
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v688 = v549[1];
      ;
      ;
      undefined;
      const v747 = true;
      const v748 = await txn1.getOutput('api', 'v747', ctc2, v747);
      if (v276) {
        stdlib.protect(ctc0, await interact.out(v688, v748), {
          at: './index.rsh:91:8:application',
          fs: ['at ./index.rsh:91:8:application call to [unknown function] (defined at: ./index.rsh:91:8:function exp)', 'at ./index.rsh:91:43:application call to "result" (defined at: ./index.rsh:91:23:function exp)', 'at ./index.rsh:91:23:application call to [unknown function] (defined at: ./index.rsh:91:23:function exp)'],
          msg: 'out',
          who: 'contractAPI_exitLoop'
          });
        }
      else {
        }
      
      ;
      ;
      return;
      
      break;
      }
    case 'contractAPI_optIn0': {
      const v819 = v549[1];
      return;
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v950 = v549[1];
      return;
      break;
      }
    }
  
  
  };
export async function contractAPI_optIn(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_optIn expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_optIn expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([]);
  const ctc9 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc10 = stdlib.T_Tuple([ctc9]);
  const ctc11 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc12 = stdlib.T_Tuple([ctc11]);
  const ctc13 = stdlib.T_Data({
    contractAPI_deposit0: ctc10,
    contractAPI_exitLoop0: ctc8,
    contractAPI_optIn0: ctc8,
    contractAPI_winnerAnnouncement0: ctc12
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v419, v432, v437, v438, v440, v441, v442, v449, v450] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v479 = stdlib.protect(ctc8, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:90:7:application call to [unknown function] (defined at: ./index.rsh:90:7:function exp)', 'at ./index.rsh:42:17:application call to "runcontractAPI_optIn0" (defined at: ./index.rsh:90:7:function exp)', 'at ./index.rsh:42:17:application call to [unknown function] (defined at: ./index.rsh:42:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_optIn'
    });
  
  const v523 = ['contractAPI_optIn0', v479];
  
  const txn1 = await (ctc.sendrecv({
    args: [v419, v432, v437, v438, v440, v441, v442, v449, v450, v523],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc13],
    pay: [stdlib.checkedBigNumberify('./index.rsh:90:7:decimal', stdlib.UInt_max, 0), [[stdlib.checkedBigNumberify('./index.rsh:48:12:decimal', stdlib.UInt_max, 0), v432]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v549], secs: v551, time: v550, didSend: v276, from: v548 } = txn1;
      
      switch (v549[0]) {
        case 'contractAPI_deposit0': {
          const v552 = v549[1];
          
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v688 = v549[1];
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v819 = v549[1];
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:90:7:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:48:12:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v432
            });
          undefined;
          const v886 = true;
          const v887 = await txn1.getOutput('api', 'v886', ctc2, v886);
          
          const v4697 = v437;
          const v4698 = v438;
          const v4700 = v440;
          const v4701 = v441;
          const v4702 = v442;
          const v4704 = v449;
          const v4705 = v450;
          sim_r.isHalt = false;
          
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v950 = v549[1];
          
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc13],
    waitIfNotPresent: false
    }));
  const {data: [v549], secs: v551, time: v550, didSend: v276, from: v548 } = txn1;
  switch (v549[0]) {
    case 'contractAPI_deposit0': {
      const v552 = v549[1];
      return;
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v688 = v549[1];
      return;
      break;
      }
    case 'contractAPI_optIn0': {
      const v819 = v549[1];
      ;
      ;
      undefined;
      const v886 = true;
      const v887 = await txn1.getOutput('api', 'v886', ctc2, v886);
      if (v276) {
        stdlib.protect(ctc0, await interact.out(v819, v887), {
          at: './index.rsh:90:8:application',
          fs: ['at ./index.rsh:90:8:application call to [unknown function] (defined at: ./index.rsh:90:8:function exp)', 'at ./index.rsh:90:40:application call to "result" (defined at: ./index.rsh:90:20:function exp)', 'at ./index.rsh:90:20:application call to [unknown function] (defined at: ./index.rsh:90:20:function exp)'],
          msg: 'out',
          who: 'contractAPI_optIn'
          });
        }
      else {
        }
      
      const v4697 = v437;
      const v4698 = v438;
      const v4700 = v440;
      const v4701 = v441;
      const v4702 = v442;
      const v4704 = v449;
      const v4705 = v450;
      return;
      
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v950 = v549[1];
      return;
      break;
      }
    }
  
  
  };
export async function contractAPI_winnerAnnouncement(ctcTop, interact) {
  if (typeof(ctcTop) !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(new Error(`The backend for contractAPI_winnerAnnouncement expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for contractAPI_winnerAnnouncement expects to receive an interact object as its second argument.`));}
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Null;
  const ctc1 = stdlib.T_Bytes(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 32));
  const ctc2 = stdlib.T_Bool;
  const ctc3 = stdlib.T_UInt;
  const ctc4 = stdlib.T_Struct([['challengeId', ctc1], ['paid', ctc2], ['wagerAmount', ctc3]]);
  const ctc5 = stdlib.T_Data({
    None: ctc0,
    Some: ctc4
    });
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Token;
  const ctc8 = stdlib.T_Tuple([ctc1, ctc6, ctc6, ctc3, ctc3, ctc3]);
  const ctc9 = stdlib.T_Tuple([ctc8]);
  const ctc10 = stdlib.T_Tuple([ctc3, ctc1]);
  const ctc11 = stdlib.T_Tuple([ctc10]);
  const ctc12 = stdlib.T_Tuple([]);
  const ctc13 = stdlib.T_Data({
    contractAPI_deposit0: ctc11,
    contractAPI_exitLoop0: ctc12,
    contractAPI_optIn0: ctc12,
    contractAPI_winnerAnnouncement0: ctc9
    });
  
  const map0 = {};
  const map0_ctc = ctc5;
  
  
  const [v419, v432, v437, v438, v440, v441, v442, v449, v450] = await ctc.getState(stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4), [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3]);
  const v460 = stdlib.protect(ctc9, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: ['at ./index.rsh:50:5:application call to [unknown function] (defined at: ./index.rsh:50:5:function exp)', 'at ./index.rsh:42:17:application call to "runcontractAPI_winnerAnnouncement0" (defined at: ./index.rsh:49:7:function exp)', 'at ./index.rsh:42:17:application call to [unknown function] (defined at: ./index.rsh:42:17:function exp)'],
    msg: 'in',
    who: 'contractAPI_winnerAnnouncement'
    });
  const v461 = v460[stdlib.checkedBigNumberify('./index.rsh:49:7:spread', stdlib.UInt_max, 0)];
  const v465 = v461[stdlib.checkedBigNumberify('./index.rsh:50:7:array', stdlib.UInt_max, 3)];
  const v469 = stdlib.gt(v465, stdlib.checkedBigNumberify('./index.rsh:50:87:decimal', stdlib.UInt_max, 0));
  const v470 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:50:94:decimal', stdlib.UInt_max, 2), v465);
  const v472 = stdlib.le(v470, v449);
  const v473 = v469 ? v472 : false;
  stdlib.assert(v473, {
    at: './index.rsh:50:77:application',
    fs: ['at ./index.rsh:50:5:application call to [unknown function] (defined at: ./index.rsh:50:66:function exp)', 'at ./index.rsh:50:5:application call to [unknown function] (defined at: ./index.rsh:50:5:function exp)', 'at ./index.rsh:42:17:application call to "runcontractAPI_winnerAnnouncement0" (defined at: ./index.rsh:49:7:function exp)', 'at ./index.rsh:42:17:application call to [unknown function] (defined at: ./index.rsh:42:17:function exp)'],
    msg: null,
    who: 'contractAPI_winnerAnnouncement'
    });
  
  const v531 = ['contractAPI_winnerAnnouncement0', v460];
  
  const txn1 = await (ctc.sendrecv({
    args: [v419, v432, v437, v438, v440, v441, v442, v449, v450, v531],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc13],
    pay: [stdlib.checkedBigNumberify('./index.rsh:51:70:decimal', stdlib.UInt_max, 0), [[stdlib.checkedBigNumberify('./index.rsh:51:73:decimal', stdlib.UInt_max, 0), v432]]],
    sim_p: (async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      stdlib.simMapDupe(sim_r, 0, map0);
      
      const {data: [v549], secs: v551, time: v550, didSend: v276, from: v548 } = txn1;
      
      switch (v549[0]) {
        case 'contractAPI_deposit0': {
          const v552 = v549[1];
          
          break;
          }
        case 'contractAPI_exitLoop0': {
          const v688 = v549[1];
          
          break;
          }
        case 'contractAPI_optIn0': {
          const v819 = v549[1];
          
          break;
          }
        case 'contractAPI_winnerAnnouncement0': {
          const v950 = v549[1];
          const v951 = v950[stdlib.checkedBigNumberify('./index.rsh:49:7:spread', stdlib.UInt_max, 0)];
          const v953 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 1)];
          const v954 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 2)];
          const v955 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 3)];
          const v956 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 4)];
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:51:70:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./index.rsh:51:73:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: v432
            });
          undefined;
          const v1041 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v953), null);
          let v1042;
          switch (v1041[0]) {
            case 'None': {
              const v1043 = v1041[1];
              v1042 = false;
              
              break;
              }
            case 'Some': {
              const v1044 = v1041[1];
              v1042 = true;
              
              break;
              }
            }
          const v1045 = stdlib.protect(map0_ctc, stdlib.simMapRef(sim_r, 0, v954), null);
          let v1046;
          switch (v1045[0]) {
            case 'None': {
              const v1047 = v1045[1];
              v1046 = false;
              
              break;
              }
            case 'Some': {
              const v1048 = v1045[1];
              v1046 = true;
              
              break;
              }
            }
          const v1049 = v1042 ? v1046 : false;
          const v1050 = await txn1.getOutput('api', 'v1049', ctc2, v1049);
          
          if (v1049) {
            const v1056 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:78:19:decimal', stdlib.UInt_max, 2), v955);
            const v1057 = stdlib.gt(v1056, stdlib.checkedBigNumberify('./index.rsh:78:30:decimal', stdlib.UInt_max, 0));
            const v1060 = stdlib.le(v1056, v449);
            const v1061 = v1057 ? v1060 : false;
            stdlib.assert(v1061, {
              at: './index.rsh:78:16:application',
              fs: ['at ./index.rsh:52:5:application call to [unknown function] (defined at: ./index.rsh:52:5:function exp)'],
              msg: null,
              who: 'contractAPI_winnerAnnouncement'
              });
            const v1063 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:79:31:decimal', stdlib.UInt_max, 2), v956);
            const v1064 = stdlib.sub(v1056, v1063);
            const v1068 = stdlib.sub(v449, v1064);
            sim_r.txns.push({
              amt: v1064,
              kind: 'from',
              to: v953,
              tok: undefined
              });
            stdlib.simMapSet(sim_r, 0, v953, undefined);
            stdlib.simMapSet(sim_r, 0, v954, undefined);
            const v1070 = stdlib.add(v440, v1063);
            const v1071 = stdlib.sub(v437, stdlib.checkedBigNumberify('./index.rsh:82:114:decimal', stdlib.UInt_max, 2));
            const v1073 = stdlib.sub(v438, v1056);
            const v4706 = v1071;
            const v4707 = v1073;
            const v4709 = v1070;
            const v4710 = v441;
            const v4711 = v442;
            const v4713 = v1068;
            const v4714 = v450;
            sim_r.isHalt = false;
            }
          else {
            const v4715 = v437;
            const v4716 = v438;
            const v4718 = v440;
            const v4719 = v441;
            const v4720 = v442;
            const v4722 = v449;
            const v4723 = v450;
            sim_r.isHalt = false;
            }
          break;
          }
        }
      return sim_r;
      }),
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc6, ctc7, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc3, ctc13],
    waitIfNotPresent: false
    }));
  const {data: [v549], secs: v551, time: v550, didSend: v276, from: v548 } = txn1;
  switch (v549[0]) {
    case 'contractAPI_deposit0': {
      const v552 = v549[1];
      return;
      break;
      }
    case 'contractAPI_exitLoop0': {
      const v688 = v549[1];
      return;
      break;
      }
    case 'contractAPI_optIn0': {
      const v819 = v549[1];
      return;
      break;
      }
    case 'contractAPI_winnerAnnouncement0': {
      const v950 = v549[1];
      const v951 = v950[stdlib.checkedBigNumberify('./index.rsh:49:7:spread', stdlib.UInt_max, 0)];
      const v953 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 1)];
      const v954 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 2)];
      const v955 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 3)];
      const v956 = v951[stdlib.checkedBigNumberify('./index.rsh:51:7:array', stdlib.UInt_max, 4)];
      ;
      ;
      undefined;
      const v1041 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v953), null);
      let v1042;
      switch (v1041[0]) {
        case 'None': {
          const v1043 = v1041[1];
          v1042 = false;
          
          break;
          }
        case 'Some': {
          const v1044 = v1041[1];
          v1042 = true;
          
          break;
          }
        }
      const v1045 = stdlib.protect(map0_ctc, stdlib.mapRef(map0, v954), null);
      let v1046;
      switch (v1045[0]) {
        case 'None': {
          const v1047 = v1045[1];
          v1046 = false;
          
          break;
          }
        case 'Some': {
          const v1048 = v1045[1];
          v1046 = true;
          
          break;
          }
        }
      const v1049 = v1042 ? v1046 : false;
      const v1050 = await txn1.getOutput('api', 'v1049', ctc2, v1049);
      if (v276) {
        stdlib.protect(ctc0, await interact.out(v950, v1050), {
          at: './index.rsh:49:8:application',
          fs: ['at ./index.rsh:49:8:application call to [unknown function] (defined at: ./index.rsh:49:8:function exp)', 'at ./index.rsh:75:31:application call to "winnerAnnouncementResult" (defined at: ./index.rsh:52:5:function exp)', 'at ./index.rsh:52:5:application call to [unknown function] (defined at: ./index.rsh:52:5:function exp)'],
          msg: 'out',
          who: 'contractAPI_winnerAnnouncement'
          });
        }
      else {
        }
      
      if (v1049) {
        const v1056 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:78:19:decimal', stdlib.UInt_max, 2), v955);
        const v1057 = stdlib.gt(v1056, stdlib.checkedBigNumberify('./index.rsh:78:30:decimal', stdlib.UInt_max, 0));
        const v1060 = stdlib.le(v1056, v449);
        const v1061 = v1057 ? v1060 : false;
        stdlib.assert(v1061, {
          at: './index.rsh:78:16:application',
          fs: ['at ./index.rsh:52:5:application call to [unknown function] (defined at: ./index.rsh:52:5:function exp)'],
          msg: null,
          who: 'contractAPI_winnerAnnouncement'
          });
        const v1063 = stdlib.mul(stdlib.checkedBigNumberify('./index.rsh:79:31:decimal', stdlib.UInt_max, 2), v956);
        const v1064 = stdlib.sub(v1056, v1063);
        const v1068 = stdlib.sub(v449, v1064);
        ;
        map0[v953] = undefined;
        map0[v954] = undefined;
        const v1070 = stdlib.add(v440, v1063);
        const v1071 = stdlib.sub(v437, stdlib.checkedBigNumberify('./index.rsh:82:114:decimal', stdlib.UInt_max, 2));
        const v1073 = stdlib.sub(v438, v1056);
        const v4706 = v1071;
        const v4707 = v1073;
        const v4709 = v1070;
        const v4710 = v441;
        const v4711 = v442;
        const v4713 = v1068;
        const v4714 = v450;
        return;
        }
      else {
        const v4715 = v437;
        const v4716 = v438;
        const v4718 = v440;
        const v4719 = v441;
        const v4720 = v442;
        const v4722 = v449;
        const v4723 = v450;
        return;
        }
      break;
      }
    }
  
  
  };
const _ALGO = {
  appApproval: `#pragma version 5
txn RekeyTo
global ZeroAddress
==
assert
txn Lease
global ZeroAddress
==
assert
int 0
store 0
txn ApplicationID
bz alloc
byte base64()
app_global_get
dup
int 0
extract_uint64
store 1
int 8
extract_uint64
store 2
txn OnCompletion
int OptIn
==
bz normal
txn Sender
int 42
bzero
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
b checkSize
normal:
txn NumAppArgs
int 3
==
assert
txna ApplicationArgs 0
btoi
preamble:
// Handler 0
dup
int 0
==
bz l0_afterHandler0
pop
// check step
int 0
load 1
==
assert
// check time
txna ApplicationArgs 1
btoi
dup
int 0
==
swap
load 2
==
||
assert
byte base64()
pop
txna ApplicationArgs 2
dup
len
int 0
==
assert
pop
// "CheckPay"
// "./index.rsh:28:12:dot"
// "[]"
int 100000
dup
bz l1_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
global CurrentApplicationAddress
dig 1
gtxns Receiver
==
assert
l1_checkTxnK:
pop
// "CheckPay"
// "./index.rsh:28:12:dot"
// "[]"
txn Sender
int 1
bzero
dig 1
extract 0 32
app_global_put
pop
int 1
store 1
global Round
store 2
txn OnCompletion
int NoOp
==
assert
b updateState
l0_afterHandler0:
// Handler 1
dup
int 1
==
bz l2_afterHandler1
pop
// check step
int 1
load 1
==
assert
// check time
txna ApplicationArgs 1
btoi
dup
int 0
==
swap
load 2
==
||
assert
int 1
bzero
app_global_get
dup
store 255
pop
txna ApplicationArgs 2
dup
len
int 16
==
assert
dup
int 0
extract_uint64
store 254
dup
int 8
extract_uint64
store 253
pop
// Initializing token
int 100000
dup
bz l3_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
global CurrentApplicationAddress
dig 1
gtxns Receiver
==
assert
l3_checkTxnK:
pop
int 0
itxn_begin
itxn_field AssetAmount
int axfer
itxn_field TypeEnum
global CurrentApplicationAddress
itxn_field AssetReceiver
load 253
itxn_field XferAsset
itxn_submit
int 0
l4_makeTxnK:
pop
// "CheckPay"
// "./index.rsh:40:12:dot"
// "[]"
// Just "sender correct"
// "./index.rsh:40:12:dot"
// "[]"
load 255
txn Sender
==
assert
load 255
load 253
itob
concat
int 16
bzero
int 1
itob // bool
substring 7 8
concat
int 8
bzero
concat
int 8
bzero
concat
int 8
bzero
concat
global Round
itob
concat
int 8
bzero
concat
int 8
bzero
concat
b loopBody2
l2_afterHandler1:
l5_afterHandler2:
// Handler 3
dup
int 3
==
bz l6_afterHandler3
pop
// check step
int 4
load 1
==
assert
// check time
txna ApplicationArgs 1
btoi
dup
int 0
==
swap
load 2
==
||
assert
int 1
bzero
app_global_get
dup
extract 0 32
store 255
dup
int 32
extract_uint64
store 254
dup
int 40
extract_uint64
store 253
dup
int 48
extract_uint64
store 252
dup
int 56
extract_uint64
store 251
dup
int 64
extract_uint64
store 250
dup
int 72
extract_uint64
store 249
dup
int 80
extract_uint64
store 248
dup
int 88
extract_uint64
store 247
pop
txna ApplicationArgs 2
dup
len
int 121
==
assert
dup
store 246
pop
load 246
int 0
getbyte
int 0
==
bz l8_switchAftercontractAPI_deposit0
load 246
extract 1 40
dup
store 245
dup
store 244
int 0
extract_uint64
store 243
load 248
load 243
+
store 242
// "CheckPay"
// "./index.rsh:42:17:dot"
// "[]"
load 243
dup
bz l9_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
global CurrentApplicationAddress
dig 1
gtxns Receiver
==
assert
l9_checkTxnK:
pop
// "CheckPay"
// "./index.rsh:42:17:dot"
// "[]"
// Nothing
// "./index.rsh:98:14:application"
// "[at ./index.rsh:97:5:application call to [unknown function] (defined at: ./index.rsh:97:5:function exp)]"
load 243
int 0
>
assert
txn Sender
dup
int 1
bzero
app_local_get
swap
pop
dup
store 241
int 0
getbyte
int 0
==
bz l11_switchAfterNone
int 1
store 240
l11_switchAfterNone:
load 241
int 0
getbyte
int 1
==
bz l12_switchAfterSome
int 0
store 240
l12_switchAfterSome:
l10_switchK:
byte base64(AAAAAAAAAkQ=)
load 240
itob // bool
substring 7 8
concat
log // 9
load 240
txn Sender
dup
int 1
bzero
app_local_get
swap
pop
dup
store 239
int 0
getbyte
int 0
==
bz l14_switchAfterNone
txn Sender
dup
dup
int 1
bzero
app_local_get
swap
pop
byte base64(AQ==)
load 244
extract 8 32
int 1
itob // bool
substring 7 8
concat
load 243
itob
concat
concat
store 238
dup
pop
load 238
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 255
load 254
itob
concat
load 253
int 1
+
itob
load 252
load 243
+
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
int 1
+
itob
concat
load 249
load 243
+
itob
concat
global Round
itob
concat
load 242
itob
concat
load 247
itob
concat
b loopBody2
l14_switchAfterNone:
load 239
int 0
getbyte
int 1
==
bz l15_switchAfterSome
load 255
load 254
itob
concat
load 253
itob
load 252
load 243
+
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 242
itob
concat
load 247
itob
concat
b loopBody2
l15_switchAfterSome:
l13_switchK:
l8_switchAftercontractAPI_deposit0:
load 246
int 0
getbyte
int 1
==
bz l16_switchAftercontractAPI_exitLoop0
// "CheckPay"
// "./index.rsh:42:17:dot"
// "[]"
// "CheckPay"
// "./index.rsh:42:17:dot"
// "[]"
byte base64(AAAAAAAAAus=)
int 1
itob // bool
substring 7 8
concat
log // 9
int 1
load 255
load 254
itob
concat
load 253
itob
load 252
itob
concat
int 0
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
itob
concat
load 247
itob
concat
b loopBody2
l16_switchAftercontractAPI_exitLoop0:
load 246
int 0
getbyte
int 2
==
bz l17_switchAftercontractAPI_optIn0
// "CheckPay"
// "./index.rsh:42:17:dot"
// "[]"
// "CheckPay"
// "./index.rsh:42:17:dot"
// "[]"
byte base64(AAAAAAAAA3Y=)
int 1
itob // bool
substring 7 8
concat
log // 9
int 1
load 255
load 254
itob
concat
load 253
itob
load 252
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
itob
concat
load 247
itob
concat
b loopBody2
l17_switchAftercontractAPI_optIn0:
load 246
int 0
getbyte
int 3
==
bz l18_switchAftercontractAPI_winnerAnnouncement0
load 246
extract 1 120
dup
store 245
dup
store 244
extract 32 32
store 243
load 244
extract 64 32
store 242
// "CheckPay"
// "./index.rsh:42:17:dot"
// "[]"
// "CheckPay"
// "./index.rsh:42:17:dot"
// "[]"
load 243
dup
int 1
bzero
app_local_get
swap
pop
dup
store 241
int 0
getbyte
int 0
==
bz l20_switchAfterNone
int 0
store 240
l20_switchAfterNone:
load 241
int 0
getbyte
int 1
==
bz l21_switchAfterSome
int 1
store 240
l21_switchAfterSome:
l19_switchK:
load 242
dup
int 1
bzero
app_local_get
swap
pop
dup
store 239
int 0
getbyte
int 0
==
bz l23_switchAfterNone
int 0
store 238
l23_switchAfterNone:
load 239
int 0
getbyte
int 1
==
bz l24_switchAfterSome
int 1
store 238
l24_switchAfterSome:
l22_switchK:
load 240
load 238
&&
store 237
byte base64(AAAAAAAABBk=)
load 237
itob // bool
substring 7 8
concat
log // 9
load 237
dup
bz l25_ifF
int 2
load 244
int 96
extract_uint64
*
store 236
// Nothing
// "./index.rsh:78:16:application"
// "[at ./index.rsh:52:5:application call to [unknown function] (defined at: ./index.rsh:52:5:function exp)]"
load 236
int 0
>
load 236
load 248
<=
&&
assert
int 2
load 244
int 104
extract_uint64
*
store 235
load 236
load 235
-
dup
store 234
dup
bz l26_makeTxnK
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
load 243
itxn_field Receiver
itxn_submit
int 0
l26_makeTxnK:
pop
load 243
dup
dup
int 1
bzero
app_local_get
swap
pop
int 42
bzero
store 233
dup
pop
load 233
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 242
dup
dup
int 1
bzero
app_local_get
swap
pop
int 42
bzero
store 233
dup
pop
load 233
swap
pop
dig 1
int 1
bzero
dig 2
extract 0 42
app_local_put
pop
pop
load 255
load 254
itob
concat
load 253
int 2
-
itob
load 252
load 236
-
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
load 235
+
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
load 234
-
itob
concat
load 247
itob
concat
b loopBody2
l25_ifF:
load 255
load 254
itob
concat
load 253
itob
load 252
itob
concat
int 1
itob // bool
substring 7 8
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
global Round
itob
concat
load 248
itob
concat
load 247
itob
concat
b loopBody2
l18_switchAftercontractAPI_winnerAnnouncement0:
l7_switchK:
l6_afterHandler3:
int 0
assert
loopBody2:
dup
int 0
extract_uint64
store 255
dup
int 8
extract_uint64
store 254
dup
extract 16 1
btoi
store 253
dup
int 17
extract_uint64
store 252
dup
int 25
extract_uint64
store 251
dup
int 33
extract_uint64
store 250
dup
int 41
extract_uint64
store 249
dup
int 49
extract_uint64
store 248
dup
int 57
extract_uint64
store 247
pop
dup
extract 0 32
store 246
dup
int 32
extract_uint64
store 245
pop
load 253
bz l27_ifF
load 246
load 245
itob
concat
load 255
itob
concat
load 254
itob
concat
load 252
itob
concat
load 251
itob
concat
load 250
itob
concat
load 248
itob
concat
load 247
itob
concat
int 1
bzero
dig 1
extract 0 96
app_global_put
pop
int 4
store 1
global Round
store 2
txn OnCompletion
int NoOp
==
assert
b updateState
l27_ifF:
load 248
dup
bz l28_makeTxnK
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
load 246
itxn_field Receiver
itxn_submit
int 0
l28_makeTxnK:
pop
load 247
dup
bz l29_makeTxnK
itxn_begin
itxn_field AssetAmount
int axfer
itxn_field TypeEnum
load 246
itxn_field AssetReceiver
load 245
itxn_field XferAsset
itxn_submit
int 0
l29_makeTxnK:
pop
int 0
itxn_begin
itxn_field AssetAmount
int axfer
itxn_field TypeEnum
global CreatorAddress
itxn_field AssetCloseTo
global CurrentApplicationAddress
itxn_field AssetReceiver
load 245
itxn_field XferAsset
itxn_submit
int 0
l30_makeTxnK:
pop
int 0
itxn_begin
itxn_field Amount
int pay
itxn_field TypeEnum
global CreatorAddress
itxn_field CloseRemainderTo
global CurrentApplicationAddress
itxn_field Receiver
itxn_submit
int 0
l31_makeTxnK:
pop
txn OnCompletion
int DeleteApplication
==
assert
updateState:
byte base64()
load 1
itob
load 2
itob
concat
app_global_put
checkSize:
load 0
dup
dup
int 1
+
global GroupSize
==
assert
txn GroupIndex
==
assert
int 1000
*
txn Fee
<=
assert
done:
int 1
return
alloc:
txn OnCompletion
int NoOp
==
assert
int 0
store 1
int 0
store 2
b updateState
`,
  appClear: `#pragma version 5
int 0
`,
  mapDataKeys: 1,
  mapDataSize: 42,
  stateKeys: 1,
  stateSize: 96,
  unsupported: [],
  version: 6
  };
const _ETH = {
  ABI: `[
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "internalType": "struct T7",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "stateMutability": "payable",
    "type": "constructor"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "msg",
        "type": "uint256"
      }
    ],
    "name": "ReachError",
    "type": "error"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "indexed": false,
        "internalType": "struct T7",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_e0",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v431",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v432",
                "type": "address"
              }
            ],
            "internalType": "struct T11",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T12",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_e1",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "components": [
                  {
                    "internalType": "enum _enum_T18",
                    "name": "which",
                    "type": "uint8"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "internalType": "uint256",
                            "name": "elem0",
                            "type": "uint256"
                          },
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem1",
                            "type": "tuple"
                          }
                        ],
                        "internalType": "struct T13",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T14",
                    "name": "_contractAPI_deposit0",
                    "type": "tuple"
                  },
                  {
                    "internalType": "bool",
                    "name": "_contractAPI_exitLoop0",
                    "type": "bool"
                  },
                  {
                    "internalType": "bool",
                    "name": "_contractAPI_optIn0",
                    "type": "bool"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem0",
                            "type": "tuple"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem1",
                            "type": "address"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem2",
                            "type": "address"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem3",
                            "type": "uint256"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem4",
                            "type": "uint256"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem5",
                            "type": "uint256"
                          }
                        ],
                        "internalType": "struct T16",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T17",
                    "name": "_contractAPI_winnerAnnouncement0",
                    "type": "tuple"
                  }
                ],
                "internalType": "struct T18",
                "name": "v549",
                "type": "tuple"
              }
            ],
            "internalType": "struct T19",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T20",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_e3",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v1049",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v1049",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v580",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v580",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v747",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v747",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "v886",
        "type": "bool"
      }
    ],
    "name": "_reach_oe_v886",
    "type": "event"
  },
  {
    "stateMutability": "payable",
    "type": "fallback"
  },
  {
    "inputs": [],
    "name": "ViewReader_read",
    "outputs": [
      {
        "components": [
          {
            "internalType": "bool",
            "name": "elem0",
            "type": "bool"
          },
          {
            "internalType": "uint256",
            "name": "elem1",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem2",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem3",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem4",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem5",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem6",
            "type": "uint256"
          }
        ],
        "internalType": "struct T3",
        "name": "",
        "type": "tuple"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "_reachCreationTime",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "_reachCurrentState",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      },
      {
        "internalType": "bytes",
        "name": "",
        "type": "bytes"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "_reachCurrentTime",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v431",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v432",
                "type": "address"
              }
            ],
            "internalType": "struct T11",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T12",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_m1",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "components": [
                  {
                    "internalType": "enum _enum_T18",
                    "name": "which",
                    "type": "uint8"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "internalType": "uint256",
                            "name": "elem0",
                            "type": "uint256"
                          },
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem1",
                            "type": "tuple"
                          }
                        ],
                        "internalType": "struct T13",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T14",
                    "name": "_contractAPI_deposit0",
                    "type": "tuple"
                  },
                  {
                    "internalType": "bool",
                    "name": "_contractAPI_exitLoop0",
                    "type": "bool"
                  },
                  {
                    "internalType": "bool",
                    "name": "_contractAPI_optIn0",
                    "type": "bool"
                  },
                  {
                    "components": [
                      {
                        "components": [
                          {
                            "components": [
                              {
                                "internalType": "bytes32",
                                "name": "elem0",
                                "type": "bytes32"
                              }
                            ],
                            "internalType": "struct T0",
                            "name": "elem0",
                            "type": "tuple"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem1",
                            "type": "address"
                          },
                          {
                            "internalType": "address payable",
                            "name": "elem2",
                            "type": "address"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem3",
                            "type": "uint256"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem4",
                            "type": "uint256"
                          },
                          {
                            "internalType": "uint256",
                            "name": "elem5",
                            "type": "uint256"
                          }
                        ],
                        "internalType": "struct T16",
                        "name": "elem0",
                        "type": "tuple"
                      }
                    ],
                    "internalType": "struct T17",
                    "name": "_contractAPI_winnerAnnouncement0",
                    "type": "tuple"
                  }
                ],
                "internalType": "struct T18",
                "name": "v549",
                "type": "tuple"
              }
            ],
            "internalType": "struct T19",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T20",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "_reach_m3",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "elem0",
            "type": "uint256"
          },
          {
            "components": [
              {
                "internalType": "bytes32",
                "name": "elem0",
                "type": "bytes32"
              }
            ],
            "internalType": "struct T0",
            "name": "elem1",
            "type": "tuple"
          }
        ],
        "internalType": "struct T13",
        "name": "_a0",
        "type": "tuple"
      }
    ],
    "name": "contractAPI_deposit",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "contractAPI_exitLoop",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "contractAPI_optIn",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "bytes32",
                "name": "elem0",
                "type": "bytes32"
              }
            ],
            "internalType": "struct T0",
            "name": "elem0",
            "type": "tuple"
          },
          {
            "internalType": "address payable",
            "name": "elem1",
            "type": "address"
          },
          {
            "internalType": "address payable",
            "name": "elem2",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "elem3",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem4",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "elem5",
            "type": "uint256"
          }
        ],
        "internalType": "struct T16",
        "name": "_a0",
        "type": "tuple"
      }
    ],
    "name": "contractAPI_winnerAnnouncement",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "stateMutability": "payable",
    "type": "receive"
  }
]`,
  Bytecode: `0x6080604052604051620025183803806200251883398101604081905262000026916200019c565b600080554360035560408051825181526020808401511515908201527ff6b2f582026eaf8fd1fe583a836da56a1b30b8bd595170ad494773aa9148b06e910160405180910390a16200007b34156008620000cc565b6040805160208082018352338083526001600081905543905583519182015290910160405160208183030381529060405260029080519060200190620000c3929190620000f6565b50505062000244565b81620000f25760405163100960cb60e01b81526004810182905260240160405180910390fd5b5050565b828054620001049062000207565b90600052602060002090601f01602090048101928262000128576000855562000173565b82601f106200014357805160ff191683800117855562000173565b8280016001018555821562000173579182015b828111156200017357825182559160200191906001019062000156565b506200018192915062000185565b5090565b5b8082111562000181576000815560010162000186565b600060408284031215620001af57600080fd5b604080519081016001600160401b0381118282101715620001e057634e487b7160e01b600052604160045260246000fd5b6040528251815260208301518015158114620001fb57600080fd5b60208201529392505050565b600181811c908216806200021c57607f821691505b602082108114156200023e57634e487b7160e01b600052602260045260246000fd5b50919050565b6122c480620002546000396000f3fe60806040526004361061008f5760003560e01c80638376c5bd116100565780638376c5bd1461013657806398613ab414610149578063ab53f2c61461015c578063e0b74db31461017f578063e5dc75de146101ec57005b80630aec9b53146100985780631e93b0f1146100c257806327c19c1a146100e1578063793c1e1414610101578063832307571461012157005b3661009657005b005b3480156100a457600080fd5b506100ad610201565b60405190151581526020015b60405180910390f35b3480156100ce57600080fd5b506003545b6040519081526020016100b9565b3480156100ed57600080fd5b506100ad6100fc366004611d23565b610267565b34801561010d57600080fd5b506100ad61011c366004611df6565b6102d4565b34801561012d57600080fd5b506001546100d3565b610096610144366004611e12565b610345565b610096610157366004611e2a565b610385565b34801561016857600080fd5b506101716103c1565b6040516100b9929190611e69565b34801561018b57600080fd5b5061019461045e565b6040516100b99190600060e0820190508251151582526020830151602083015260408301516040830152606083015160608301526080830151608083015260a083015160a083015260c083015160c083015292915050565b3480156101f857600080fd5b506100ad6106ba565b604080516080810182526000808252602082018190529181018290526060810182905261022c6118a2565b6102346118c1565b60006060820152600281526040805160208082019092528281529083015261025c8284610723565b505060400151919050565b60408051608081018252600080825260208201819052918101829052606081018290526102926118a2565b61029a6118c1565b6040805160208082018352878252838101919091526000835281518082019092528282528301526102cb8284610723565b50505192915050565b60408051608081018252600080825260208201819052918101829052606081018290526102ff6118a2565b6103076118c1565b604080516020808201835287825260808401919091526003835281518082019092528282528301526103398284610723565b50506060015192915050565b60408051608081018252600080825260208201819052918101829052606081019190915261038161037b36849003840184611eb9565b826111a3565b5050565b6040805160808101825260008082526020820181905291810182905260608101919091526103816103bb36849003840184611f66565b82610723565b6000606060005460028080546103d690612031565b80601f016020809104026020016040519081016040528092919081815260200182805461040290612031565b801561044f5780601f106104245761010080835404028352916020019161044f565b820191906000526020600020905b81548152906001019060200180831161043257829003601f168201915b50505050509050915091509091565b6104a06040518060e001604052806000151581526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b6001600054141561055b576000600280546104ba90612031565b80601f01602080910402602001604051908101604052809291908181526020018280546104e690612031565b80156105335780601f1061050857610100808354040283529160200191610533565b820191906000526020600020905b81548152906001019060200180831161051657829003601f168201915b505050505080602001905181019061054b9190612071565b905061055960006007611373565b505b600460005414156106ab5760006002805461057590612031565b80601f01602080910402602001604051908101604052809291908181526020018280546105a190612031565b80156105ee5780601f106105c3576101008083540402835291602001916105ee565b820191906000526020600020905b8154815290600101906020018083116105d157829003601f168201915b5050505050806020019051810190610606919061209f565b905061064c6040805161010081018252600060208201818152928201819052606082018190526080820181905260a0820181905260c0820181905260e082015290815290565b80516001905260a08083015182516020015260c080840151835160409081019190915260808086015185516060908101919091529186015185519091015284015183519092019190915260e09092015181519092019190915251919050565b6106b760006007611373565b90565b60408051608081018252600080825260208201819052918101829052606081018290526106e56118a2565b6106ed6118c1565b6000604082015260018181905250604080516020808201909252828152908301526107188284610723565b505060200151919050565b6107336004600054146017611373565b815161074e90158061074757508251600154145b6018611373565b60008080556002805461076090612031565b80601f016020809104026020016040519081016040528092919081815260200182805461078c90612031565b80156107d95780601f106107ae576101008083540402835291602001916107d9565b820191906000526020600020905b8154815290600101906020018083116107bc57829003601f168201915b50505050508060200190518101906107f1919061209f565b90506107fb611933565b7f6ddd45aeedf08031b8a08d147d36f6384adbf30b97f32b25a8827eb450d06b468460405161082a9190612128565b60405180910390a1600060208501515151600381111561084c5761084c611ea3565b1415610b9a57602080850151510151808252515160e083015161086f9190612207565b602082015280515151610885903414600d611373565b61089f6108983384602001516000611399565b600e611373565b805151516108b0901515600f611373565b6108b9336113b1565b604082018190525160009060018111156108d5576108d5611ea3565b14156108e7576001606082015261090e565b6001604082015151600181111561090057610900611ea3565b141561090e57600060608201525b7f72a1b3cbf5e57d9a9ea2945e59ff7bc334ba533d5a6341a997140a9cd69882bd8160600151604051610945911515815260200190565b60405180910390a160608101511515835261095f336113b1565b6080820181905251600090600181111561097b5761097b611ea3565b1415610acb5780515160209081015160a083018051919091528051600190830181905283515151825160409081019190915233600090815260048552819020805460ff1990811684178255935180515193820193909355938201516002850180549094169015151790925501516003909101556109f6611a2c565b825181516001600160a01b039182169052602080850151835192169101526040830151610a2590600190612207565b602082015152815151516060840151610a3e9190612207565b6020808301805190910191909152805160016040909101819052608085015191516060019190915260a0840151610a759190612207565b6020820151608001528151515160c0840151610a919190612207565b6020808301805160a0019290925281514360c090910152830151815160e001526101008085015191510152610ac5816114a0565b5061119d565b60016080820151516001811115610ae457610ae4611ea3565b1415610b9557610af2611a2c565b825181516001600160a01b039182169052602080850151835192169181019190915260408401519082015152815151516060840151610b319190612207565b602080830180518201929092528151600160409091015260808086015183516060015260a08087015184519092019190915260c080870151845190920191909152825143910152830151815160e001526101008085015191510152610ac5816114a0565b61119d565b6001602085015151516003811115610bb457610bb4611ea3565b1415610cc357610bc634156010611373565b610be0610bd93384602001516000611399565b6011611373565b604051600181527f1e83d33026ed9df5c0a304faa8182b67d9bee81417b0bbf7f90c2f29e0b4bbb39060200160405180910390a160016020840152610c23611a2c565b825181516001600160a01b03918216905260208085015183519216918101919091526040808501518284018051919091526060808701518251909401939093528051600092019190915260808086015182519093019290925260a08086015182519093019290925260c08086015182519093019290925280514392019190915260e0808501518251909101526101008085015191510152610ac5816114a0565b6002602085015151516003811115610cdd57610cdd611ea3565b1415610dec57610cef34156012611373565b610d09610d023384602001516000611399565b6013611373565b604051600181527f54475f0b04e109e3dd7fd39346e970ccb8930f05c4986d4236bb640cfcb526e69060200160405180910390a160016040840152610d4c611a2c565b825181516001600160a01b03918216905260208085015183519216918101919091526040808501518284018051919091526060808701518251909401939093528051600192019190915260808086015182519093019290925260a08086015182519093019290925260c08086015182519093019290925280514392019190915260e0808501518251909101526101008085015191510152610ac5816114a0565b6003602085015151516003811115610e0657610e06611ea3565b141561119d576020840151516080015160c0820152610e2734156014611373565b610e41610e3a3384602001516000611399565b6015611373565b60c08101515160200151610e54906113b1565b60e08201819052516000906001811115610e7057610e70611ea3565b1415610e83576000610100820152610eab565b600160e0820151516001811115610e9c57610e9c611ea3565b1415610eab5760016101008201525b60c08101515160400151610ebe906113b1565b6101208201819052516000906001811115610edb57610edb611ea3565b1415610eee576000610140820152610f17565b6001610120820151516001811115610f0857610f08611ea3565b1415610f175760016101408201525b806101000151610f28576000610f2f565b8061014001515b151561016082018190526040519081527f6b992e3b7abbce947465e9e7887b79e388f82820688b38f221e65184f30f12e79060200160405180910390a1610160810180511515606085015251156111955760c08101515160600151610f9590600261221f565b6101808201819052610fc190610fac576000610fba565b8260e0015182610180015111155b6016611373565b60c08101515160800151610fd690600261221f565b6101a08201819052610180820151610fee919061223e565b6101c0820181905260c082015151602001516040516001600160a01b039091169180156108fc02916000818181858888f19350505050158015611035573d6000803e3d6000fd5b5060c081018051516020908101516001600160a01b03908116600090815260049092526040808320805461ffff19908116825560018083018690556002808401805460ff199081169091556003948501889055975151850151909516865292852080549091168155918201849055918101805490941690935591909101556110bb611a2c565b825181516001600160a01b0391821690526020808501518351921691015260408301516110ea9060029061223e565b6020820151526101808201516060840151611105919061223e565b60208083018051909101919091525160016040909101526101a082015160808401516111319190612207565b6020820180516060019190915260a08085015182516080015260c0808601518351909201919091529051439101526101c082015160e0840151611174919061223e565b60208201805160e001919091526101008085015191510152610ac5816114a0565b610d4c611a2c565b50505050565b6111b3600160005414600b611373565b81516111ce9015806111c757508251600154145b600c611373565b6000808055600280546111e090612031565b80601f016020809104026020016040519081016040528092919081815260200182805461120c90612031565b80156112595780601f1061122e57610100808354040283529160200191611259565b820191906000526020600020905b81548152906001019060200180831161123c57829003601f168201915b50505050508060200190518101906112719190612071565b90507f64e402695c69cb5e36a62d15ce0da10e549e9202f37e9905d2bc2b09411fd1c7836040516112c5919081518152602091820151805183830152909101516001600160a01b0316604082015260600190565b60405180910390a16112d934156009611373565b80516112f1906001600160a01b03163314600a611373565b6112f9611a2c565b815181516001600160a01b0391821690526020808601518101518351921691810191909152808201805160009081905281519092018290528051600160409091015280516060018290528051608001829052805160a00182905280514360c090910152805160e00182905251610100015261119d816114a0565b816103815760405163100960cb60e01b8152600481018290526024015b60405180910390fd5b60006113a7838530856116a8565b90505b9392505050565b6113b9611a9f565b60016001600160a01b03831660009081526004602052604090205460ff1660018111156113e8576113e8611ea3565b141561148f576001600160a01b038216600090815260046020526040908190208151606081019092528054829060ff16600181111561142957611429611ea3565b600181111561143a5761143a611ea3565b81528154610100900460ff908116151560208084019190915260408051608081018252600186015460608201908152815260028601549093161515918301919091526003909301548184015291015292915050565b60008152600160208201525b919050565b8060200151604001511561162e5761150f60405180610120016040528060006001600160a01b0316815260200160006001600160a01b03168152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b8151516001600160a01b0390811682528251602090810151909116818301528083018051516040808501919091528151830151606080860191909152825101516080808601919091528251015160a0808601919091528251015160c0850152815160e0908101519085015290516101009081015190840152600460005543600155516116059183910181516001600160a01b0390811682526020808401519091169082015260408083015190820152606080830151908201526080808301519082015260a0828101519082015260c0808301519082015260e0808301519082015261010091820151918101919091526101200190565b60405160208183030381529060405260029080519060200190611629929190611ad8565b505050565b805151602082015160e001516040516001600160a01b039092169181156108fc0291906000818181858888f19350505050158015611670573d6000803e3d6000fd5b508051602080820151915190830151610100015161168f929190611782565b600080805560018190556116a590600290611b5c565b50565b604080516001600160a01b0385811660248301528481166044830152606480830185905283518084039091018152608490920183526020820180516001600160e01b03166323b872dd60e01b17905291516000928392839291891691839161170f91612255565b60006040518083038185875af1925050503d806000811461174c576040519150601f19603f3d011682016040523d82523d6000602084013e611751565b606091505b509150915061176282826001611796565b50808060200190518101906117779190612271565b979650505050505050565b61178d8383836117d1565b61162957600080fd5b606083156117a55750816113aa565b8251156117b55782518084602001fd5b60405163100960cb60e01b815260048101839052602401611390565b604080516001600160a01b038481166024830152604480830185905283518084039091018152606490920183526020820180516001600160e01b031663a9059cbb60e01b17905291516000928392839291881691839161183091612255565b60006040518083038185875af1925050503d806000811461186d576040519150601f19603f3d011682016040523d82523d6000602084013e611872565b606091505b509150915061188382826002611796565b50808060200190518101906118989190612271565b9695505050505050565b6040518060400160405280600081526020016118bc611b96565b905290565b6040805160a0810190915280600081526020016118dc611ba9565b815260006020820181905260408201526060016118bc6040805161010081018252600060e0820181815260208301908152928201819052606082018190526080820181905260a0820181905260c082015290815290565b604051806101e00160405280611947611ba9565b81526020016000815260200161195b611a9f565b81526000602082015260400161196f611a9f565b815260408051608081018252600060608201818152825260208281018290529282015291019081526020016119de6040805161010081018252600060e0820181815260208301908152928201819052606082018190526080820181905260a0820181905260c082015290815290565b81526020016119eb611a9f565b8152600060208201526040016119ff611a9f565b81526020016000151581526020016000151581526020016000815260200160008152602001600081525090565b604080516080810182526000918101828152606082019290925290819081526020016118bc60405180610120016040528060008152602001600081526020016000151581526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b60408051606080820183526000808352602080840182905284516080810186529283018281528352820181905281840152909182015290565b828054611ae490612031565b90600052602060002090601f016020900481019282611b065760008555611b4c565b82601f10611b1f57805160ff1916838001178555611b4c565b82800160010185558215611b4c579182015b82811115611b4c578251825591602001919060010190611b31565b50611b58929150611bbc565b5090565b508054611b6890612031565b6000825580601f10611b78575050565b601f0160209004906000526020600020908101906116a59190611bbc565b60405180602001604052806118bc6118c1565b60405180602001604052806118bc611bd1565b5b80821115611b585760008155600101611bbd565b6040518060400160405280600081526020016118bc6040518060200160405280600080191681525090565b6040516020810167ffffffffffffffff81118282101715611c2d57634e487b7160e01b600052604160045260246000fd5b60405290565b6040805190810167ffffffffffffffff81118282101715611c2d57634e487b7160e01b600052604160045260246000fd5b60405160a0810167ffffffffffffffff81118282101715611c2d57634e487b7160e01b600052604160045260246000fd5b604051610120810167ffffffffffffffff81118282101715611c2d57634e487b7160e01b600052604160045260246000fd5b600060208284031215611cd957600080fd5b611ce1611bfc565b9135825250919050565b600060408284031215611cfd57600080fd5b611d05611c33565b905081358152611d188360208401611cc7565b602082015292915050565b600060408284031215611d3557600080fd5b6113aa8383611ceb565b6001600160a01b03811681146116a557600080fd5b600060c08284031215611d6657600080fd5b60405160c0810181811067ffffffffffffffff82111715611d9757634e487b7160e01b600052604160045260246000fd5b604052905080611da78484611cc7565b81526020830135611db781611d3f565b60208201526040830135611dca81611d3f565b80604083015250606083013560608201526080830135608082015260a083013560a08201525092915050565b600060c08284031215611e0857600080fd5b6113aa8383611d54565b600060608284031215611e2457600080fd5b50919050565b60006101808284031215611e2457600080fd5b60005b83811015611e58578181015183820152602001611e40565b8381111561119d5750506000910152565b8281526040602082015260008251806040840152611e8e816060850160208701611e3d565b601f01601f1916919091016060019392505050565b634e487b7160e01b600052602160045260246000fd5b60008183036060811215611ecc57600080fd5b611ed4611c33565b833581526040601f1983011215611eea57600080fd5b611ef2611c33565b9150602084013582526040840135611f0981611d3f565b6020838101919091528101919091529392505050565b80151581146116a557600080fd5b803561149b81611f1f565b600060c08284031215611f4a57600080fd5b611f52611bfc565b9050611f5e8383611d54565b815292915050565b6000818303610180811215611f7a57600080fd5b611f82611c33565b83358152610160601f1983011215611f9957600080fd5b611fa1611bfc565b611fa9611c64565b602086013560048110611fbb57600080fd5b81526040603f1985011215611fcf57600080fd5b611fd7611bfc565b9350611fe68760408801611ceb565b8452836020820152611ffa60808701611f2d565b604082015261200b60a08701611f2d565b606082015261201d8760c08801611f38565b608082015281526020820152949350505050565b600181811c9082168061204557607f821691505b60208210811415611e2457634e487b7160e01b600052602260045260246000fd5b805161149b81611d3f565b60006020828403121561208357600080fd5b61208b611bfc565b825161209681611d3f565b81529392505050565b600061012082840312156120b257600080fd5b6120ba611c95565b6120c383612066565b81526120d160208401612066565b602082015260408301516040820152606083015160608201526080830151608082015260a083015160a082015260c083015160c082015260e083015160e08201526101008084015181830152508091505092915050565b815181526020820151518051610180830191906004811061215957634e487b7160e01b600052602160045260246000fd5b8060208501525060208101515180516040850152602081015151606085015250604081015115156080840152606081015161219860a085018215159052565b506080908101515180515160c085015260208101516001600160a01b0390811660e086015260408201511661010085015260608101516101208501529081015161014084015260a0015161016090920191909152919050565b634e487b7160e01b600052601160045260246000fd5b6000821982111561221a5761221a6121f1565b500190565b6000816000190483118215151615612239576122396121f1565b500290565b600082821015612250576122506121f1565b500390565b60008251612267818460208701611e3d565b9190910192915050565b60006020828403121561228357600080fd5b81516113aa81611f1f56fea2646970667358221220eaa371bb5d9b57e8222c122498d5a483e66b491dd025d4a4d1f5298fc86d417564736f6c63430008090033`,
  BytecodeLen: 9496,
  Which: `oD`,
  version: 5,
  views: {
    ViewReader: {
      read: `ViewReader_read`
      }
    }
  };
export const _Connectors = {
  ALGO: _ALGO,
  ETH: _ETH
  };
export const _Participants = {
  "Deployer": Deployer,
  "contractAPI_deposit": contractAPI_deposit,
  "contractAPI_exitLoop": contractAPI_exitLoop,
  "contractAPI_optIn": contractAPI_optIn,
  "contractAPI_winnerAnnouncement": contractAPI_winnerAnnouncement
  };
export const _APIs = {
  contractAPI: {
    deposit: contractAPI_deposit,
    exitLoop: contractAPI_exitLoop,
    optIn: contractAPI_optIn,
    winnerAnnouncement: contractAPI_winnerAnnouncement
    }
  };
