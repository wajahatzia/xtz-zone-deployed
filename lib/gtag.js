export const pageview = url => {
  window.gtag('config', 'G-VEWRF7QLQ9', {
    page_path: url
  })
}

export const event = ({ action, category, label, value }) => {

  typeof window !== "undefined" && window.gtag('event', action, {
    event_category: category,
    event_label: label,
    value: value
  })
}
