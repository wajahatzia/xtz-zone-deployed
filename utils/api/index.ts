import axios from "axios";

export const server = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_SERVICE,
  // headers: {
  //   "Access-Control-Allow-Origin": 'http://localhost:3000'
  // }
});