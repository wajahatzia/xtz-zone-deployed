export {}
// import algosdk from "algosdk";
// import { ARC11_Wallet, WalletTransaction } from "./types";

// interface Provider {
//   algodClient: algosdk.Algodv2,
//   indexer: algosdk.Indexer,
//   getDefaultAddress: () => any,
//   // isIsolatedNetwork: boolean,
//   // signAndPostTxns: (txns:WalletTransaction[], opts?: any) => Promise<any>,
// };

// const decodeB64Txn = (ts:string): any => {
//   const tb = Buffer.from(ts, 'base64');
//   return algosdk.decodeUnsignedTransaction(tb);
// };

// const signTxns = async (txns: string[], magic: any) => {
//   console.log('signTxns')
//   return Promise.all(txns.map( async (ts) => {
//     console.log("🚀 ~ file: NewMagicWallet.ts ~ line 42 ~ returnPromise.all ~ ts", ts)
//     const t = decodeB64Txn(ts);
//     console.log("🚀 ~ file: NewMagicWallet.ts ~ line 43 ~ returnPromise.all ~ t", t)
//     const genesisHashB64 = Buffer.from(t.genesisHash).toString('base64')
//     t.genesisHash = genesisHashB64
//     const base32Encode = (await import('base32-encode')).default
//     if( t.to ){
//       const mergedArrayTo = new Uint8Array(t.to.checksum.length + t.to.publicKey.length)
//       mergedArrayTo.set(t.to.publicKey)
//       mergedArrayTo.set(t.to.checksum, t.to.publicKey.length)
//       t.to = base32Encode(mergedArrayTo, 'RFC4648')
//     }
//     if( t.from ) {
//       const mergedArrayFrom = new Uint8Array(t.from.checksum.length + t.from.publicKey.length)
//       mergedArrayFrom.set(t.from.publicKey)
//       mergedArrayFrom.set(t.from.checksum, t.from.publicKey.length)
//       t.from = base32Encode(mergedArrayFrom, 'RFC4648')
//     }
//     if ( !t.fee) t.fee = 1000
//     delete t.lease
//     const suggestedParams = {
//       genesisID: t.genesisID,
//       firstRound: t.firstRound,
//       lastRound: t.lastRound,
//       genesisHash: genesisHashB64,
//       flatFee: false,
//       fee: t.fee
//     }
//     t.suggestedParams = suggestedParams
//     console.log('t with encoded line 58', t)

//     return await magic.algorand.signTransaction(t)
//   })
//   )
// };

// export const walletFallback_Magic = (magic: any): ARC11_Wallet => {
// console.log("🚀 ~ file: NewMagicWallet.ts ~ line 54 ~ magic", magic)
//   let p: Provider | undefined = undefined
//   const enable = async (): Promise<any> => {
//     console.log('enable')
//     p = {
//       algodClient: new algosdk.Algodv2('', 'https://testnet.algoexplorerapi.io', ''),
//       indexer: new algosdk.Indexer('', 'https://testnet.algoexplorerapi.io/idx2', ''),
//       getDefaultAddress: async () => { 
//         return await magic.user.getWallet()
//       },
//       // signAndPostTxns: signAndPostTxns //declare this correctly
//     }
//     const addr = await magic.algorand.getWallet()
//     return { accounts: [ addr ]}
//   }
//   const getAlgodv2 = (): any => {
//     return p?.algodClient
//   }
//   const getIndexer = (): any => {
//     return p?.indexer
//   }
//   const signAndPostTxns = async (txns:WalletTransaction[], sopts?:any) => {
//     console.log('signAndPostTxns')
//     if ( !p ) { throw new Error(`must call enable`) };
//     void(sopts);
//     console.log(`fallBack: signAndPostTxns`, {txns});
//     const to_sign: string[] = [];
//     txns.forEach((txn) => {
//       if ( ! txn.stxn ) {
//         to_sign.push(txn.txn);
//       }
//     });
//     console.log(`fallBack: signAndPostTxns`, {to_sign});
//     const signed: string[] = to_sign.length == 0 ? [] : await signTxns(to_sign, magic);
//     console.log(`fallBack: signAndPostTxns`, {signed});
//     const stxns: string[] = txns.map((txn) => {
//       if ( txn.stxn ) { return txn.stxn; }
//       const s = signed.shift();
//       if ( ! s ) { throw new Error(`txn not signed`); }
//       return s;
//     });
//     const bs = stxns.map((stxn) => Buffer.from(stxn, 'base64'));
//     console.log(`fallBack: signAndPostTxns`, bs);
//     await p.algodClient.sendRawTransaction(bs).do();
//     return {};
//   }

//   return { enable, getAlgodv2, getIndexer, signAndPostTxns };
// }
