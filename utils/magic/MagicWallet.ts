export {}
// import { AlgorandExtension } from '@magic-ext/algorand';
// import algosdk from 'algosdk';
// const algosdk_transaction = require('algosdk/dist/cjs/src/transaction');
// import { Buffer } from 'buffer';
// import { Magic } from 'magic-sdk';

// class MagicWallet {
//   enabled: boolean;
//   magic: any;
//   connection: any;
//   network: any;
//   algodv2: any;
//   indexer: any;
//   email: any;
//   accounts: any[];
//   sk: any;
//   constructor(opts: any) {
//     const {connection, magic} = opts;
//     if (!connection) throw Error(`connection required`);

//     this.enabled = false;
//     this.magic = magic;
//     //this.accounts = [addr];
//     this.connection = connection;
//     this.network = null;
//     this.algodv2 = null;
//     this.indexer = null;
//     this.email = null;
//     this.accounts = [];
//     // this.didToken = issuer;
//   }

//   async enable(eopts: any) {
//     if(eopts.network === undefined) {
//       this.network = 'testnet-v1.0'; 
//       eopts.network = 'testnet-v1.0';
//     } else { 
//       this.network = eopts.network 
//     }
//     console.log(this.network, eopts.network)
//     if (this.network && eopts.network !== this.network) {
//       throw Error(`This MagicWallet is only valid for "${this.network}", not for "${eopts.network}"`);
//     }
//     this.network = eopts.network;

//     const {network, connection} = this;
//     if (!network) {
//       throw Error(`No network selected`);
//     }
//     const c = connection[this.network];
//     if (!c) {
//       throw Error(`Connection info not found for ${network}, only have info for: ${Object.keys(connection).join(', ')}`);
//     }
//     if (!c.algodv2 || !c.indexer) {
//       throw Error(`Missing connection info for ${network}`);
//     }

//     this.algodv2 = new algosdk.Algodv2('', 'https://testnet.algoexplorerapi.io', '');
//     this.indexer = new algosdk.Indexer('', 'https://testnet.algoexplorerapi.io/idx2', '');
//     this.enabled = true;
//     console.log(this.algodv2, this.indexer, this.enabled)
//     return {network, accounts: this.accounts};
//   }

//   setAccount(address: any) {
//     this.accounts = [address]
//   }

//   getDefaultAccount() { console.log('MagicWallet getDefaultAccount');return this.accounts[0] }

//   _requireEnabled() {
//     if (!this.enabled) throw Error('Please call enable first');
//   }

//   async getAlgodv2() {
//     // this._requireEnabled();
//     return this.algodv2;
//   }

//   async getIndexer() {
//     // this._requireEnabled();
//     return this.indexer;
//   }

//   setSendingTransaction(status:boolean) {}

//   setTxHash(txID: any) {
    
//   }

//   async signAndPostTxns(txns: any) {
//     console.log("🚀 ~ file: MagicWallet.ts ~ line 92 ~ MagicWallet ~ signAndPostTxns ~ txns", txns)
//     const postTxns = async (stxns: any) => {
//       console.log("🚀 ~ file: MagicWallet.ts ~ line 94 ~ MagicWallet ~ postTxns ~ stxns", stxns)
//       console.log('postTxns')
//         const algodv2 = new algosdk.Algodv2('', 'https://testnet.algoexplorerapi.io', '');
//         // const txnArray = stxns.map( (txn:any) => {
//         //   const b64 = Buffer.from(txn.blob, 'base64')
//         //   return b64
//         // })
//         // console.log("🚀 ~ file: MagicWallet.ts ~ line 111 ~ MagicWallet ~ postTxns ~ txnArray", txnArray)
//         try{
//           // const txnsBlobs = stxns.map( (txn:any) => txn.blob)
//           const tx = await algodv2.sendRawTransaction(stxns).do();
//           console.log("🚀 ~ file: MagicWallet.ts ~ line 104 ~ MagicWallet ~ postTxns ~ tx", tx)
//           return tx
//         } catch (error) {
//           console.log("🚀 ~ file: MagicWallet.ts ~ line 101 ~ MagicWallet ~ postTxns ~ error", error)
//         }
      
//     }
//     const initMagic = () => {
//       const algoExt = new AlgorandExtension({
//         rpcUrl: 'https://testnet.algoexplorerapi.io',
  
//       })
//       const magic = new Magic<any, any, any>('pk_live_00731A75840E8B0F', {
//         extensions: {
//           algorand: algoExt,
//         },
//     });
//     return magic
//     }
//     const magic = initMagic()
//     const signTxns = async (txns: any, opts: any) =>  {
//       console.log('signTxns')
//       void(opts);
  
//       // A real wallet would prompt the user for approval here.
//       console.log(`demoWallet: Signing ${txns.length} txns`);
  
//       const convertedTxns = await Promise.all(txns.map( async (txnObj: any) => {
//         if (Array.isArray(txnObj.signers) && txnObj.signers.length === 0) {
//           console.log('null')
//           return null;
//         }
//         try{
//         console.log("🚀 ~ file: MagicWallet.ts ~ line 122 ~ MagicWallet ~ returnawaitPromise.all ~ txnObj", txnObj)
//         const txnBuf = Buffer.from(txnObj.txn, 'base64');
//         const t = algosdk_transaction.decodeUnsignedTransaction(txnBuf);
//         const genesisHashB64 = Buffer.from(t.genesisHash).toString('base64')
//         // const groupB64 = Buffer.from(t.group).toString('base64')
//         // const noteB64 = Buffer.from(t.note).toString('base64')
//         // const tagB64 = Buffer.from(t.tag).toString('base64')
//         let modifiedT = {
//           ...t,
//           genesisHash:genesisHashB64,
//           // group: groupB64,
//           note: undefined,
//         }
//         const base32Encode = (await import('base32-encode')).default

//         if( t.to ){
//           console.log(1)
//           const mergedArrayTo = new Uint8Array(t.to.checksum.length + t.to.publicKey.length)
//           console.log(2)
//           mergedArrayTo.set(t.to.publicKey)
//           console.log(3)
//           mergedArrayTo.set(t.to.checksum, t.to.publicKey.length)
//           modifiedT = {
//             ...modifiedT,
//             to: base32Encode(mergedArrayTo, 'RFC4648')
//           }
//         }
//         if( t.from ){
//           console.log(4)
//           const mergedArrayFrom = new Uint8Array(t.from.checksum.length + t.from.publicKey.length)
//           console.log(5)
//           mergedArrayFrom.set(t.from.publicKey)
//           console.log(6)
//           mergedArrayFrom.set(t.from.checksum, t.from.publicKey.length)
//           modifiedT = {
//             ...modifiedT,
//             from: base32Encode(mergedArrayFrom, 'RFC4648'),
//           }
//         }
//         if ( !t.fee) modifiedT.fee = 1000
//         delete modifiedT.lease
//         delete modifiedT.group
//         modifiedT.type === 'appl' && modifiedT.onComplete === 0
//         // delete modifiedT.tag
//         // modifiedT.appArgs && delete modifiedT.appArgs
//         console.log("🚀 ~ file: MagicWallet.ts ~ line 134 ~ MagicWallet ~ t", t)
//         console.log("🚀 ~ file: MagicWallet.ts ~ line 133 ~ MagicWallet ~ modifiedT", modifiedT)
//         // const signedTX = await magic.algorand.signTransaction(modifiedT);
//         // console.log("send transaction", signedTX);
//         // return signedTX;
//         const suggestedParams = {
//           genesisID: modifiedT.genesisID,
//           firstRound: modifiedT.firstRound,
//           lastRound: modifiedT.lastRound,
//           genesisHash: genesisHashB64,
//           flatFee: false,
//           fee: 0
//         }

//         const txn = {
//           ...modifiedT,
//           suggestedParams: suggestedParams
//         }

//         // const signedTxn = await magic.algorand.signTransaction(txn)
//         // console.log("🚀 ~ file: MagicWallet.ts ~ line 201 ~ MagicWallet ~ convertedTxns ~ signedTxn", signedTxn)
//         return txn
//         } catch (error){
//         console.log("🚀 ~ file: MagicWallet.ts ~ line 159 ~ MagicWallet ~ returnawaitPromise.all ~ error", error)
//       }
//       }))

//       // return signedTxns
//       console.log("🚀 ~ file: MagicWallet.ts ~ line 201 ~ MagicWallet ~ convertedTxns ~ convertedTxns", convertedTxns)
//       const signedTx = await magic.algorand.signGroupTransaction(convertedTxns);
//       console.log("🚀 ~ file: MagicWallet.ts ~ line 202 ~ MagicWallet ~ signTxns ~ signedTx", signedTx)
//       return signedTx
//     }
//     console.log('signAndPostTxns')
//     // console.log(this.enabled)
//     // if (!this.enabled) throw Error('Please call enable first');
//     // const stxns = this.signTxns(txns, null);
//     const stxns = await signTxns(txns, null)
//     console.log("🚀 ~ file: MagicWallet.ts ~ line 164 ~ MagicWallet ~ signAndPostTxns ~ stxns", stxns)
//     // Would prefer not to do this here,
//     // but keeping it compatible with ARC-0001
//     for (const i in stxns) {
//       if (!stxns[i]) {
//         stxns[i] = txns[i].stxn;
//       }
//     }
//     const postTxnsReturn = await postTxns(stxns)
//     return postTxnsReturn;
//   }
// }

// export {MagicWallet};
