export enum actiontype {
  ADDWALLET = 'ADDWALLET',
  LOADING = 'loading',
  FULFILLED = 'succeeded',
  REJECTED = 'rejected',
  SET_ROLE = "SET_ROLE",
  CLEARERROR = 'CLEARERROR',
  CLEAR_WALLET = 'CLEAR_WALLET'
}

export enum reachActionType {
  LOADING = 'loading',
  FULFILLED = 'succeeded',
  REJECTED = 'rejected',
  SETWAGERFUlFILLED = 'setWagerFulfilled'
}

export enum reachContractActionType {
  OPTIN = 'opt-in',
  DEPOSIT = 'deposit',
}