import { loadStdlib } from "@reach-sh/stdlib";
import { connectToWallet } from "../myAlgo";
const reach = loadStdlib('ALGO')

try{
  // reach.setSignStrategy('AlgoSigner')
  // reach.setProviderByName('TestNet')
} catch (error) {
  console.log(error)
}

export const connectToWalletReach:any = async () => {
  try {
    const account = await reach.getDefaultAccount()
    const balAtomic = await reach.balanceOf(account?.address)
    const balance = reach.formatCurrency( balAtomic, 4)
    return [ account, balance ]
  } catch (error) {
    console.log(error)
  }
}