import algosdk from "algosdk";


const initAlgodClient = () => {
  const client = new algosdk.Algodv2("",'https://api.testnet.algoexplorer.io', '');
  return client
}

const initAlgodClientSingleton = () => {
  var instance
  if(!instance){
    instance = initAlgodClient()
  }
  return instance
}

export const getWalletBalance = async ( account:string ) => {
  const algod = initAlgodClientSingleton()
  try {
  const balance = await algod.accountInformation(account).do()
  return balance
  } catch (error) {
    console.log(error)
  }
}