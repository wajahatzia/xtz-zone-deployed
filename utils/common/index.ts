import { Dispatch, State, StatusAPI } from "../../context/AppContext/types"
import { connectToWalletReach } from "../ReachWallet"
import { actiontype } from "../constants"
import Router from 'next/router'
import { notifyPlayer1 } from "../../context/AppContext/api"
import { connectToWallet } from "../myAlgo"
import { loadStdlib } from "@reach-sh/stdlib"
// import { walletFallback_Magic } from "../magic/NewMagicWallet"

const reach = loadStdlib('ALGO')

export const initWallet = async (state: State, dispatch: Dispatch) => {
  if(!state?.wallet?.account) {
    try{
      const [ account, balance ] = await connectToWallet(dispatch)
      dispatch({ 
        type: actiontype.ADDWALLET, 
        status: {
          connectWallet: StatusAPI.FULFILLED
        },
        payload: {
          account: account, 
          balance: balance 
        } 
      })

    } catch (error: any) {
      if (error.message === 'Can not open popup window - blocked') {
        dispatch({
          type: actiontype.REJECTED,
          payload: { error: { message: "Your browser is blocking the pop-up windows on this site. Please adjust your browser settings"} },
          status: { sendWager: StatusAPI.REJECTED }
        })
      }
    }
  }
}

// export const initMagicWalletToState = async (state: State, dispatch: Dispatch, magic: any, wallet: any) => {
//   if(!state?.wallet?.account) {
//     try {
//         const metadata = await magic.user.getMetadata();
//         // wallet.setAccount(metadata.publicAddress)
//         // reach.setWalletFallback(() => wallet)
//         reach.setWalletFallback(() => walletFallback_Magic(magic))
//         const account = await reach.getDefaultAccount()
//         console.log("🚀 ~ file: index.ts ~ line 38 ~ initMagicWalletToState ~ account", account)
//         const balAtomic = await reach.balanceOf(account)
//         const balance = reach.formatCurrency(balAtomic, 4)

//         dispatch({
//           type: actiontype.ADDWALLET,
//           status: {
//             connectWallet: StatusAPI.FULFILLED,
//           },
//           payload: {
//             account: account,
//             balance: balance
//           }
//         })
//     }catch (error){
//       console.log(error)
//     }
//   } 
// }

export const copyToClipboard = async (copyTextRef: React.RefObject<HTMLParagraphElement>): Promise<void> => {
  if (copyTextRef?.current) {
      window?.getSelection()?.removeAllRanges();
      const range = document.createRange();
      range.selectNode(copyTextRef?.current);
      window?.getSelection()?.addRange(range);
      document.execCommand("copy");
      window?.getSelection()?.removeAllRanges();
  }
};

export const formatTime = (timetotal: number) => {
  let seconds: any = timetotal % 60
  let minutes: any = Math.floor(timetotal / 60);
  minutes = minutes.toString().length === 1 ? '0' + minutes.toString() : minutes.toString()
  seconds = seconds.toString().length === 1 ? '0' + seconds.toString() : seconds.toString();
  return (minutes + ':' + seconds)
}

export const progress = (
  timetotal: number, 
  setTimeString: React.Dispatch<React.SetStateAction<string | undefined>>,
  dispatch: Dispatch,
  notifyParams: string
  ) => {
  // const elem: HTMLElement | null = document.querySelector(".bar");
  // let widthMultiplier = 16;
  // elem!.style.width = widthMultiplier + "%";
  const timer = setInterval(async () => {
      timetotal = timetotal - 1 >= 0 ? timetotal - 1 : 0
      setTimeString(formatTime(timetotal));
      if (timetotal % 5 === 0) {
          await notifyPlayer1(dispatch, notifyParams, timer);
      }
      if (timetotal <= 0) {
          // elem!.style.width = widthMultiplier + "%";
          clearInterval(timer);
          Router.push('/reject');

      } else {
          if (timetotal % 60 === 0) {
              // elem!.style.width = widthMultiplier + "%";
              // widthMultiplier = widthMultiplier + 6;
          }
      }
  }, 1000);
}

export const countdownTimer = (date: any, startFromFifteen: boolean) => {
  if (startFromFifteen){
    date.setMinutes(15)
    date.setSeconds(0)
  }
  const timeMinusOne = date - 1000
  return new Date(timeMinusOne)
}