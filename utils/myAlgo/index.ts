import { loadStdlib } from "@reach-sh/stdlib";
import { Dispatch, StatusAPI } from "../../context/AppContext/types";
import { actiontype } from "../constants";
const reach = loadStdlib('ALGO')
import WalletConnect from '@reach-sh/stdlib/ALGO_WalletConnect';

export const connectToWallet: any = async (dispatch: Dispatch) => {
  try {
    if (typeof window !== "undefined"){
      // const MyAlgoConnect = await initMyAlgoSingleton()
      reach.setWalletFallback(reach.walletFallback({ providerEnv: process.env.NEXT_PUBLIC_NETWORK_MODE, WalletConnect }));
      const account = await reach.getDefaultAccount()
      const balAtomic = await reach.balanceOf(account)
      const balance = reach.formatCurrency( balAtomic, 4)
      return [ account, balance ]
    }
  }
  catch (err:any) {
    if (err.message === 'Can not open popup window - blocked') {
      dispatch({
        type: actiontype.REJECTED,
        payload: { error: { message: "Your browser is blocking the pop-up windows on this site. Please adjust your browser settings"} },
        status: { popup: StatusAPI.REJECTED }
      })
    }
  }
}