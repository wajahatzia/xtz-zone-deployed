import dynamic from 'next/dynamic';
import AcceptReject from '../components/Challenge/Decision';
import { Metadata } from '../components/layout/Metadata';

const NavBar = dynamic(() => import('../components/layout/Navbar'))
const Footer = dynamic(() => import('../components/layout/Footer'))

export default function SecondStep() {
    return (
        <>
            <Metadata title={"Zone Gamefi"} description={"XTZ Blockchain Game"}/>
            <main style={{fontFamily: '"Poppins", sans-serif', backgroundColor: '#FFD024'}}>
                <NavBar yellow={true} showButton={false} isDisabled = {true}/>
                <AcceptReject />
                <Footer yellow={true}/>
            </main>
        </>
    )
}

