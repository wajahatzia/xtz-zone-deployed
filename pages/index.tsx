import dynamic from 'next/dynamic';
import { Landing } from '../components/Landing';
import { Metadata } from '../components/layout/Metadata';

const NavBar = dynamic(() => import('../components/layout/Navbar'))
const Footer = dynamic(() => import('../components/layout/Footer'))

export default function Home() {
  return (
    <>
    <Metadata title={"Zone Gamefi"} description={"XTZ Blockchain Game"}/>
      <main style={{backgroundColor: '#fff'}}>
        <NavBar yellow={false} showButton={true}/>
        <Landing />
        <Footer yellow={false} />
      </main>
    </>
  )
}

