import Tabs from '../components/layout/Tabs';
import dynamic from "next/dynamic";
import { Metadata } from '../components/layout/Metadata';
import withPrivateRoute from '../components/HOC/WithPrivateRoute';

const NavBar = dynamic(() => import('../components/layout/Navbar'))
const Footer = dynamic(() => import('../components/layout/Footer'))

function Step() {
    return (
        <>
            <Metadata title={"Zone Gamefi"} description={"XTZ Blockchain Game"}/>
            <main style={{ fontFamily: '"Poppins", sans-serif', backgroundColor: '#FFD024' }}>
                <NavBar yellow={true} />
                <Tabs />
                <Footer yellow={true} />
            </main>
        </>
    )
}

export default withPrivateRoute(Step);