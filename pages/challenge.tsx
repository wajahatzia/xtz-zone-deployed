import dynamic from 'next/dynamic';
import { Overview } from '../components/Challenge/Overview';
import withPrivateRoute from '../components/HOC/WithPrivateRoute';
import { Metadata } from '../components/layout/Metadata';

const NavBar = dynamic(() => import('../components/layout/Navbar'))
const Footer = dynamic(() => import('../components/layout/Footer'))
function Challenge() {
    return (
        <>
            <Metadata title={"Zone Gamefi"} description={"XTZ Blockchain Game"}/>
            <main style={{fontFamily: '"Poppins", sans-serif', backgroundColor: '#FFD024'}}>
                <NavBar yellow={true} showButton={false} isDisabled = {true}/>
                <Overview />
                <Footer yellow={true}/>
            </main>
        </>
    )
}

export default withPrivateRoute(Challenge)