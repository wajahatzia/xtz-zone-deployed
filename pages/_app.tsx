import '../styles/style.css'
import '../styles/responsive.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import type { AppContext, AppProps } from 'next/app'
import { AccountProvider } from '../context/AppContext/state'
import { ReachProvider } from '../context/ReachContext/state'
import dynamic from 'next/dynamic';
import { hotjar } from 'react-hotjar'
import { useEffect } from 'react'
import * as FullStory from '@fullstory/browser';
import * as gtag from '../lib/gtag';
import { useRouter } from 'next/router';


const Toast = dynamic(() => import('../components/Toast'))

const MyApp = ({ Component, pageProps }: AppProps) => {
  const router = useRouter()
  useEffect(() => {
    hotjar.initialize(2703720, 6)
  }, [])
  useEffect(() => {
    FullStory.init({ orgId: '16N7G4', namespace: "FS", });
  }, [])
  useEffect(() => {
    const handleRouteChange = (url: any) => {
      gtag.pageview(url)
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])
  return (
    <ReachProvider>
      <AccountProvider>
        <Component {...pageProps} />
        <Toast />
      </AccountProvider>
    </ReachProvider>
  )
}
MyApp.getInitialProps = async (appContext: AppContext) => {
  let pageProps = {};
  if (appContext.Component.getInitialProps) {
    pageProps = await appContext.Component.getInitialProps(appContext.ctx);
  }
  return { ...pageProps };
};
export default MyApp
