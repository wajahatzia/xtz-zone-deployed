import dynamic from 'next/dynamic';
import { ChallengeRejected } from '../components/Challenge/Rejected';
import { Metadata } from '../components/layout/Metadata';

const NavBar = dynamic(() => import('../components/layout/Navbar'))
const Footer = dynamic(() => import('../components/layout/Footer'))

export default function SecondStep() {
    return (
        <>
            <Metadata title={"Zone Gamefi"} description={"XTZ Blockchain Game"}/>
            <main style={{fontFamily: '"Poppins", sans-serif', backgroundColor: '#FFD024'}}>
                <NavBar yellow={true} showButton={false}/>
                <ChallengeRejected />
                <Footer yellow={true}/>
            </main>
        </>
    )
}
