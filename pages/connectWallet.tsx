import dynamic from "next/dynamic";
import Footer from "../components/layout/Footer";
import { Metadata } from "../components/layout/Metadata";
import NavBar from "../components/layout/Navbar";
import Link from 'next/link';
import classnames from "classnames";

const FirstStep = dynamic(() => import("../components/Steps"))

export default function ConnectWallet() {
  return (
    <>
      <Metadata title={"Zone Gamefi"} description={"XTZ Blockchain Game"}/>
      <main style={{ fontFamily: '"Poppins", sans-serif', backgroundColor: '#FFD024' }}>
          <NavBar yellow={true} />
          <section className="challenge-friend challenge-friend-step-1">
            <div className="container">
                <div className="row ">
                    <div className="col-sm-10 mx-auto ">
                        <div className="bg-black-radius align-items-center justify-content-center d-flex">
                            <div className="challenge-friend-inner game-box-inner challenge-friend-inner-step-1 challenge-step text-center py-3">
                                <div className="tab-dots-step">
                                    <ul>
                                        <li className={classnames("list-inline-item active")}><a></a>
                                        </li>
                                        <li className={classnames("list-inline-item")}><a></a>
                                        </li>
                                        <li className={classnames("list-inline-item")}><a></a>
                                        </li>
                                        <li className={classnames("list-inline-item")}><a></a>
                                        </li>
                                    </ul>
                                    <h3>Step 1</h3>
                                </div>
                                <FirstStep />
                                {/* {isTabOne && <FirstStep />} */}
                                {/* {isTabTwo && <SecondStep />}
                                {isTabThree && <ThirdStep />}
                                {isTabFourth && <FourthStep />} */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
          <Footer yellow={true} />
      </main>
    </>
  )
}

