const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});
const withTM = require('next-transpile-modules')(['@reach-sh']);

module.exports = withBundleAnalyzer({
  reactStrictMode: true,
});
