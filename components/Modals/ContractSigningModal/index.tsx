import { Modal } from "react-bootstrap"
import { SigningLoader } from "../../Loaders/SigningLoader"
import { useState, useEffect } from 'react';
import classnames from "classnames";
import { useReach } from "../../../context/ReachContext/state";
import { reachContractActionType } from "../../../utils/constants";

export const ContractSigningModal: React.FC<any> = ({ role, handleButtonClickP1, handleButtonClickP2, ...rest }) => {
  const [ showFooter, setShowFooter ] = useState<boolean>(false)
  const { state: {status: { contractStage }}} = useReach()
  const [ wait, setWait ] = useState<boolean>(false);
  const [ countdown, setCountdown ] = useState<number>(30)
  useEffect(() => {
    const timer = setTimeout(() => {
      setShowFooter(true)
      clearTimeout(timer)
    }, 20000)
  }, [])
  useEffect(() => {
    if (wait) { 
      if(countdown === 0) {
        setCountdown(30)
        setWait(false)
      }
      const timer = setTimeout(() => {
        if(countdown === 0){
          clearTimeout(timer)
        } else {
          setCountdown(countdown - 1)
          clearTimeout(timer)
        }
      }, 1000)
    }
  }, [countdown, wait])

  const resendNotification = async() => {
    setWait(true)
    try{
      if (role === 'P1') {
        const optInStatus = contractStage === reachContractActionType.DEPOSIT ? false : true
        await handleButtonClickP1(({retry: true, optIn: optInStatus}))
      }
      if (role === 'P2') {
        const optInStatus = contractStage === reachContractActionType.DEPOSIT ? false : true
        await handleButtonClickP2({retry: true, optIn: optInStatus})
      }
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Modal
      {...rest}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      dialogClassName="contract-signing-dialog-modal"
      contentClassName="contract-signing-content-modal"
    >
      <Modal.Header
        bsPrefix="contract-signing-header-modal"
      >
        <Modal.Title id="contained-modal-title-vcenter" bsPrefix="contract-signing-title-modal">
          Signing Contract
        </Modal.Title>
      </Modal.Header>
      <Modal.Body bsPrefix="contract-signing-body-modal">
        {
          contractStage === reachContractActionType.OPTIN 
            ? <div className="pb-3 text-center">
              <div>
              Opting-In to Contract
              </div>
              <div className="contract-signing-body-modal-stage text-center">
                Transaction (1 of 2)
              </div>
              </div>
            : contractStage === reachContractActionType.DEPOSIT
              ? <div className="pb-3">
                <div>
                Depositing Wager Amount
                </div>
                <div className="contract-signing-body-modal-stage text-center">
                  Transaction (2 of 2)
                </div>
                </div>
              : <div className="pb-3">Waiting for confirmation</div>  
        }
        <SigningLoader />
      </Modal.Body>
      <Modal.Footer bsPrefix="contract-signing-footer-modal">
        <div className={classnames({'hidden': !showFooter})}>Wallet not receiving request?</div>
        { !wait && <div 
          className={classnames("contract-signing-resend", {'hidden': !showFooter})}
          onClick={ resendNotification }
        >Resend notification</div>}
        { wait && <div className={classnames("contract-signing-resend", {'hidden': !showFooter})}>
          Wait for {countdown} seconds before retrying
          </div>}
      </Modal.Footer>
    </Modal>
  )
}