import { Modal } from "react-bootstrap"

export const LandingModal: React.FC<any> = (props) => {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      contentClassName="landing--modal--bg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="landing--modal--title">
          {
            process.env.NEXT_PUBLIC_NETWORK_MODE === 'MainNet'
            ? 'Welcome to ZONE (beta)'
            : 'Welcome to ZONE Testnet'
          }
        </div>
        <div className="landing--modal--info">
        {
            process.env.NEXT_PUBLIC_NETWORK_MODE === 'MainNet'
            ? <>
              You will need ALGOs to play. You are using the beta version of Zone. If you identify bugs or usability issues, please report them <a href="https://forms.gle/K46tozNPD364zN5P9" target="_blank" rel="noreferrer">here</a>. Join our social groups for more info!
            </>
            : <>
              You will need Testnet ALGOs to play. They have no monetary value and you can get them <a href="https://bank.testnet.algorand.network/" target="_blank" rel="noreferrer">here</a>. 
              You are using the beta version of Zone Testnet. If you identify bugs or usability issues, please report them <a href="https://forms.gle/K46tozNPD364zN5P9" target="_blank" rel="noreferrer">here</a>. Join our social groups for more info!
            </>
          }
        
        </div>
        <div className="d-flex flex-wrap">
          <a className="landing--modal--btn my-1" href="https://discord.com/invite/rKFBUhHV8B" target="_blank" rel="noreferrer">Discord</a>
          <a className="landing--modal--btn my-1" href="https://twitter.com/XTZ_ESPORTS" target="_blank" rel="noreferrer">Twitter</a>
          <a className="landing--modal--btn my-1" href="https://t.me/ZoneBYXtz" target="_blank" rel="noreferrer">Telegram</a>
        </div>
        <div className="landing--modal--hashtag">
          #WelcomeToTheZone
        </div>
      </Modal.Body>
    </Modal>
  )
}