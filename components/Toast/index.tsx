/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useAccount } from "../../context/AppContext/state";
import { toast } from "react-toastify";
import { useEffect } from "react";
import { actiontype } from "../../utils/constants";
import { StatusAPI } from "../../context/AppContext/types";
import { useReach } from "../../context/ReachContext/state";

const Toast: React.FC = () => {
    const { state: { error }, dispatch } = useAccount();
    const { state: { error: reachError } } = useReach();
    const clearData = {
        message: ''
    }

    useEffect(() => {
        if (error) {
            toast(error.message, 
            //     {
            //     onClose: () => dispatch({
            //         type: actiontype.CLEARERROR,
            //         payload: { error: clearData },
            //         status: { clearError: StatusAPI.CLEARERROR }
            //     })
            // }
            );
        }
        if (reachError) {
            toast(reachError.message, );
        }
    }, [error, reachError])

    return (
        <>
            <ToastContainer 
                position="bottom-right" 
                autoClose={4000} 
                hideProgressBar 
                newestOnTop={false} 
                closeOnClick rtl={false} 
                pauseOnFocusLoss 
                draggable={false} 
                pauseOnHover />
        </>
    )
}

export default Toast;