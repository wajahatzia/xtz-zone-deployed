import { useState, FC, useEffect } from "react";
import Link from 'next/link';
import { LandingModal } from "../Modals/LandingModal";

export const Landing: FC = () => {
    const [modalShow, setModalShow] = useState<undefined | boolean>();
    useEffect(() => {
        if( typeof modalShow === 'undefined'){
            setModalShow(true)
        }
    }, [modalShow])
    return (
        <>
        <LandingModal show={modalShow} onHide={() => setModalShow(false)}/>
        <section className="challenge-friend">
            <div className="container">
                <div className="row ">
                    <div className="col-sm-10 mx-auto ">
                        <div className="bg-black-radius align-items-center justify-content-center d-flex">
                            <div className="challenge-friend-inner challenge-image text-center">
                                <h2>Challenge friends to your favorite games.</h2>
                                <p>Just pick your opponent, set the wager amount and get ready to battle. Challenge anyone to a game. Wager your ALGO. Win big!</p>
                                <Link href="/connectWallet"><a>Challenge Now</a></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>
    )
}