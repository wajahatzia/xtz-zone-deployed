export {}
// import { AlgorandExtension } from '@magic-ext/algorand';
// import { Magic } from 'magic-sdk';
// import React, { useEffect, useState } from 'react'
// import { useForm } from 'react-hook-form';
// import { useAccount } from '../../context/AppContext/state';
// import { initMagicWalletToState } from '../../utils/common';
// import { setRole } from '../../context/AppContext/api';
// // eslint-disable-next-line react/display-name
// export const withMagic = (Component:any) => (props: any) => {
//   const { state, dispatch } = useAccount();
//   const [ isLoggedIn, setIsLoggedIn ] = useState<boolean>(false);
//   const { register, handleSubmit, watch, formState: { errors } } = useForm();
//   const initMagic = () => {
//     const algoExt = new AlgorandExtension({
//       rpcUrl: 'https://testnet.algoexplorerapi.io',

//     })
//     const magic = new Magic<any, any, any>('pk_live_00731A75840E8B0F', {
//       extensions: {
//         algorand: algoExt,
//       },
//     });
//     return magic
//   }
//   const onSubmit = async (value: any) => {
//     try{
//     const magic = initMagic()
//     const email = value.email
//     await magic.auth.loginWithMagicLink({ email });
//     setIsLoggedIn(true)
//     props?.P2 && props.setDecision(true)
//     props?.P1 && setRole(dispatch, 'P1')
//     props?.P2 && setRole(dispatch, 'P2')
//     await initMagicWalletToState(state, dispatch, magic, "")
//     } catch (error) {
//       console.log(error)
//     }
//   }
//   // eslint-disable-next-line react/display-name
//   return (<Component {...props} handleSubmit={handleSubmit} onSubmit={onSubmit} register={register} />)
// }