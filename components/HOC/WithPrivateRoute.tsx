import { useRouter } from "next/router";
import { useEffect } from "react";
import { useAccount } from "../../context/AppContext/state"

const withPrivateRoute = (Component:any) => (props:any) => {
  const { state: {walletStatus} } = useAccount();
  const router = useRouter();
  useEffect(() => {
    if (!walletStatus) {
      router.replace('/')
    }
  }, [router, walletStatus])
  return <Component {...props} />
}

export default withPrivateRoute;