import React, { useEffect, useRef, useState } from 'react';

export const BarLoader: React.FC = () => {
  const loader = useRef<any>()
  const parentLoader = useRef<any>()
  const [loadingCounter, setLoadingCounter] = useState<any>(0)
  
  useEffect(() => {
      const interval = setInterval(() => {
          if(loader.current) {
              setLoadingCounter(((loader.current.offsetWidth/parentLoader.current.offsetWidth)*100)) 
          }
      }, 100)
      if(loadingCounter >= '99') {
          setLoadingCounter(99)
          clearInterval(interval)
      }
  }, [loadingCounter])
  return (
    <div className="challenge-accept-loader-container">
      <div className="d-flex justify-content-between">
          <div>
            {
              loadingCounter < 30
                ? 'Setting up the game'
                : loadingCounter > 98 
                  ? 'Fetching data'
                  : loadingCounter > 70 ?
                    'Setting up Smart Contract'
                    : 'Setting up resources'
            }
          </div>
          <div>
              { loadingCounter && loadingCounter.toFixed(0)
              } {' '} %
          </div>
      </div>
      <div className="challenge-accept-loading-bar-container" ref={parentLoader}>
          <span className="challenge-accept-loading-bar" ref={loader}></span>
      </div>
    </div>
  )
}