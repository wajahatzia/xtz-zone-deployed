import React from 'react';

export const SigningLoader: React.FC = () => {
  return (
    <div className="signing-loader-container">
      <span className="signing-loader"></span>
    </div>
  )
}