import Image from "next/image";
import React from "react";
import closeImg from "../../../public/images/close-ic.png"
import acceptedImg from '../../../public/images/challenge-accepted.png'

export const Accepted: React.FC = () => {
    return (
        <section className="challenge-friend challenge-friend-step-1">
            <div className="container">
                <div className="row">
                    <div className="col-sm-10 mx-auto ">
                        <div className="bg-black-radius align-items-center justify-content-center d-flex animating__box-animation mx-auto">
                            <div>
                                
                                <div className="challenge-friend-inner challenge-friend-inner-step-2 accept-reject text-center">
                                    <Image src={acceptedImg} alt="challenge accepted" />
                                    <h2>CHALLENGE ACCEPTED!</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}