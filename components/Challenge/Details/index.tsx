import Image from "next/image";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useAccount } from "../../../context/AppContext/state";
import chessChallengeImg from '../../../public/images/chess-challenge.png'
import { countdownTimer } from "../../../utils/common";

type Props = {
    reset?: boolean
    wager?: string | string[] | undefined
}

const displayTime = (time: any) => {
    if(time.getSeconds() < 10) return `${time.getMinutes()}:0${time.getSeconds()}`
    else return `${time.getMinutes()}:${time.getSeconds()}`
}

export const Details: React.FC<Props> = ({ reset = false, wager }) => {
    useEffect(() => {
        console.log(wager)
    }, [wager])
    const [ countdownTime, setCountdownTime ] = useState(countdownTimer(new Date(), true))
    const { state: { player2Accept: { wagerAmount } } } = useAccount()
    const router = useRouter()
    useEffect(() => {
        let minutes = countdownTime.getMinutes()
        let seconds = countdownTime.getSeconds()
        if( minutes === 0 && seconds === 0){
            // TODO: Clear store at time end
            router.push("/reject")
        }
    }, [countdownTime, router])

    useEffect(() => {
        const timer = setTimeout(() => {
            setCountdownTime(countdownTimer(countdownTime, false))
        }, 1000)
        if( reset ){
            // To reset timeout after accepting challenge
            clearTimeout(timer)
            const timerAfterReset = setTimeout(() => {
                setCountdownTime(countdownTimer(countdownTime, false))
            }, 1000)
            return () => clearTimeout(timerAfterReset)
        }
        return () => clearTimeout(timer)

    })

    return (
        <div className="challenge-left text-center ">
        <h4>CHALLENGE</h4>
        <Image src={chessChallengeImg} alt="chess challenge" />
        <ul className="d-flex flex-row flex-xl-column justify-content-around justify-content-xl-center flex-wrap">
            <li>
                <span>GAME</span>CHESS
            </li>
            <li>
                <span>TIME</span>{displayTime(countdownTime)}
            </li>
            <li>
                <span>PRIZE</span>{wagerAmount && (wagerAmount / 1000000)* 2}{wager && (parseInt(wager as string)/1000000)*2} Algo
            </li>
        </ul>
    </div>
    )
}