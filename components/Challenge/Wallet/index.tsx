/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import { useRouter } from "next/router";
import React from "react";
import { useEffect } from "react";
import { useAccount } from "../../../context/AppContext/state";
import { initWallet } from "../../../utils/common";

export const ConnectWallet: React.FC = () => {
    const router = useRouter()
    const { state, dispatch } = useAccount()

    useEffect(() => {
        if (state.walletStatus) router.push('/acceptReject')
    }, [router, state.walletStatus])
    
    return (
        <section className="challenge-friend challenge-friend-step-1">
            <div className="container">
                <div className="row ">
                    <div className="col-sm-10 mx-auto ">
                        <div className="bg-black-radius align-items-center justify-content-center d-flex">
                            <div className="close-icon">
                                <a href="#"><img src="images/close-ic.png" /></a>
                            </div>
                            <div className="challenge-friend-inner challenge-friend-inner-step-1 text-center connect-wallet">
                                <h2>Connect your wallet to challenge and receive your winnings.</h2>
                                <p>Connect your My Algo Algorand Wallet to start playing.</p>
                                <a className="unsign-btn" onClick={() => initWallet(state, dispatch)}>Connect Wallet</a>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}