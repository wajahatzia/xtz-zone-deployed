import React from "react";
import { Details } from "../Details";
import classnames from 'classnames';
import { Player1Display } from "./Player1Display";
import { Player2Display } from "./Player2Display";

type Props = {
    role: string | null;
    showButton1: boolean;
    showButton2: boolean;
    handleButtonClickP1: ({retry, optIn}: {retry: boolean, optIn: boolean}) => Promise<void>;
    handleButtonClickP2: ({retry, optIn}: {retry: boolean, optIn: boolean}) => Promise<void>;
    url: string | null;
}

export const ChallengeDetails: React.FC<Props> = ({ 
    role, 
    showButton1, 
    showButton2, 
    handleButtonClickP1, 
    handleButtonClickP2,
    url,
    }) => {

    return (
        <section className="challenge-friend challenge-friend-step-1">
            <div className="container">
                <div className="row challenge__row">
                    <div className="col-sm-12  col-xl-11 mx-auto ">
                        <div className="bg-black-radius align-items-center justify-content-center d-flex flex-column flex-xl-row">
                            <Details reset={true}/>
                            <div className="challenge-right">
                                <div className="player-details d-flex flex-md-row flex-column justify-content-center mb-4">
                                    <Player1Display
                                        role={role}
                                        showButton={showButton1}
                                        handleButtonClick={handleButtonClickP1}
                                    />
                                    <div className="player-1">
                                        <h2 style={{margin: "auto"}}>VS</h2>
                                    </div>
                                    <Player2Display
                                        role={role}
                                        showButton1={showButton1}
                                        showButton2={showButton2}
                                        handleButtonClick={handleButtonClickP2}
                                    />
                                </div>
                                <div className={classnames("join-game-btn text-center", { 'hideDiv': (showButton1 || showButton2) })}>
                                    {url && 
                                        <a href={url} target="_blank" rel="noreferrer" >
                                            <button className="unsign-btn"> Join Game</button>
                                        </a>
                                    }
                                </div>
                                <p>Once you sign the challenge, the wager amount will be deposited in the smart contract and will be released according to the game result.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
