import React, { useEffect, useState } from "react";
import { SignContract, SignContractStatusAPI } from "../../../context/AppContext/api";
import { useAccount } from "../../../context/AppContext/state";
import { StatusAPI } from "../../../context/AppContext/types";
import { attachPlayer1, attachPlayer2 } from "../../../context/ReachContext/api";
import { useReach } from "../../../context/ReachContext/state";
import { Result } from "./Result";
import { ChallengeDetails } from "./ChallengeDetails";
import { actiontype } from "../../../utils/constants";
import { ContractSigningModal } from "../../Modals/ContractSigningModal";

export const Overview: React.FC = () => {
    const { state: { wallet: { account, balance }, player2Accept, role, status }, dispatch } = useAccount()
    const { state: { interactP1, interactP2, status: { attachP1, attachP2, signedP1Status, signedP2Status } }, dispatch: ReachDispatch } = useReach()
    const [showButton1, setShowButton1] = useState<boolean>(true);
    const [showButton2, setShowButton2] = useState<boolean>(true);
    const [modalShow, setModalShow] = useState<undefined | boolean>(false);

    useEffect(() => {
        if( role === 'P1' )
            player2Accept.oppSigned && setShowButton2(false)
        if ( role === 'P2' )
            player2Accept.oppSigned && setShowButton1(false)
    }, [player2Accept.oppSigned, role])

    useEffect(() => {
        player2Accept._id && SignContractStatusAPI(dispatch, {userId: account.networkAccount.addr, gameId: player2Accept._id})
    }, [account, dispatch, player2Accept._id])
    useEffect(() => {
        const sendSignedStatus = async () => {
            player2Accept._id && await SignContract(dispatch, {username: account.networkAccount.addr, userId: account.networkAccount.addr, gameId: player2Accept._id, status: 'signed'})
        }
        if ( attachP1 === StatusAPI.FULFILLED) {
            sendSignedStatus() // await
            setShowButton1(false)
            setModalShow(false)
        }
        if (attachP2 === StatusAPI.FULFILLED) {
            sendSignedStatus() //await
            setShowButton2(false)
            setModalShow(false)
        }
    }, [account?.networkAccount?.addr, attachP1, attachP2, dispatch, player2Accept?._id])

    const handleClick = async ({retry = false, optIn = true}) => {
        console.log('attachP1', attachP1)
        console.log('retry', retry)
        if( (attachP1 === StatusAPI.IDLE) || retry ){
            try {
                setModalShow(true)
                await attachPlayer1(ReachDispatch, { account: account, wager: player2Accept.wagerAmount, matchId: player2Accept.matchId, optIn: optIn})
            } catch (error: any) {
                if (error.message === 'Can not open popup window - blocked') {
                    dispatch({
                      type: actiontype.REJECTED,
                      payload: { error: { message: "Your browser is blocking the pop-up windows on this site. Please adjust your browser settings"} },
                      status: { sendWager: StatusAPI.REJECTED }
                    })
                }
            }
        }
    }
    const handleClick2 = async ({retry = false, optIn = true}) => {
        console.log('attachP2', attachP2)
        console.log('retry', retry)
        if (player2Accept.oppSigned && (attachP2 === StatusAPI.IDLE || retry)) {
            try {
                setModalShow(true)
                await attachPlayer2(ReachDispatch, { account: account, wager: player2Accept.wagerAmount, matchId: player2Accept.matchId, optIn: optIn })
            } catch (error: any) {
                if (error.message === 'Can not open popup window - blocked') {
                    dispatch({
                      type: actiontype.REJECTED,
                      payload: { error: { message: "Your browser is blocking the pop-up windows on this site. Please adjust your browser settings"} },
                      status: { sendWager: StatusAPI.REJECTED }
                    })
                }
            }
        }
    }

    return (
        <>
        <ContractSigningModal  
            show={modalShow} 
            role={role} 
            handleButtonClickP1={handleClick}
            handleButtonClickP2={handleClick2}
        />
            {status?.winner ? 
                <Result role={role} winner={player2Accept.result.winner}/> 
                : 
                <ChallengeDetails 
                    role={role} 
                    handleButtonClickP1={handleClick}
                    handleButtonClickP2={handleClick2}
                    showButton1={showButton1}
                    showButton2={showButton2}
                    url={player2Accept?.url}
                /> }
        </>
    )
}
