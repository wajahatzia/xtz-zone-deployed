import react from 'react';
import Image from 'next/image';
import classnames from 'classnames';
import checkImg from '../../../public/images/check.png'
import { Spinner } from "react-bootstrap";
import userSigned from '../../../public/images/user-with-sign.png';
import userNotSigned from '../../../public/images/user-without-sign.png';

type Props = {
  role: string | null;
  showButton: boolean;
  handleButtonClick: ({retry, optIn}: {retry: boolean, optIn: boolean}) => Promise<void>;
}

export const Player1Display: React.FC<Props> = ({
  role,
  showButton,
  handleButtonClick
}) => {
  return (
    <div className="player-1">
        <h3>{role === 'P1' ? 'You' : 'Player 1'}</h3>
        <Image
            src={(showButton) ? userNotSigned : userSigned}
            layout="intrinsic"
            alt="signed status"
        />
        <span className={classnames("signed-btn", { "hideDiv": showButton })} >
            <Image src={checkImg} alt="check" />{' '}Signed
        </span>
        <div className={classnames({ "hideDiv": !showButton })}>
            {
            role === 'P2' 
                ? <div className="player-waiting">
                        Awaiting Confirmation
                        <div className="bar-loader-container">
                            <span className="bar-loader bar-loader-1"></span>
                            <span className="bar-loader bar-loader-2"></span>
                            <span className="bar-loader bar-loader-3"></span>
                        </div>
                    </div>
                :
                    <button onClick={() => handleButtonClick({retry: false, optIn: true})} className="unsign-btn mt-3" disabled={role === 'P2'}>
                        Sign
                    </button>
            }
        </div>
    </div>
  )
}