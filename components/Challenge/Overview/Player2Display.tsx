import { useEffect } from 'react';
import Image from 'next/image';
import classnames from 'classnames';
import checkImg from '../../../public/images/check.png'
import userSigned from '../../../public/images/user-with-sign.png';
import userNotSigned from '../../../public/images/user-without-sign.png';
import { useAccount } from '../../../context/AppContext/state';

type Props = {
  role: string | null;
  showButton1: boolean;
  showButton2: boolean;
  handleButtonClick: ({retry, optIn}: {retry: boolean, optIn: boolean}) => Promise<void>;
}

export const Player2Display: React.FC<Props> = ({
  role,
  showButton1,
  showButton2,
  handleButtonClick
}) => {
const { state: { player2Accept } } = useAccount()
useEffect(() => {
    console.log('oppSigned', player2Accept?.oppSigned)
}, [player2Accept?.oppSigned])
  return (
    <div className="player-2">
        <h3>{role === 'P2' ? 'You' : 'Player 2'}</h3>
        <Image
            src={showButton2 ? userNotSigned : userSigned}
            layout="intrinsic"
            alt="signed status"
            className={classnames({'player-img-border': showButton2})}
          />
        <span className={classnames("signed-btn", { "hideDiv": showButton2 })}>
            <Image src={checkImg} alt="signed status" />{' '}Signed
        </span>
        <div className={classnames({ "hideDiv": !showButton2 })}>
            {
            role === 'P1' 
                ?
                    showButton1 
                        ?
                            <div className="player-waiting">
                                Waiting for your Response
                            </div>
                        :
                            <div className="player-waiting">
                                Awaiting Confirmation
                                <div className="bar-loader-container">
                                    <span className="bar-loader bar-loader-1"></span>
                                    <span className="bar-loader bar-loader-2"></span>
                                    <span className="bar-loader bar-loader-3"></span>
                                </div>
                            </div>

                :
                    <>
                    <button onClick={() => handleButtonClick({retry: false, optIn: true})} className="unsign-btn mt-3 p2--button" disabled={!player2Accept.oppSigned}>
                        Sign
                    </button>
                    {role === 'P2' && !player2Accept.oppSigned && <div className="p2--info">You can sign once your opponent signs the contract</div>}
                    </>
            }
        </div>
    </div>    
  )
}