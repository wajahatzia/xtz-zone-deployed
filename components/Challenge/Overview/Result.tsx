import Image from "next/image";
import React from "react";
import closeImg from '../../../public/images/close-ic.png'
import userWithSignImg from '../../../public/images/user-with-sign.png'

type Props = {
    role: string | null;
    winner: string;
}

export const Result: React.FC<Props> = ({ role, winner }) => {
    return (
        <section className="challenge-friend challenge-friend-step-1">
            <div className="container">
                <div className="row ">
                    <div className="col-sm-10 mx-auto ">
                        <div className="bg-black-radius align-items-center justify-content-center d-flex animating__box-animation mx-auto">
                            <div className="animating__box-content">
                                <div className="challenge-right challenge-right-winner">
                                    <div className="player-details d-flex flex-row justify-content-center align-items-center">
                                        <div className="player-2 image-center-user">
                                            <h4>Winner</h4>
                                            <div className="image-user">
                                                <Image src={userWithSignImg} className="img-fluid" alt="user with sign"/>
                                                {(role === 'P1' && winner === 'white') && <h3> You</h3>}
                                                {(role === 'P1' && winner === 'black') && <h3> Player 2</h3>}
                                                {(role === 'P2' && winner === 'black') && <h3>You</h3>}
                                                {(role === 'P2' && winner === 'white') && <h3>Player 1</h3>}

                                            </div>
                                            <h2>{((winner === "white" && role === 'P1') || (winner === 'black' && role === 'P2')) ? "Congratulation!" : "Better Luck Next Time"}</h2>
                                            {(winner === "white" && role === 'P1') && <p>You won the Challenge. Prize Money is transferred in your wallet</p> }
                                            {(winner === "black" && role === 'P2') && <p>You won the Challenge. Prize Money is transferred in your wallet</p> }
                                            {(winner === "white" && role === 'P2') && <p>Player 1 has won the Challenge.</p>}
                                            {(winner === "black" && role === 'P1') && <p>Player 2 has won the Challenge.</p>}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
