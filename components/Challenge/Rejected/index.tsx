import React from "react";
import Image from 'next/image';
import img from '../../../public/images/reject-image.png';
import closeImg from "../../../public/images/close-ic.png";

export const ChallengeRejected: React.FC = () => {
    return (
        <section className="challenge-friend challenge-friend-step-1">
            <div className="container">
                <div className="row ">
                    <div className="col-sm-10 mx-auto ">
                        <div className="bg-black-radius align-items-center justify-content-center d-flex animating__box-animation mx-auto">
                            <div className="animating__box-content">
                                <div className="challenge-friend-inner challenge-friend-inner-step-2 reject-challenge text-center">
                                    <Image
                                        src={img}
                                        layout="intrinsic" 
                                        alt="chickened out"
                                    />
                                    <h2>Sorry! Your opponent chickened out! </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}