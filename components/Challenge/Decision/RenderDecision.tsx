import React from "react";
import { Details } from "../Details";
import Image from 'next/image';
import img from '../../../public/images/chess-table-big.png'
import { StatusAPI } from "../../../context/AppContext/types";
// import { withMagic } from "../../HOC/WithMagic";
import { BarLoader } from "../../Loaders/BarLoader";

type Props = {
    handleButtonClick: (flag: boolean) => void;
    loading: string;
    // setDecision: any;
    wager: string | string[] | undefined;
}


// const MagicForm: React.FC = ({handleSubmit, onSubmit, register, setDecision}: any) => {
//     return (
//         <form onSubmit={handleSubmit(onSubmit)}>
//             <input type="email" placeholder="Enter your email" {...register('email')}/>
//             <button type="submit">Accept and Connect Wallet with email</button>
//         </form>    
//     )
// }

// const ConnectWalletWithMagic = withMagic(MagicForm)

export const RenderDecision: React.FC<Props> = ({handleButtonClick, loading, wager}) => {
    
    return (
        <section className="challenge-friend challenge-friend-step-1">
            <div className="container">
                <div className="row challenge__row">
                    <div className="col-sm-12  col-xl-11 mx-auto ">
                        <div className="bg-black-radius align-items-center justify-content-center d-flex flex-column flex-xl-row">
                            <Details wager={wager}/>
                            <div className="challenge-right px-3">
                                <div className="challenge-friend-inner challenge-friend-inner-step-2 accept-reject d-flex flex-column  justify-content-center align-items-center">
                                    <Image
                                        src={img}
                                        layout="intrinsic"
                                        alt="img"
                                    />
                                    <h2>You have been challenged in Chess!</h2>
                                    <p className="challenge-friend-inner-p">Accept Challenge and start playing.</p>
                                    <div className="join-game-btn d-flex flex-column flex-md-row justify-content-center align-items-center py-3">
                                        <button 
                                            className="unsign-btn my-3 mx-1" 
                                            onClick={() => handleButtonClick(true)}
                                        >
                                            {
                                                loading === StatusAPI.LOADING 
                                                    ? 'Loading...' 
                                                    : 'Accept Challenge'
                                            }
                                        </button>
                                        {/* <ConnectWalletWithMagic P2={true} setDecision={setDecision}/>*/}
                                        <button className="reject-btn ms-md-4" onClick={() => handleButtonClick(false)}>Reject Challenge</button>
                                    </div>
                                </div>
                                {
                                    loading === StatusAPI.LOADING && <BarLoader />
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
