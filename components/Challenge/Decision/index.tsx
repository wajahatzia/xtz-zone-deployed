import { NextRouter, useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { DecisionChallengeAPI, setRole } from '../../../context/AppContext/api'
import { StatusAPI } from '../../../context/AppContext/types'
// import { setP2Interact } from '../../../context/ReachContext/api'
import { initWallet } from '../../../utils/common'
import { withRouter } from "next/router"
import { useReach } from '../../../context/ReachContext/state'
import { useAccount } from '../../../context/AppContext/state'
import { RenderDecision } from './RenderDecision'
import { reachActionType } from '../../../utils/constants'

type Props = {
  router: NextRouter;
}

const AcceptReject: React.FC<Props> = ({ router }) => {
    const { query: { id, wagerAmount } } = router
    const route = useRouter()
    const { dispatch: ReachDispatch } = useReach();
    const { state, state: { player2Accept, status, walletStatus, wallet: { account } }, dispatch } = useAccount()
    const [ decision, setDecision ] = useState<boolean | null>(null)

    useEffect(() => {
        if (status.acceptChallenge === StatusAPI.FULFILLED && decision) {
            // setP2Interact(ReachDispatch, {
            //     getChallengeParams() {
            //         return [Number(player2Accept?.wagerAmount), account?.networkAccount?.addr, player2Accept.matchId, 'date', 217532199, { metaData: 'black' }]
            //     },
            //     displayWinner: (challengeId: any, winnerAddress: any,loserAddress: any, wager: any, fees: any, token: any) => {
            //         console.log(winnerAddress)
            //     },
            //     log: (payload: any) => console.log(JSON.stringify(payload)),
            //     signed: () => { 
            //         console.log('signed p2')
            //         ReachDispatch({ type: reachActionType.FULFILLED, status: { signedP2Status: true }})
            //     }
            // })
            route.push('/challenge')
        }
        if( status.acceptChallenge === StatusAPI.FULFILLED && !decision) route.push('/reject')
    }, [route, status.acceptChallenge, decision, ReachDispatch, player2Accept?.wagerAmount, player2Accept.appId, account, player2Accept.matchId])

    useEffect(() => {
        if (status.acceptChallenge === StatusAPI.IDLE && walletStatus && decision) {
            const data = {
                username: account?.networkAccount?.addr,
                userId: account?.networkAccount?.addr,
                gameId: id,
                status: 'accepted'
            }
            DecisionChallengeAPI(dispatch, data)
        }
        // if (status.acceptChallenge === StatusAPI.IDLE && walletStatus && !decision) {
        //     const data = {
        //         username: account?.networkAccount?.addr,
        //         userId: account?.networkAccount?.addr,
        //         gameId: id,
        //         status: 'rejected'
        //     }
        //     DecisionChallengeAPI(dispatch, data)
        // }
    }, [dispatch, status.acceptChallenge, walletStatus, decision, account?.networkAccount?.addr, id])

    const handleClick = (flag: boolean): void => {
        flag ? setDecision(true) : setDecision(false)
        if (!walletStatus ) {
            initWallet(state, dispatch)
            setRole(dispatch, 'P2')
        }
    }
  return (
    <RenderDecision handleButtonClick={handleClick} loading={status.acceptChallenge} wager={wagerAmount}/>
  )
}

export default withRouter(AcceptReject)