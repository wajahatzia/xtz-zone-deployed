import React from "react";
import { useRouter } from 'next/router';
import { useAccount } from "../../context/AppContext/state";
import { useEffect } from "react";
import { initWallet } from "../../utils/common";
import { setRole } from "../../context/AppContext/api";

export const FirstStep: React.FC = () => {
  const router = useRouter();
  const { state, dispatch } = useAccount();

  useEffect(() => {
    if (state.walletStatus) router.push('/step?tab=2')
  }, [router, state.walletStatus])

  return (
    <>
      <h2>Connect your wallet to challenge and receive your winnings.</h2>
      <p>Connect your My Algo Algorand Wallet to start playing.</p>
      <button onClick={() => {initWallet(state, dispatch); setRole(dispatch, 'P1')}}>Connect Wallet</button><br/>
    </>
  )
}

export default FirstStep
