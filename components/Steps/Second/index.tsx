import React from "react";
import Link from 'next/link';
import Image from "next/image";
import chessIcon from '../../../public/images/hover-chess.png'
import game1Icon from '../../../public/images/game-1.png'
import game2Icon from '../../../public/images/Group 799.png'
import game3Icon from '../../../public/images/game-3.png'
import game4Icon from '../../../public/images/game-4.png'

export const SecondStep: React.FC = () => {
    return (
        <>
            <h2>Select your game.</h2>
            <div className="game-selection">
                <div className="game-selection-inner flex-column flex-md-row d-flex justify-content-center align-items-center flex-wrap pb-3">
                    <Link href="/step?tab=3" passHref>
                        <button className="game-box active justify-content-center align-items-center d-flex my-3 my-lg-0">
                            <div className="hover-game-box">
                                <Image src={chessIcon} alt="chess"/>
                            </div>
                            <Image src={game1Icon} alt="game 1" />
                        </button>
                    </Link>

                    <button className="game-box justify-content-center align-items-center d-flex disabled my-3 my-lg-0" aria-disabled="true" >
                        <Image src={game2Icon} alt="game 2"/>
                        <p>Coming Soon</p>
                    </button>
                    <button className="game-box justify-content-center align-items-center d-flex disabled my-3 my-lg-0" aria-disabled="true">
                        <Image src={game3Icon} alt="game 3"/>
                        <p>Coming Soon</p>
                    </button>
                    <button  className="game-box justify-content-center align-items-center d-flex disabled my-3 my-lg-0" aria-disabled="true">
                        <Image src={game4Icon} alt="game 4"/>
                        <p>Coming Soon</p>
                    </button>
                </div>
            </div>
        </>
    )
}

export default SecondStep