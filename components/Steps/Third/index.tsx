import React from "react";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useAccount } from "../../../context/AppContext/state";
import { useRouter } from "next/router";
import { StatusAPI } from "../../../context/AppContext/types";
import { useEffect } from "react";
import { generateLinkAPI } from "../../../context/AppContext/api";
import { ThirdDisplay } from "./ThirdDisplay";
import { actiontype } from "../../../utils/constants";

const schemaWager = (balance:string | null ) => {
    const value = parseFloat(balance ? balance : '0')
    const schema = yup.object().shape({
        wager: yup.number().positive().max(value, "You have insufficient balance, please enter a different value").required()
    })
    return schema
}

export const ThirdStep: React.FC = () => {
    const router = useRouter()
    const { state: { wallet: { balance , account }, status: { sendWager } }, dispatch } = useAccount()
    const { register, handleSubmit, formState: { errors } } = useForm<any>({
        resolver: yupResolver(schemaWager(balance))
    })
    const onSubmit = handleSubmit(async (data) => {
        const bodyData = {
            username:account?.networkAccount?.addr,
            userId:account?.networkAccount?.addr,
            wagerAmount: (data.wager * 1000000),
            game:"chess"
        }
        try{
            await generateLinkAPI(dispatch, bodyData)
        } catch (error) {
            console.log(error)
        }
    })
    useEffect(() => {
        if(errors.wager) {
            dispatch({
                type: actiontype.REJECTED,
                payload: { error: { message: errors.wager.message } },
                status: { wagerVerification: StatusAPI.REJECTED }
              })
        }
    }, [dispatch, errors])
    useEffect(() => {
        if(sendWager === StatusAPI.FULFILLED) router.push('/step?tab=4')
    }, [router, sendWager])

    return (
        <ThirdDisplay onSubmit={onSubmit} register={register} balance={balance} errors={errors}/>
    )
}

export default ThirdStep