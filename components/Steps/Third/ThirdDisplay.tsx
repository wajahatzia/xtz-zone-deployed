import React from 'react'

type Props = {
  onSubmit: (e?: React.BaseSyntheticEvent<object, any, any> | undefined) => Promise<void>;
  errors: any;
  register: any;
  balance: string | null;
}

export const ThirdDisplay: React.FC<Props> = ({ onSubmit, errors, register, balance }) => {
  return (
    <form onSubmit={onSubmit}>
        <h2>Set Wager Amount.</h2>
        <div className="set-wager-amount">
            <div className="wager-box">
                <input type="number" step="any" placeholder="50" defaultValue={0} {...register("wager")}/>
                <h4>Algo</h4>
            </div>
            {/* <div className="text-danger" style={{maxWidth: '340px'}}>{errors.wager && errors.wager.message}</div> */}
            <p>Available Wallet Balance : {balance} Algo</p>
        </div>
        <button className="btn-signup" type="submit">Continue</button>
    </form>
  )
}