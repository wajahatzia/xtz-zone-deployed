import Image from 'next/image';
import React from 'react'
import shareImg from '../../../public/images/share.png';
import copyImg from '../../../public/images/copy.png';
import { copyToClipboard } from '../../../utils/common';
import classnames from "classnames";
import { Spinner } from 'react-bootstrap';

type Props = {
  copyTextRef: React.RefObject<HTMLParagraphElement>;
  url: string | null;
  showAwait: boolean;
  timeString: string | undefined;
  handleButtonClick: () => void;
}

export const FourthDisplay: React.FC<Props> = ({
  copyTextRef,
  url,
  showAwait,
  timeString,
  handleButtonClick
}) => {
  return (
    <>
      <h2>Share this invite link with your friend. Let the games begin!</h2>
      <div className="set-wager-amount d-flex align-items-center justify-content-center set-copy-amount">
          <div className="wager-box copy-box">
              <p ref={copyTextRef}>{url}</p>
              <a href="#" className="copy" onClick={() => copyToClipboard(copyTextRef)}><Image src={copyImg} alt="copy" /></a>
          </div>
          <div className="share-icon">
              <a href="#"><Image src={shareImg} alt="share"/></a>
          </div>
      </div>
      <p className={classnames({ 'hideDiv': showAwait })} >Click on the button below if you have shared the link</p>
      <div className={classnames({ 'hideDiv': showAwait })}>
          <button onClick={handleButtonClick} >I have shared the link</button>
      </div>
      <div className={classnames({ 'hideDiv': !showAwait })}>
          <button className="await-btn">
            <Spinner animation="border" role="status" />
            <span className="ms-2">I have shared the link</span>
          </button>
      </div>
      <div className={classnames("fourthStepTimer", { 'hideDiv': !showAwait })}>
        <span>The challenge will expire in <span>{timeString}</span></span>
      </div>

    </>
  )
}
