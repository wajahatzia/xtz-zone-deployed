import { useRouter } from "next/router";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useAccount } from "../../../context/AppContext/state";
import { StatusAPI } from "../../../context/AppContext/types";
import { useReach } from "../../../context/ReachContext/state";
import { useRef } from "react";
// import { setP1Interact } from "../../../context/ReachContext/api";
import { FourthDisplay } from "./FourthDisplay";
import { progress } from "../../../utils/common";
import { SignContract } from "../../../context/AppContext/api";
import { reachActionType } from "../../../utils/constants";

export const FourthStep: React.FC = () => {
    const router = useRouter()
    const { state: { wallet: { account }, player2Link: { gameId, url }, player2Accept, status: { notifyPlayer1: notification } }, dispatch } = useAccount()
    const { dispatch: ReachDispatch } = useReach()
    const [showAwait, setShowAwait] = useState<boolean>(false);
    const [timeString, setTimeString] = useState<string>();
    const notifyParams = account?.networkAccount?.addr + '/' + gameId;
    const copyTextRef = useRef<HTMLParagraphElement>(null);

    const handleClick = () => {
        setShowAwait(!showAwait);
        progress(900, setTimeString, dispatch, notifyParams);
    }
    useEffect(() => {
        if (notification === StatusAPI.FULFILLED) {
            // setP1Interact(ReachDispatch, {
            //     wager: Number(player2Accept?.wagerAmount),
            //     getChallengeParams: () => {
            //         return [Number(player2Accept?.wagerAmount), account?.networkAccount?.addr, player2Accept.matchId, 'date', 217532199, { metaData: 'white' }]
            //     },
            //     displayWinner: (challengeId: any, winnerAddress: any,loserAddress:any, wager: any, fees: any, token: any) => {
            //         console.log(winnerAddress)
            //     },
            //     log: (payload: any) => console.log(JSON.stringify(payload)),
            //     signed: () => { 
            //         console.log('signed p1')
            //         ReachDispatch({ type: reachActionType.FULFILLED, status: { signedP1Status: true }})
            //     }
            // })
            router.push('/challenge')
        }
    }, [ReachDispatch, account, dispatch, notification, player2Accept._id, player2Accept.appId, player2Accept.matchId, player2Accept?.wagerAmount, router])
    return (
        <FourthDisplay 
            copyTextRef={copyTextRef}
            url={url}
            showAwait={showAwait}
            timeString={timeString}
            handleButtonClick={handleClick}
        />
    )
}

export default FourthStep