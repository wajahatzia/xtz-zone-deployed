import Head  from "next/head"

export const Metadata: React.FC<{title?: string, description?: string}> = ({title, description}) => {
  return (
    <Head>
      `<meta charSet="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <title>{title}</title>
        <meta name="description" content={description} />
        <link rel="image/png" href="/favicon.png" />
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-VEWRF7QLQ9"></script>
        <script
        dangerouslySetInnerHTML={{
          __html:`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-VEWRF7QLQ9');
          `
        }} />
      </Head>
  )
}