import React from "react";
import Link from 'next/link';
import { NextRouter, withRouter } from "next/router"
import classnames from "classnames";
import dynamic from "next/dynamic";
// import { withMagic } from "../../HOC/WithMagic";

const FirstStep = dynamic(() => import("../../Steps"))
const SecondStep = dynamic(() => import("../../Steps/Second"))
const ThirdStep = dynamic(() => import("../../Steps/Third"))
const FourthStep = dynamic(() => import("../../Steps/Fourth"))

// const FirstStepWithMagic = withMagic(FirstStep)

type Props = {
    router: NextRouter;
}

const Tabs: React.FC<Props> = ({ router }) => {
    const {
        query: { tab }
    } = router

    const isTabOne = tab === "1" || tab == null
    const isTabTwo = tab === "2"
    const isTabThree = tab === "3"
    const isTabFourth = tab === "4"

    return (
        <section className="challenge-friend challenge-friend-step-1">
            <div className="container">
                <div className="row ">
                    <div className="col-sm-10 mx-auto ">
                        <div className="bg-black-radius align-items-center justify-content-center d-flex">
                            <div className="challenge-friend-inner game-box-inner challenge-friend-inner-step-1 challenge-step text-center py-3">
                                <div className="tab-dots-step">
                                    <ul>
                                        <li className={classnames("list-inline-item", { 'active': isTabOne })}><a></a>
                                        </li>
                                        <li className={classnames("list-inline-item", { 'active': isTabTwo })}><a></a>
                                        </li>
                                        <li className={classnames("list-inline-item", { 'active': isTabThree })}><a></a>
                                        </li>
                                        <li className={classnames("list-inline-item", { 'active': isTabFourth })}><a></a>
                                        </li>
                                    </ul>
                                    {isTabOne && <h3>Step 1</h3>}
                                    {isTabTwo && <h3>Step 2</h3>}
                                    {isTabThree && <h3>Step 3</h3>}
                                    {isTabFourth && <h3>Step 4</h3>}
                                </div>
                                {isTabOne && <FirstStep />}
                                {/* {isTabOne && <FirstStep />} */}
                                {isTabTwo && <SecondStep />}
                                {isTabThree && <ThirdStep />}
                                {isTabFourth && <FourthStep />}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}


export default withRouter(Tabs)