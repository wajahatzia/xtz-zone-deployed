import React from 'react';
import Image from 'next/image';
import logo from '../../../public/images/xtz-logo.png';
import logoWhite from '../../../public/images/xtz-logo-white.png'
import classnames from 'classnames'
import { useAccount } from '../../../context/AppContext/state';
import { initWallet } from '../../../utils/common';
import currencyLogo from '../../../public/images/image-circle-logo.png';
import Link from 'next/link';
import { Navbar, Nav, Container, Dropdown } from 'react-bootstrap'
import { actiontype } from '../../../utils/constants';

type Props = {
  yellow: boolean;
  showButton?: boolean;
  isDisabled?: boolean;
}

export const NavBar: React.FC<Props> = ({ yellow, isDisabled }) => {
  const { state, dispatch } = useAccount()
  const handleDisconnect = () => {
    dispatch({type: actiontype.CLEAR_WALLET})
    if (typeof window !== "undefined") {  
      localStorage.removeItem("walletconnect")
      window.location.reload()  
    }
  }
  return (
    <header className={classnames('py-3', { 'yellow-bg-head': yellow })}>
      <div className="container">
        <Navbar expand="md">
          <Container>
            {isDisabled ? 
              <Link href="" passHref>
                <Navbar.Brand className="d-flex align-items-center">
                  <Image src={yellow ? logoWhite : logo} alt="xtx logo" />
                </Navbar.Brand>
              </Link>  
            :
            <Link href="/" passHref>
            <Navbar.Brand className="d-flex align-items-center">
              <Image src={yellow ? logoWhite : logo} alt="xtx logo" />
            </Navbar.Brand>
          </Link>  
          }
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav w-100">
              <Nav className="ms-auto w-100">
              { state.walletStatus ? 
                <>
                  <form className="button-right me-md-0 w-100 d-flex justify-content-center align-items-center justify-content-md-end flex-column flex-sm-row flex-wrap">
                    <div className="time-count me-sm-5 d-flex align-items-center">
                      <div className="me-2">
                      <Image src={currencyLogo} quality={100} alt="currency-logo"/>
                      </div>
                      <span>{state?.wallet?.balance}</span>
                    </div>
                    <div className="d-flex flex-nowrap">
                    <a href="#" className={"btn-signup btn-nav-dropdown"}>{state?.wallet?.account?.networkAccount?.addr}</a>
                    <Dropdown align="end" className={classnames("btn-dropdown", { "btn-dropdown-bg-yellow": !yellow, "btn-dropdown-bg-white": yellow})}>
                    <Dropdown.Toggle>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item 
                        className="text-center"
                        onClick={handleDisconnect}
                        >
                        Disconnect
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                  </div>
                  </form>
                </>
                :
                <form className="button-right me-md-0 w-100 d-flex justify-content-center justify-content-md-end">
                    <span className="btn-signup" onClick={() => initWallet(state, dispatch)}>Connect Wallet</span>
                </form>
              }
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
    </header>
  )
}

export default NavBar;