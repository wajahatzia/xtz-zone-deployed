/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import React from "react";
import Image from 'next/image';
import classnames from 'classnames';
type Props = {
    yellow: boolean
}

export const Footer: React.FC<Props> = ({yellow}) => {
    return (
        <footer className={classnames({ 'footer-yellow': yellow })}>
        <div className="container">
          <div className="row">
            <div className="col-sm-6 col-md-4">
              <div className="footer-inner">
                <div className="subscribe">
                  <h5>Subscribe to our newsletter</h5>
                  <div className="input-subscribe">
                    <input type="text" name="" placeholder="Email address"/>
                    <button type="button" className="button-ylow"><img src="images/right-arrow.png" alt="right arrow"/></button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-md-3">
              <div className="footer-inner">
                <h4>Services</h4>
                <ul className="list-inline">
                  <li><a href="#">Esports Events</a></li>
                  <li><a href="#">Esports Marketing</a></li>
                  <li><a href="#"> Broadcasting </a></li>
                </ul>
              </div>
            </div>
            <div className="col-sm-6 col-md-2">
              <div className="footer-inner">
                <h4>About</h4>
                <ul className="list-inline">
                  <li><a href="#">Our Story</a></li>
                  <li><a href="#">Benefits</a></li>
                  <li><a href="#"> Team </a></li>
                  <li><a href="#"> Careers </a></li>
                </ul>
              </div>
            </div>
            <div className="col-sm-6 col-md-3">
              <div className="footer-inner">
                <h4>Help</h4>
                <ul className="list-inline">
                  <li><a href="#">FAQs</a></li>
                  <li><a href="#">Contact Us</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div className="row last-footer-row align-items-center">
            <div className="col-sm-6 col-md-6 col-lg-4">
              <ul className="list-inline terms">
                  <li className="list-inline-item"><a href="#">Terms & Conditions</a></li>
                  <li className="list-inline-item"><a href="#">Privacy Policy</a></li>
              </ul>
            </div>
            <div className="col-sm-6 col-md-6 col-lg-8">
              <ul className="list-inline soical-ics">
                  <li className="list-inline-item"><a href="#"><img src="images/foot-soc-1.png"/></a></li>
                  <li className="list-inline-item"><a href="#"><img src="images/foot-soc-2.png"/></a></li>
                  <li className="list-inline-item"><a href="#"><img src="images/foot-soc-3.png"/></a></li>
                  <li className="list-inline-item"><a href="#"><img src="images/foot-soc-4.png"/></a></li>
                  <li className="list-inline-item"><a href="#"><img src="images/foot-soc-5.png"/></a></li>
                  <li className="list-inline-item"><a href="#"><img src="images/foot-soc-6.png"/></a></li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    )
}

export default Footer