/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import React from "react";
import Image from 'next/image';
import classnames from 'classnames';
import instaIcon from '../../../public/images/foot-soc-6.png'
import linkedinIcon from '../../../public/images/foot-soc-5.png'
import discordIcon from '../../../public/images/foot-soc-4.png'
import youtubeIcon from '../../../public/images/foot-soc-3.png'
import twitterIcon from '../../../public/images/foot-soc-2.png'
import fbIcon from '../../../public/images/foot-soc-1.png'
import telegramIcon from '../../../public/images/foot-soc-7.png'
import redditIcon from '../../../public/images/foot-soc-8.png'


type Props = {
    yellow: boolean
}

export const Footer: React.FC<Props> = ({yellow}) => {
    return (
        <footer className={classnames({ 'footer-yellow': yellow })}>
        <div className="container">
          <div className="row">
            <div className="col-6 d-flex gap-2">
              <a href="https://www.facebook.com/XTZesports.in" target="_blank" rel="noreferrer">
                <Image src={fbIcon} alt="facebook" />
              </a>
              <a href="https://twitter.com/XTZ_ESPORTS" target="_blank" rel="noreferrer">
                <Image src={twitterIcon} alt="twitter"/>
              </a>
              <a href="https://www.youtube.com/c/XTZESPORTS" target="_blank" rel="noreferrer">
                <Image src={youtubeIcon} alt="youtube" />
              </a>
              <a href="https://discord.com/invite/rKFBUhHV8B" target="_blank" rel="noreferrer">
                <Image src={discordIcon} alt="discord" />
              </a>
              <a href="https://www.linkedin.com/company/xtzesports" target="_blank" rel="noreferrer">
                <Image src={linkedinIcon} alt="linkedin" />
              </a>
              <a href="https://www.instagram.com/xtz.esports/" target="_blank" rel="noreferrer">
                <Image src={instaIcon} alt="instagram" />
              </a>
              <a href="https://t.me/ZoneBYXtz" target="_blank" rel="noreferrer">
                <Image src={telegramIcon} alt="telegram" />
              </a>
              <a href="https://www.reddit.com/r/ZoneGameFi/" target="_blank" rel="noreferrer">
                <Image src={redditIcon} alt="reddit" />
              </a>
            </div>
            <div className="col-6 d-flex justify-content-end">
              <a className="text-black" href="https://docsend.com/view/j22evyxhdhc78d3w" target="_blank" rel="noreferrer">Whitepaper</a>
            </div>
          </div>
        </div>
      </footer>
    )
}

export default Footer