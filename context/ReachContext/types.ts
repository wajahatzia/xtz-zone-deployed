import { reachActionType, reachContractActionType } from "../../utils/constants";
import { ErrorPayload, StatusAPI } from "../AppContext/types";

type ReachStatusPaylaod = {
  attachP1?: StatusAPI
  setP1Interact?: StatusAPI
  setP2Interact?: StatusAPI
  attachP2?: StatusAPI
  signedP1Status?: boolean
  signedP2Status?: boolean
  contractStage?: reachContractActionType
}

type SetP1InteractPayload = {
  interactP1: {
    wager: any
    getChallengeParams: any
    displayWinner: any
    log: any
    signed: any
  }
}

type SetP2InteractPayload = {
  interactP2: {
    getChallengeParams: any
    displayWinner: any
    log: any
    signed: any
  }
}


export type ReachAction = {
  type: reachActionType | reachContractActionType
  status: ReachStatusPaylaod
  payload?: ErrorPayload | SetP1InteractPayload | SetP2InteractPayload
}

type Status = {
  attachP1: string
  setP1Interact: string
  attachP2: string
  setP2Interact: string
  signedP1Status: boolean
  signedP2Status: boolean
  contractStage: string
}

type Error = {
  message: string | null;
  details?: string| null;
}

export type ReachState = {
  interactP1: any;
  interactP2: any;
  status: Status,
  error: Error
}

export type ReachDispatch = (action: ReachAction) => void;