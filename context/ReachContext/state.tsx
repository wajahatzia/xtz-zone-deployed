import { createContext, useContext, useReducer } from 'react';
import { AppProviderProps, StatusAPI } from '../AppContext/types';
import { reachReducer } from './reducer';
import { ReachDispatch, ReachState } from './types';

const ReachContext = createContext<
  {
    state: ReachState;
    dispatch: ReachDispatch
  } | undefined
>(undefined)

const initialStateValue = {
  interactP1: {},
  interactP2: {},
  status: {
    attachP1: StatusAPI.IDLE,
    setP1Interact: StatusAPI.IDLE,
    setP2Interact: StatusAPI.IDLE,
    attachP2: StatusAPI.IDLE,
    signedP1Status: false,
    signedP2Status: false,
    contractStage: StatusAPI.IDLE
  },
  error: {
    message:null,
    details: null,
  },
}

const ReachProvider = ({children}: AppProviderProps) => {
  const [ state, dispatch ] = useReducer(reachReducer, initialStateValue);
  const value = { state, dispatch}
  return (
    <ReachContext.Provider value={value}>
      {children}
    </ReachContext.Provider>
  )
}

const useReach = () => {
  const context = useContext(ReachContext)
  if(context === undefined) {
    throw new Error('useReach must be used within a ReachProvider')
  }
  return context
}

export { ReachProvider, useReach}
