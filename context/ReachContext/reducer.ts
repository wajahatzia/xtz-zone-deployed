import { reachActionType } from "../../utils/constants";
import { ReachAction, ReachState } from "./types";

export const reachReducer = (state: ReachState, action: ReachAction) => {
  switch(action.type) {
    case reachActionType.LOADING: {
      return {
        ...state,
        status: {
          ...state.status,
          ...action.status
        }
      }
    }
    case reachActionType.FULFILLED: {
      return {
        ...state,
        ...action.payload,
        status: {
          ...state.status,
          ...action.status
        }
      }
    }
    case reachActionType.REJECTED: {
      return {
        ...state,
        status: {
          ...state.status,
          ...action.status
        },
        ...action.payload
      }
    }
    case reachActionType.SETWAGERFUlFILLED: {
      return {
        ...state,
        status: {
          ...state.status,
          ...action.status,
        },
        interactP1: {
          ...state.interactP1,
          ...action.payload
        }
      }
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}