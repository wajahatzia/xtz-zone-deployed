import { reachActionType, reachContractActionType } from "../../utils/constants";
import { StatusAPI } from "../AppContext/types";
import { ReachDispatch } from "./types";
import * as backend from '../../build/index.main';

type AttachPlayerPayload = {
  account: any
  wager: number | null
  matchId: string | null
  optIn: boolean
}

const AttachPlayer = async (
    {account, wager, matchId, optIn}: AttachPlayerPayload, dispatch: ReachDispatch
  ) => {
  try {
    const ctc = await account.contract(backend, 52156950)
    console.log("🚀 ~ file: api.ts ~ line 9 ~ AttachPlayer ~ ctc", ctc)
    const ctcApi = ctc.a.contractAPI;
    dispatch({
      type: reachActionType.LOADING,
      status: {
        contractStage: reachContractActionType.OPTIN
      }
    })
    if(optIn){
        const optIn = await ctcApi.optIn()
        console.log('optin', optIn)
        if(!optIn){
          throw new Error('Opt-In')
        }
    }
    dispatch({
      type: reachActionType.LOADING,
      status: {
        contractStage: reachContractActionType.DEPOSIT
      }
    })
      const deposit = await ctcApi.deposit([wager, matchId]);
      console.log('deposit', deposit)
      if(!deposit) {
        throw new Error('Deposit')
      }
  } catch (error: any) {
    throw error
  }
}

export const attachPlayer1 = async (dispatch: ReachDispatch, payload: AttachPlayerPayload) => {
  dispatch({
    type: reachActionType.LOADING,
    status: {
      attachP1: StatusAPI.LOADING
    }
  })
  try {
    await AttachPlayer(payload, dispatch)
    dispatch({
      type: reachActionType.FULFILLED,
      status: {
        attachP1: StatusAPI.FULFILLED
      }
    })

  } catch (error: any) {
    console.log('Error', error)
    if(error.message === 'Opt-In'){
      dispatch({ 
        type: reachActionType.REJECTED, 
        payload: { error: {message: 'Opt-In failed, please retry' }},
        status: { attachP1: StatusAPI.REJECTED }
      })
    }
    else if(error.message === 'Deposit'){
      dispatch({ 
        type: reachActionType.REJECTED, 
        payload: { error: {message: 'Deposit failed, please retry' }},
        status: { attachP1: StatusAPI.REJECTED }
      })
    } else
    dispatch({ 
      type: reachActionType.REJECTED, 
      payload: { error: {message: 'Signing Failed, please retry' }},
      status: { attachP1: StatusAPI.REJECTED }
    })
  }
}

export const attachPlayer2 = async (dispatch: ReachDispatch, payload: AttachPlayerPayload) => {
  dispatch({
    type: reachActionType.LOADING,
    status: {
      attachP2: StatusAPI.LOADING
    }
  })
  try {
    await AttachPlayer(payload, dispatch)
    dispatch({
      type: reachActionType.FULFILLED,
      status: {
        attachP2: StatusAPI.FULFILLED
      }
    })

  } catch (error: any) {
    dispatch({ 
      type: reachActionType.REJECTED, 
      payload: { error: { message: 'attachPlayer2 Api failed' }},
      status: { attachP2: StatusAPI.REJECTED }
    })
  }
}

type P1InteractPayload = {
  wager: any
  getChallengeParams: any
  displayWinner: any
  log: any
  signed: any
}

// export const setP1Interact = (dispatch: ReachDispatch, payload: P1InteractPayload) => {
//   dispatch({
//     type: reachActionType.LOADING,
//     status: {
//       setP1Interact: StatusAPI.LOADING
//     }
//   })
//   try {
//     dispatch({
//       type: reachActionType.FULFILLED,
//       status: {
//         setP1Interact: StatusAPI.FULFILLED
//       },
//       payload: {
//         interactP1: {
//           wager: Number(payload.wager),
//           getChallengeParams: payload.getChallengeParams,
//           displayWinner: payload.displayWinner,
//           log: payload.log,
//           signed: payload.signed,
//         }
//       }
//     })

//   } catch (error: any) {
//     dispatch({ 
//       type: reachActionType.REJECTED, 
//       payload: { error: { message: 'setP1Interact Api failed' }},
//       status: { setP1Interact: StatusAPI.REJECTED }
//     })
//   }
// }

type P2InteractPayload = {
  getChallengeParams: any;
  displayWinner: any;
  log: any
  signed: any
}

// export const setP2Interact = (dispatch: ReachDispatch, payload: P2InteractPayload) => {
//   dispatch({
//     type: reachActionType.LOADING,
//     status: {
//       setP2Interact: StatusAPI.LOADING
//     }
//   })
//   try {
//     dispatch({
//       type: reachActionType.FULFILLED,
//       status: {
//         setP2Interact: StatusAPI.FULFILLED
//       },
//       payload: {
//         interactP2: {
//           getChallengeParams: payload.getChallengeParams,
//           displayWinner: payload.displayWinner,
//           log: payload.log,
//           signed: payload.signed,
//         }
//       }
//     })
//   } catch (error: any) {
//     dispatch({ 
//       type: reachActionType.REJECTED, 
//       payload: { error: { message:'setP2Interact Api failed' }},
//       status: { setP2Interact: StatusAPI.REJECTED }
//     })
//   }
// }