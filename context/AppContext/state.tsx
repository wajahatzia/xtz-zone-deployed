import { createContext, useContext, useReducer } from 'react';
import { boolean } from 'yup/lib/locale';
import { actiontype } from '../../utils/constants';
import { walletReducer } from './reducer';
import { State, Dispatch, AppProviderProps, StatusAPI } from './types';

const AppContext = createContext<
  {
    state: State;
    dispatch: Dispatch
  } | undefined
>(undefined)

export const initialStateValue = {
  wallet: {
    balance: null,
    account: null,
  },
  player2Link: {
    url: null,
    gameId: null
  },
  walletStatus: false,
  status: {
    sendWager: StatusAPI.IDLE,
    notifyPlayer1: StatusAPI.IDLE,
    acceptChallenge: StatusAPI.IDLE,
    playerSigned: StatusAPI.IDLE,
    signContract: StatusAPI.IDLE,
    setRole: boolean,
    clearError:StatusAPI.IDLE,
    winner: null
  },
  error: {
    message:null,
    details: null,
  },
  contract: null,
  role: 'P1',
  player2Accept: {
    wagerAmount: null,
    notes: null,
    origin: null,
    status: 'Pending',
    matchId: null,
    _id: null,
    createdAt: null,
    updatedAt: null,
    __v: null,
    url: null,
    appId: '52156950',
    isSigned: false,
    oppSigned: false,
    result: {
      black: 0,
      white: 0,
      winner: ''
    }
  }
}

const AccountProvider = ({ children }: AppProviderProps) => {
  const [state, dispatch] = useReducer(walletReducer, initialStateValue)
  const value = { state, dispatch }
  return (
    <AppContext.Provider value={value}>
      {children}
    </AppContext.Provider>
  )
}

const useAccount = () => {
  const context = useContext(AppContext)
  if (context === undefined) {
    throw new Error('useAccount must be used within a AppProvider')
  }
  return context
}
export { AccountProvider, useAccount }