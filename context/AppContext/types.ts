import { ReactNode } from "react";
import { actiontype } from "../../utils/constants";

export type WagerPayload = {
  wager: string
  address: string
  name: string
  game: string
}

type Error = {
  message: string | null;
  details?: string | null;
}

export interface ErrorPayload {
  error: Error
  details?: string
}

export interface Player2Link {
  url: string | null;
  gameId: string | null;
}

export interface Player2LinkPayload {
  player2Link: Player2Link
}

export type ContractInfo = {
  contractInfo: {
    appId: string;
    address: string;
  },
  wager: string;
  role: string;
}


export type Player2Accept = {
  wagerAmount: number | null;
  notes: string | null;
  origin: string | null;
  status: string;
  matchId: string | null;
  _id: string | null;
  createdAt: string | null;
  updatedAt: string | null;
  __v: number | null;
  url: string | null;
  appId:string | null;
  isSigned: boolean;
  oppSigned: boolean;
  result: {
    black: number
    white: number
    winner: string
  }
}

export interface Player2AcceptPayload {
  player2Accept: Player2Accept
}

export interface ContractInfoPayload {
  contract: ContractInfo
}

type StatusPayload = {
  sendWager?: StatusAPI
  notifyPlayer1?: StatusAPI
  connectWallet?: StatusAPI
  acceptChallenge?: StatusAPI
  playerSigned?: StatusAPI
  signContract?: StatusAPI
  setRole?: boolean
  winner?: boolean
  clearError?: StatusAPI
  popup?: StatusAPI
  wagerVerification?: StatusAPI
}

type RolePayload = {
  role: string
  acceptChallenge?: StatusAPI 
  clearError?:StatusAPI
  winner?: boolean | null
}

export type Action = {
  type: actiontype,
  payload?: Account | WagerPayload | ErrorPayload | Player2LinkPayload | ContractInfoPayload | StatusPayload | Player2AcceptPayload | RolePayload
  status?: StatusPayload
}

export type Dispatch = (action: Action) => void;

export type Account = {
  // address: string;
  // name: string;
  balance: string | null;
  account: any
}

export enum StatusAPI {
  IDLE = 'idle',
  LOADING = 'loading',
  FULFILLED = 'suceeded',
  REJECTED = 'failed',
  CLEARERROR = 'clear'
}

type Status = {
  sendWager: string
  notifyPlayer1: string
  acceptChallenge: string
  playerSigned: string
  clearError:string
  winner:boolean | null
}

export type State = {
  wallet: Account
  walletStatus: boolean
  player2Link: Player2Link
  status: Status
  error: Error
  contract: ContractInfo | null
  role: null | string
  player2Accept: Player2Accept 
}

export type AppProviderProps = { children: ReactNode }