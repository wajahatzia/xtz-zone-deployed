import { server } from "../../utils/api"
import { actiontype } from "../../utils/constants"
import { Dispatch, StatusAPI } from "./types"

type sendWagerData = {
  wagerAmount: number;
  username: string;
  userId: string;
  game: string;
}

type GenerateLinkAPIResponse = {
  data: {
    url: string;
    gameId: string;
  }
}

export const setRole = (dispatch: Dispatch, role: string) => {
  dispatch({
    type: actiontype.SET_ROLE,
    payload: { role: role },
    status: { setRole: true }
  })
}

export const generateLinkAPI = async (dispatch: Dispatch, wager: sendWagerData) => {
  dispatch({
    type: actiontype.LOADING,
    status: { sendWager: StatusAPI.LOADING }
  })
  try {
    const response = await server.post<GenerateLinkAPIResponse>('init', { ...wager })
    console.log("🚀 ~ file: api.ts ~ line 35 ~ generateLinkAPI ~ response", response)
    dispatch({
      type: actiontype.FULFILLED,
      payload: { player2Link: response.data.data },
      status: { sendWager: StatusAPI.FULFILLED }
    })
    return response
  } catch (error: any) {
    dispatch({
      type: actiontype.REJECTED,
      payload: { error: { message: error.response.data.message, details: "Link Generation failed"} },
      status: { sendWager: StatusAPI.REJECTED }
    })
  }
}

export const notifyPlayer1 = async (dispatch: Dispatch, userId: string, timer: any) => {
  dispatch({
    type: actiontype.LOADING,
    status: { notifyPlayer1: StatusAPI.LOADING }
  })
  try {
    const response = await server.get<Player2AcceptResponse>(`${userId}`)
    console.log("🚀 ~ file: api.ts ~ line 68 ~ notifyPlayer1 ~ response", response)
    if (response.data.status === 'accepted') {
      clearInterval(timer)
      dispatch({
        type: actiontype.FULFILLED,
        payload: { player2Accept: response.data },
        status: { notifyPlayer1: StatusAPI.FULFILLED }
      })
    }
  } catch (error: any) {
    dispatch({
      type: actiontype.REJECTED,
      payload: { error: {message: "NotifyPlayer1 failed", details: error.response.data.message} },
      status: { notifyPlayer1: StatusAPI.REJECTED }
    })
  }
}


type Player2Accept = {
  username: string;
  userId: string;
  gameId: string | string [] | undefined;
  status: string;
}

type Player2AcceptResponse = {
  wagerAmount: number;
  notes: string | null;
  origin: string;
  status: string;
  matchId: string;
  _id: string;
  createdAt: string;
  updatedAt: string;
  __v: number;
  url: string;
  appId: string;
  isSigned: boolean;
  oppSigned: boolean;
  result: {
    black: number
    white: number
    winner: string
  }
}


export const DecisionChallengeAPI = async (dispatch: Dispatch, data: Player2Accept) => {
  dispatch({
    type: actiontype.LOADING,
    status: { acceptChallenge: StatusAPI.LOADING }
  })
  try {
    const response = await server.post<Player2AcceptResponse>('decision', {...data})
    console.log("🚀 ~ file: api.ts ~ line 122 ~ DecisionChallengeAPI ~ response", response)
    dispatch({
      type: actiontype.FULFILLED,
      payload: { player2Accept: response.data },
      status: { acceptChallenge: StatusAPI.FULFILLED }
    })
  } catch (error: any) {
    dispatch({
      type: actiontype.REJECTED,
      payload: {error: {message: error.response.data.message, details: "Decision API failed"} },
      status: { acceptChallenge: StatusAPI.REJECTED }
    })
  }
}

export const SignContract = async (dispatch: Dispatch, data: Player2Accept) => {
  dispatch({
    type: actiontype.LOADING,
    status: { signContract: StatusAPI.LOADING }
  })
  try {
    const response = await server.post<Player2AcceptResponse>('decision', {...data})
    console.log("🚀 ~ file: api.ts ~ line 144 ~ SignContract ~ response", response)
    dispatch({
      type: actiontype.FULFILLED,
      payload: { player2Accept: response.data },
      status: { signContract: StatusAPI.FULFILLED }
    })
  } catch (error: any) {
    console.log(error)
    dispatch({
      type: actiontype.REJECTED,
      payload: { error: {message: 'SignContract Api failed', details:  error.response.data.message }},
      status: { signContract: StatusAPI.REJECTED }
    })
  }
}

type SignContractStatus = {
  userId: string
  gameId: string
}

export const SignContractStatusAPI = async (dispatch: Dispatch, data: SignContractStatus) => {
  dispatch({
    type: actiontype.LOADING,
    status: { playerSigned: StatusAPI.LOADING }
  })
  try {
    const timer = setInterval( async() => {
      const response = await server.get<Player2AcceptResponse>(`${data.userId}/${data.gameId}`)
      console.log("🚀 ~ file: api.ts ~ line 172 ~ timer ~ response", response)
      if (response.data.oppSigned || response.data.oppSigned) {
        dispatch({
          type: actiontype.FULFILLED,
          payload: { player2Accept: response.data },
          status: { playerSigned: StatusAPI.FULFILLED }
        })
      }
      if(response.data.status === 'finished') {
        clearInterval(timer)
        dispatch({
          type: actiontype.FULFILLED,
          payload: { player2Accept: response.data },
          status: { playerSigned: StatusAPI.FULFILLED, winner: true }
        })
      }
    }, 15000)
  } catch (error: any) {
    console.log(error)
    dispatch({
      type: actiontype.REJECTED,
      payload: { error: {message: 'SignContractStatus Api failed', details: error.response.data.message }},
      status: { playerSigned: StatusAPI.REJECTED }
    })
  }
}
