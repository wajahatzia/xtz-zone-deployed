import { actiontype } from '../../utils/constants';
import { initialStateValue } from './state';
import { Action, State } from './types';

export const walletReducer = (state: State, action: Action) => {
  switch(action.type) {
    case actiontype.CLEAR_WALLET : {
      return {
        ...initialStateValue
      }
    }
    case actiontype.ADDWALLET: {
      return {
        ...state,
        wallet: {
          ...state.wallet, 
          ...action.payload
        },
        walletStatus: true
      }
    }
    case actiontype.LOADING: {
      return {
        ...state,
        ...action.payload,
        status: {
          ...state.status,
          ...action.status
        }
      }
    }
    case actiontype.FULFILLED: {
      return {
        ...state,
        ...action.payload,
        status: {
          ...state.status,
          ...action.status
        }
      }
    }
    case actiontype.REJECTED: {
      return {
        ...state,
        ...action.payload,
        status: {
          ...state.status,
          ...action.status
        }
      }  
    }
    case actiontype.CLEARERROR: {
      return {
        ...state,
        ...action.payload,
        error:{
          ...state.error,
          ...action.status
        }

      }
      
    }
    case actiontype.SET_ROLE: {
      return {
        ...state,
        ...action.payload,
        status: {
          ...state.status,
          ...action.status
        }
      }
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}